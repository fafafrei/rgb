{
    "id": "c6bd002d-572d-4066-a1ac-64bac793916b",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tAutoTileBlue",
    "auto_tile_sets": [
        {
            "id": "51a1c5e5-c2ce-420c-97b0-e556f860dd0c",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_1",
            "tiles": [
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                11,
                12,
                13,
                14,
                15,
                0
            ]
        }
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 4,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "9b131d41-8f98-43c5-ac19-923d1dc19313",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 16,
    "tileheight": 16,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 16,
    "tilexoff": 0,
    "tileyoff": 0
}