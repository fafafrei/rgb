{
    "id": "ce349c98-b023-48eb-8973-1a6398a77fdd",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tAutoTile",
    "auto_tile_sets": [
        {
            "id": "8b478857-6076-4e16-895f-fed7fc7bc9f2",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_1",
            "tiles": [
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                11,
                12,
                13,
                14,
                15,
                0
            ]
        }
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 4,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "3b1de6e2-d53f-4ee0-baba-7d7cb0a40025",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 16,
    "tileheight": 16,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 16,
    "tilexoff": 0,
    "tileyoff": 0
}