{
    "id": "037160ef-c331-4fc9-aff9-ee56d3921523",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBullet",
    "eventList": [
        {
            "id": "ac9772d7-9592-469c-9bff-82be51fe6572",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "037160ef-c331-4fc9-aff9-ee56d3921523"
        },
        {
            "id": "7bc58beb-a662-4456-8829-8d939c2555c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c345c2bb-f876-473c-8aca-ed25991c03d4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "037160ef-c331-4fc9-aff9-ee56d3921523"
        },
        {
            "id": "5834616f-3d12-4ab5-b424-f29a837d7a47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "037160ef-c331-4fc9-aff9-ee56d3921523"
        },
        {
            "id": "7a584c5e-b868-44f0-9bfa-806592c0fe50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "037160ef-c331-4fc9-aff9-ee56d3921523"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}