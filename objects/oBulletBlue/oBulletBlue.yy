{
    "id": "4efa9dfc-2598-440e-9c2c-978a84839e26",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBulletBlue",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "037160ef-c331-4fc9-aff9-ee56d3921523",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "310c325d-96cb-4a43-ba53-6a126ee49bc6",
    "visible": true
}