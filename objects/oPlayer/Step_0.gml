/// @description Player Movement
if (rHp <= 0 || gHp <= 0 || bHp <= 0) {
	//instance_destroy();
	isDead = true;
} else {

	var hInpt = keyboard_check(vk_right) - keyboard_check(vk_left);

	if (hInpt != 0) {
		spd[h] += hInpt * acceleration;
		spd[h] = clamp(spd[h], -maxSpd, maxSpd);
	} else {
		spd[h] = lerp(spd[h], 0, fric) ;
	}


	//Jump
	if (keyboard_check_pressed(vk_up) && numJumps < maxJumps) {
		audio_play_sound(choose(jump1, jump2, jump3, jump4, jump5), 1, false);
		numJumps ++;
		spd[v] = jmpHeight;
		xScale = image_xscale * 0.6;
		yScale = image_yscale * 1.4;
		if (!place_meeting(x, y + 1, oSolid)) {
			instance_create_layer(x, y, "Instances", oBomb);
		}
	}

	if (!place_meeting(x, y + 1, oSolidPar)) {
		spd[v] += grav;
	} 
	move(spd, true);

	//Landing
	if (place_meeting(x, y + 1, oSolidPar) && !place_meeting(x, yprevious + 1, oSolidPar)) {
		with (oCamera) {
			shake = true;
			alarm[0] = room_speed*0.8;
		}
		if (place_meeting(x, y + 1, oEnemy)) {
			audio_play_sound(choose(jump1, jump2, jump3, jump4, jump5), 1, false);
			spd[v] = jmpHeight;
		}

		numJumps = 0;
		xScale = image_xscale * 1.4;
		yScale = image_yscale * 0.6;
	} 

	// Move back to normal scale
	xScale = lerp(xScale, image_xscale, 0.1);
	yScale = lerp(yScale, image_yscale, 0.1);

	//warp
	warp();
}