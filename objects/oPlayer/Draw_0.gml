/// @description Draw gun
var dir = point_direction(x, y, mouse_x, mouse_y);
var flipped = (mouse_x > x) * 2 - 1;
var sGun = sGreenGun;

if (curGun == "red") {
	sGun = sRedGun;
	cursor_sprite = sCursorRed;
} else if (curGun == "blue") {
	sGun = sBlueGun;
	cursor_sprite = sCursorBlue;
} else {
	cursor_sprite = sCursorGreen;
}

if (!isDead) {
	if (flipped > 0) {
	draw_sprite_ext(sPlayer, image_index,  x, y, xScale * flipped, yScale, 0, image_blend, image_alpha);
	draw_sprite_ext(sGun, 0,  x + 6*flipped, y - sprite_height/3, 1, flipped, dir, image_blend, image_alpha);
	} else {
		draw_sprite_ext(sGun, 0,  x + 6*flipped, y - sprite_height/3, 1, flipped, dir, image_blend, image_alpha);
		draw_sprite_ext(sPlayer, image_index,  x, y, xScale * flipped, yScale, 0, image_blend, image_alpha);
	}
}

