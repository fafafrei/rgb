/// @description Shoot
if (alarm[0] <= 0 && !isDead) {
	audio_play_sound(choose(shoot1, shoot2, shoot4, shoot4), 10, false);
	var dir = point_direction(x, y - sprite_height/3, mouse_x, mouse_y);
	var flipped = (mouse_x > x) * 2 - 1;
	var gunX = x;
	var xOffset = lengthdir_x(20, dir);
	var yOffset = lengthdir_y(20, dir); 
	var bullet = oBulletGreen;
	var bulletScale = 1;
	
	if (curGun == "red") {
		rHp -= shootDamage;
		bullet = oBulletRed;
		bulletScale = rHp/maxHp;
	} else if (curGun == "blue") {
		bHp -= shootDamage;
		bullet = oBulletBlue;
		bulletScale = bHp/maxHp;
	} else {
		gHp -= shootDamage;
		bulletScale = gHp/maxHp;
	}

	var bullet = instance_create_layer(gunX + xOffset, y - sprite_height/3 + yOffset, "Instances", bullet);
	bullet.direction = dir;
	bullet.image_angle = dir;
	bullet.image_xscale = bulletScale;
	bullet.image_yscale = bulletScale;
	alarm[0] = bulletCooldown;
}

