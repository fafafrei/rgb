{
    "id": "1a103fe8-9838-4a4d-8793-3fc647a9679e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "ed73b9f5-785f-41fd-a59f-859fce14869e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1a103fe8-9838-4a4d-8793-3fc647a9679e"
        },
        {
            "id": "43e52f38-5cf9-487a-9a6d-83eef607ad9e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1a103fe8-9838-4a4d-8793-3fc647a9679e"
        },
        {
            "id": "3b3b06bd-1775-4dc7-92b9-fb06869f2eea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1a103fe8-9838-4a4d-8793-3fc647a9679e"
        },
        {
            "id": "d733c122-1f0e-4230-b6ac-2a650c49e2aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 6,
            "m_owner": "1a103fe8-9838-4a4d-8793-3fc647a9679e"
        },
        {
            "id": "77c04eab-dacf-4e3d-ad93-8f80fe2d6960",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1a103fe8-9838-4a4d-8793-3fc647a9679e"
        },
        {
            "id": "3c0c1c0f-c1d9-4e95-8980-6f514deb2095",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6f4a7f17-432f-4004-a0e3-acd7fe72fa27",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1a103fe8-9838-4a4d-8793-3fc647a9679e"
        },
        {
            "id": "6104e7f4-f7ec-4fc8-9851-9ed4d0e1919b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "1a103fe8-9838-4a4d-8793-3fc647a9679e"
        },
        {
            "id": "ef9e0ccb-4a34-4824-ac1a-f9787748210f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d3a850ed-8b75-476b-b153-efb022ad6ad0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1a103fe8-9838-4a4d-8793-3fc647a9679e"
        },
        {
            "id": "80024a94-9b12-432d-a21f-5795e2cd0943",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "22d38337-6074-478e-966e-a0c32e34772d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1a103fe8-9838-4a4d-8793-3fc647a9679e"
        },
        {
            "id": "eebaa692-eb16-4fc5-9398-10af39e9072b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 49,
            "eventtype": 9,
            "m_owner": "1a103fe8-9838-4a4d-8793-3fc647a9679e"
        },
        {
            "id": "f5d9b866-d247-470f-86bb-8220d37a8161",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 9,
            "m_owner": "1a103fe8-9838-4a4d-8793-3fc647a9679e"
        },
        {
            "id": "6d440d7b-bee9-4d8f-9f92-5675fda37b4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 51,
            "eventtype": 9,
            "m_owner": "1a103fe8-9838-4a4d-8793-3fc647a9679e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "54dfcdfe-14ad-4102-90b4-88110ac54413",
    "visible": true
}