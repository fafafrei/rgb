
if (!isDead) {
	draw_healthbar(view_xview[0]+32, view_yview[0]+32, view_xview[0]+432, view_yview[0]+62, (rHp/maxHp)*100, c_white, c_red, c_red, 0, true, true);
	draw_healthbar(view_xview[0]+462, view_yview[0]+32, view_xview[0]+862, view_yview[0]+62, (gHp/maxHp)*100, c_white, c_green, c_green, 0, true, true);
	draw_healthbar(view_xview[0]+892, view_yview[0]+32, view_xview[0]+1292, view_yview[0]+62, (bHp/maxHp)*100, c_white, c_blue, c_blue, 0, true, true);

	draw_set_font(fontScore);
	draw_set_color(c_white);
	draw_text(view_xview[0]+1622, view_yview[0]+32, "score: " + string(oScore.theScore));
} else {
	draw_set_font(fontScore);
	draw_set_color(c_red);
	draw_text(view_wview[0]/2, view_hview[0]/2, "you are dead");
	draw_text(view_wview[0]/2, view_hview[0]/2 + 50, "press 'R' to restart or 'Esc' to quit");
	draw_text(view_wview[0]/2, view_hview[0]/2 + 100, "final score: " + string(oScore.theScore));
	
}

	