/// @description Vars
maxJumps = 3;
numJumps = 0;

maxSpd = 4;

spd = [0, 0];
grav = 1;

acceleration = 1;
fric = 0.3;
jmpHeight = -16;

maxHp = 150;
rHp = maxHp;
gHp = maxHp;
bHp = maxHp;

curGun = "red";
shootDamage = 2;


//Map keys
keyboard_set_map(ord("W"), vk_up);
keyboard_set_map(ord("A"), vk_left);
keyboard_set_map(ord("D"), vk_right);

//Bullet cooldown
bulletCooldown = room_speed/4;
alarm[0] = bulletCooldown;

//Scale vars
xScale = image_xscale;
yScale = image_yscale;

image_speed = 0.2;
audio_play_sound(music1, 100, true);

isDead = false;

cursor_sprite = sCursorRed;
window_set_cursor(cr_none);