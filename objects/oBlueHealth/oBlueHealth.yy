{
    "id": "74740bc4-957a-4504-8eed-3b0b053ff55d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBlueHealth",
    "eventList": [
        {
            "id": "97a3a5f2-d50d-40c3-abfb-aec2a9018f1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "74740bc4-957a-4504-8eed-3b0b053ff55d"
        },
        {
            "id": "f410f049-a970-4895-8d6c-d18232f84d7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "74740bc4-957a-4504-8eed-3b0b053ff55d"
        },
        {
            "id": "5616c6b9-3351-4026-b255-623b87493b39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1a103fe8-9838-4a4d-8793-3fc647a9679e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "74740bc4-957a-4504-8eed-3b0b053ff55d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "dad54724-93e7-4358-a207-c8de6e1c70a6",
    "visible": true
}