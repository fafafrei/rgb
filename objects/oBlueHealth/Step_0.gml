/// @description Movement state
if (instance_exists(oPlayer)) {
	var dir = point_direction(x, y, oPlayer.x, oPlayer.y);
	spd[h] = lengthdir_x(maxSpd, dir);
	spd[v] = lengthdir_y(maxSpd, dir);
	move(spd, false);
}