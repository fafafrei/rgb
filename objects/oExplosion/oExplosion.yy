{
    "id": "b0653817-bfd5-4960-b954-c74eba3374da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oExplosion",
    "eventList": [
        {
            "id": "577a09a9-e345-4ace-bb54-1d2b40f9d47d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "b0653817-bfd5-4960-b954-c74eba3374da"
        },
        {
            "id": "3c5c0375-1dd4-48f1-bf47-9c1f7d46a74c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b0653817-bfd5-4960-b954-c74eba3374da"
        },
        {
            "id": "26bf3ba5-6e98-4f3c-9d31-aa7208a48ada",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b0653817-bfd5-4960-b954-c74eba3374da"
        },
        {
            "id": "cca4f1bf-798c-4f19-a6ed-5270babac474",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6b0dc5b8-db72-4da8-8230-67f36318822d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b0653817-bfd5-4960-b954-c74eba3374da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "8890e2df-036d-4906-adb7-7a7c06543976",
    "visible": true
}