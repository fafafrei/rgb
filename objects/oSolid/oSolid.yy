{
    "id": "c345c2bb-f876-473c-8aca-ed25991c03d4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSolid",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "e47d8877-f73b-4d1b-ba67-4aca9a928f34",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "4c248f53-122a-461e-bd95-37b7499b80e5",
    "visible": true
}