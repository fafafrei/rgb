{
    "id": "cc1147be-f5e4-4c2e-b428-b701197d2d6f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBulletHit",
    "eventList": [
        {
            "id": "7e0ab783-2093-404d-8e2d-e8f43abc8c9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "cc1147be-f5e4-4c2e-b428-b701197d2d6f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "38263b1a-243c-40bb-b064-f45c51556f5e",
    "visible": true
}