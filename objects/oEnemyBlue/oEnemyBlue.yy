{
    "id": "1fc0049e-37e9-4485-9212-5ee39efb7773",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemyBlue",
    "eventList": [
        {
            "id": "79e5b3a4-d43d-4036-9de9-72118af43aa1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1fc0049e-37e9-4485-9212-5ee39efb7773"
        },
        {
            "id": "f3d33bda-3412-47d7-865d-2cacdc8a86cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1fc0049e-37e9-4485-9212-5ee39efb7773"
        },
        {
            "id": "a6461ded-d1b3-4452-a673-b0622be4a21a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4efa9dfc-2598-440e-9c2c-978a84839e26",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1fc0049e-37e9-4485-9212-5ee39efb7773"
        },
        {
            "id": "b6bef228-e82e-4cb3-85bb-5ba88e66b71c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "1fc0049e-37e9-4485-9212-5ee39efb7773"
        },
        {
            "id": "e508d264-ee78-4cec-89e6-e78da33e702e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "1fc0049e-37e9-4485-9212-5ee39efb7773"
        },
        {
            "id": "53ccdf9f-a3ce-45ce-82a4-d3a6353522af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "1fc0049e-37e9-4485-9212-5ee39efb7773"
        },
        {
            "id": "9b3d7de8-cd50-4fe9-a118-ceb50fddb455",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1fc0049e-37e9-4485-9212-5ee39efb7773"
        },
        {
            "id": "f97c6b56-6122-416d-867f-dacaf93a7ed2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "1fc0049e-37e9-4485-9212-5ee39efb7773"
        },
        {
            "id": "462f8e37-8e1a-48eb-b863-48e79eb153af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6b0dc5b8-db72-4da8-8230-67f36318822d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1fc0049e-37e9-4485-9212-5ee39efb7773"
        },
        {
            "id": "4b88334c-f0bd-41f9-9e4a-dc256e7feb1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1fc0049e-37e9-4485-9212-5ee39efb7773"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "6b0dc5b8-db72-4da8-8230-67f36318822d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "7f5ecedb-bd39-4c7a-a6e5-85a305f9c63e",
    "visible": true
}