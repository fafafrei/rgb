{
    "id": "22d38337-6074-478e-966e-a0c32e34772d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemyBulletGreen",
    "eventList": [
        {
            "id": "5df67cd5-bdfa-4f7c-aed0-47a1e8e27702",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "22d38337-6074-478e-966e-a0c32e34772d"
        },
        {
            "id": "6750c04a-d34c-4ac9-b02e-e60536322562",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c345c2bb-f876-473c-8aca-ed25991c03d4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "22d38337-6074-478e-966e-a0c32e34772d"
        },
        {
            "id": "fdb10ba2-a2e9-46f5-aad8-765db5aba9a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "22d38337-6074-478e-966e-a0c32e34772d"
        },
        {
            "id": "ca70154b-abba-48c4-853b-e2562493072d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "22d38337-6074-478e-966e-a0c32e34772d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "a3bf9397-7350-4686-bc27-f0314fe77540",
    "visible": true
}