{
    "id": "51d2c954-6cb6-4552-af13-3a214b7562f7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemyBulletHit",
    "eventList": [
        {
            "id": "e3e82214-b5d2-4273-83e2-54a165a4f6d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "51d2c954-6cb6-4552-af13-3a214b7562f7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "38263b1a-243c-40bb-b064-f45c51556f5e",
    "visible": true
}