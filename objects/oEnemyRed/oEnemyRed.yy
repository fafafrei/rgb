{
    "id": "de21104c-8664-4583-b8ba-09f2284682c4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemyRed",
    "eventList": [
        {
            "id": "ba89aa10-6a0f-4112-8cd5-8dc5e33401e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "de21104c-8664-4583-b8ba-09f2284682c4"
        },
        {
            "id": "dba68b23-fb6c-4124-8b2c-4452267d9d71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "de21104c-8664-4583-b8ba-09f2284682c4"
        },
        {
            "id": "aa790c6a-9500-481b-80e2-701caf19c96f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f4246aeb-ea98-4294-bd6b-2d8969cf8425",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "de21104c-8664-4583-b8ba-09f2284682c4"
        },
        {
            "id": "59b88d85-7558-40dd-b55e-586ee97da9bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "de21104c-8664-4583-b8ba-09f2284682c4"
        },
        {
            "id": "ce997473-bc38-4582-a351-3343cf4544b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "de21104c-8664-4583-b8ba-09f2284682c4"
        },
        {
            "id": "91c2f265-e65f-423d-841a-897f268b0816",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "de21104c-8664-4583-b8ba-09f2284682c4"
        },
        {
            "id": "e7f9e364-5b2e-4687-8ac3-ac1aaf8c8f2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "de21104c-8664-4583-b8ba-09f2284682c4"
        },
        {
            "id": "46c64916-f703-4620-9345-89b06250f1dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "de21104c-8664-4583-b8ba-09f2284682c4"
        },
        {
            "id": "0ce99ff0-95ae-4f64-9089-1090a28c331d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6b0dc5b8-db72-4da8-8230-67f36318822d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "de21104c-8664-4583-b8ba-09f2284682c4"
        },
        {
            "id": "a2e4bcf0-145c-46ab-b682-8d06b2b7ef73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "de21104c-8664-4583-b8ba-09f2284682c4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "6b0dc5b8-db72-4da8-8230-67f36318822d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "9dd8ce17-8eed-4cdd-945b-c149c23d31b7",
    "visible": true
}