/// @description
event_user(state);

// Death
if (hp <= 0) {
	instance_create_layer(x, y, "Instances", oGreenHealth);
	oScore.theScore += 33;
	instance_destroy();
}

if (place_meeting(x, y - 1, oPlayer)) {
	xScale = 1.5;
	yScale = 0.5;
} 

// Move back to normal scale
xScale = lerp(xScale, image_xscale, 0.1);
yScale = lerp(yScale, image_yscale, 0.1);

//Warp
warp();