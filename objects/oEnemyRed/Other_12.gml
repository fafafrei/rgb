/// @description HIT state

//Push Force
move(spdPush, false);
spdPush[h] = lerp(spdPush[h], 0, 0.1);
spdPush[v] = lerp(spdPush[v], 0, 0.1);

if (point_distance(0, 0, spdPush[h], spdPush[v]) < 1) state = MOVEMENT;