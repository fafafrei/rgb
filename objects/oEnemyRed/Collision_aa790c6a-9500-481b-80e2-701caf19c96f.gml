/// @descriptionTake damage
hp -= 1;
instance_destroy(other);
state = HIT;
var dir = other.direction;
spdPush[h] = lengthdir_x(8, dir);
spdPush[v] = lengthdir_y(8, dir);
