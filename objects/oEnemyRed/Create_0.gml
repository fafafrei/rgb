/// @description Init
spd = [0, 0];
spdPush = [0, 0];
hp = 4;
maxSpd = 3;

//Bullet cooldown
bulletCooldown = room_speed;
alarm[0] = bulletCooldown;

// States
MOVEMENT = 0;
ATTACK = 1;
HIT = 2;

xScale = image_xscale;
yScale = image_yscale;

state = MOVEMENT;
