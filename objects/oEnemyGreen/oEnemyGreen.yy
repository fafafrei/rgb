{
    "id": "d48b0b39-a241-477c-8e1b-024bae53b2e3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemyGreen",
    "eventList": [
        {
            "id": "878f7d61-3cef-4fc8-b26a-dd6684aa592c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d48b0b39-a241-477c-8e1b-024bae53b2e3"
        },
        {
            "id": "814eaaea-db53-43d1-9dfb-1c63bbf35db9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d48b0b39-a241-477c-8e1b-024bae53b2e3"
        },
        {
            "id": "c0e6d24b-2672-455b-978f-5cb640292d76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "11ee58a6-97b9-4c8a-8020-17d2da8063cc",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d48b0b39-a241-477c-8e1b-024bae53b2e3"
        },
        {
            "id": "782aa748-196f-4fa1-ac3b-18a9cbbd3f75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "d48b0b39-a241-477c-8e1b-024bae53b2e3"
        },
        {
            "id": "ba1affe5-0af5-4b22-9a2e-003a2b5ee52e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "d48b0b39-a241-477c-8e1b-024bae53b2e3"
        },
        {
            "id": "4482387f-bd66-4ed8-8559-8650fb52c317",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "d48b0b39-a241-477c-8e1b-024bae53b2e3"
        },
        {
            "id": "4da6bdc8-fa34-49ad-9f65-acd40d8c38d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d48b0b39-a241-477c-8e1b-024bae53b2e3"
        },
        {
            "id": "1fde444b-fcc0-490a-9b51-208fed057a80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "d48b0b39-a241-477c-8e1b-024bae53b2e3"
        },
        {
            "id": "094a418b-dee7-47b4-8624-c4a3813aca61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6b0dc5b8-db72-4da8-8230-67f36318822d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d48b0b39-a241-477c-8e1b-024bae53b2e3"
        },
        {
            "id": "33ea170d-0214-4d0f-a64d-334388a1dad9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d48b0b39-a241-477c-8e1b-024bae53b2e3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "6b0dc5b8-db72-4da8-8230-67f36318822d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "339921e8-913e-4a4c-9640-5b6a7fd04e17",
    "visible": true
}