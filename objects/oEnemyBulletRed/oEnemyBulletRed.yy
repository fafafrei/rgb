{
    "id": "6f4a7f17-432f-4004-a0e3-acd7fe72fa27",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemyBulletRed",
    "eventList": [
        {
            "id": "a948fe6f-801a-4c98-8868-925ed0b3c51b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6f4a7f17-432f-4004-a0e3-acd7fe72fa27"
        },
        {
            "id": "9467ac70-1af8-42d8-81f6-723e7465e455",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c345c2bb-f876-473c-8aca-ed25991c03d4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6f4a7f17-432f-4004-a0e3-acd7fe72fa27"
        },
        {
            "id": "968c6783-8e30-4358-9cf3-b752dd3ce131",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "6f4a7f17-432f-4004-a0e3-acd7fe72fa27"
        },
        {
            "id": "a74b0e70-3ad4-45e5-851a-b041f0e41de3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "6f4a7f17-432f-4004-a0e3-acd7fe72fa27"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "1bc9c0fa-4811-477e-8e7c-6299feb4f8b4",
    "visible": true
}