{
    "id": "efd7bffe-e3dc-4f69-b7ae-cc5d100b0914",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGreenHealth",
    "eventList": [
        {
            "id": "e2f2a42c-2db2-41f2-b1c3-a980b1f3a560",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "efd7bffe-e3dc-4f69-b7ae-cc5d100b0914"
        },
        {
            "id": "fd125729-e93b-4b17-814f-e7b04a59d140",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "efd7bffe-e3dc-4f69-b7ae-cc5d100b0914"
        },
        {
            "id": "cd0fee82-b995-4158-84db-b483c835b62d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1a103fe8-9838-4a4d-8793-3fc647a9679e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "efd7bffe-e3dc-4f69-b7ae-cc5d100b0914"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "4f841bdd-0813-445a-a559-bb179001fba8",
    "visible": true
}