{
    "id": "2c925560-95a1-4477-876d-542124123b89",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBomb",
    "eventList": [
        {
            "id": "1692b16d-cdcb-4f16-b716-3e4b5b8928ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2c925560-95a1-4477-876d-542124123b89"
        },
        {
            "id": "fe83ce2a-c0b7-4509-92e1-6ea7c3123a52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2c925560-95a1-4477-876d-542124123b89"
        },
        {
            "id": "71c006b1-0787-488d-8966-fc2c90dbdd13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e47d8877-f73b-4d1b-ba67-4aca9a928f34",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2c925560-95a1-4477-876d-542124123b89"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "9e1234ec-2af9-4f5a-8283-d959e5d83265",
    "visible": true
}