{
    "id": "51ff13c1-fba6-4d15-8f36-238a777adaba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCamera",
    "eventList": [
        {
            "id": "0f2645f2-bc68-472a-ac09-bcd9deda0bc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "51ff13c1-fba6-4d15-8f36-238a777adaba"
        },
        {
            "id": "187274d6-b652-46b0-88ec-cce5de29eceb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "51ff13c1-fba6-4d15-8f36-238a777adaba"
        },
        {
            "id": "01a25bf9-1cb8-4f64-8443-2d56a142646e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "51ff13c1-fba6-4d15-8f36-238a777adaba"
        },
        {
            "id": "1089a36a-9ff5-461c-947b-e864971fe6af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 70,
            "eventtype": 9,
            "m_owner": "51ff13c1-fba6-4d15-8f36-238a777adaba"
        },
        {
            "id": "e5fabca6-afb7-4de1-8b0b-1151dfa2afd9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "51ff13c1-fba6-4d15-8f36-238a777adaba"
        },
        {
            "id": "05fd853f-cf5d-4349-a594-00ef7e9a46a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "51ff13c1-fba6-4d15-8f36-238a777adaba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}