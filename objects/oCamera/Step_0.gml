x += (xTo - x)/25;
y += (yTo -y)/25;

if (follow != noone && instance_exists(oPlayer)) {
	xTo = follow.x;
	yTo = follow.y;
	var vm = matrix_build_lookat(x, y, -10, x, y, 0, 0, 1, 0);
	camera_set_view_mat(camera, vm); 
}

if (shake) {
	view_xview[0] = random_range(-20, 20);
	view_yview[0] = random_range(-20, 20);
}

