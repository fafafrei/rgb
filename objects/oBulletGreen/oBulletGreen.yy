{
    "id": "11ee58a6-97b9-4c8a-8020-17d2da8063cc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBulletGreen",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "037160ef-c331-4fc9-aff9-ee56d3921523",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "a3bf9397-7350-4686-bc27-f0314fe77540",
    "visible": true
}