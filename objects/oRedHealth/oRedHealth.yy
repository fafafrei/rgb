{
    "id": "24a17eb6-8bd1-4262-8602-a0ea7784d712",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRedHealth",
    "eventList": [
        {
            "id": "275054f4-eb35-472d-8c17-1bf268c61925",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "24a17eb6-8bd1-4262-8602-a0ea7784d712"
        },
        {
            "id": "4cd95635-2eea-4981-ba18-46de3e8d47e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "24a17eb6-8bd1-4262-8602-a0ea7784d712"
        },
        {
            "id": "7a5c0821-1521-4a9a-ac09-a8f3f207484b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1a103fe8-9838-4a4d-8793-3fc647a9679e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "24a17eb6-8bd1-4262-8602-a0ea7784d712"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "a29084d6-c27d-4c87-84e9-df2e93f4f95a",
    "visible": true
}