{
    "id": "d3a850ed-8b75-476b-b153-efb022ad6ad0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemyBulletBlue",
    "eventList": [
        {
            "id": "eef35939-bace-40c7-8cf8-1cb74f0a2760",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d3a850ed-8b75-476b-b153-efb022ad6ad0"
        },
        {
            "id": "7c14bc0b-239a-40f8-a0f1-380fdacfb8f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c345c2bb-f876-473c-8aca-ed25991c03d4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d3a850ed-8b75-476b-b153-efb022ad6ad0"
        },
        {
            "id": "e2b04418-8f99-4345-8df6-4727877e45d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "d3a850ed-8b75-476b-b153-efb022ad6ad0"
        },
        {
            "id": "1d7ef895-7336-4fc7-a522-991a5f5420d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "d3a850ed-8b75-476b-b153-efb022ad6ad0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "310c325d-96cb-4a43-ba53-6a126ee49bc6",
    "visible": true
}