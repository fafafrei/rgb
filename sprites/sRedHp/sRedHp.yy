{
    "id": "a29084d6-c27d-4c87-84e9-df2e93f4f95a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRedHp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b64d6f76-99dd-4f1a-bd61-919857940296",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a29084d6-c27d-4c87-84e9-df2e93f4f95a",
            "compositeImage": {
                "id": "8ffca275-c27e-460e-bbe3-e3e845875f42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b64d6f76-99dd-4f1a-bd61-919857940296",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb94712d-ec58-4ac9-842e-15553443e115",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b64d6f76-99dd-4f1a-bd61-919857940296",
                    "LayerId": "7851ecae-cf5c-48ab-8674-206355e135be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "7851ecae-cf5c-48ab-8674-206355e135be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a29084d6-c27d-4c87-84e9-df2e93f4f95a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 15,
    "yorig": 14
}