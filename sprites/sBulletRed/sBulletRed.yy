{
    "id": "1bc9c0fa-4811-477e-8e7c-6299feb4f8b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBulletRed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "36196954-9f27-4d3a-9de9-6727351fd459",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bc9c0fa-4811-477e-8e7c-6299feb4f8b4",
            "compositeImage": {
                "id": "7a3e2df4-25a3-4734-9d4b-026688059356",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36196954-9f27-4d3a-9de9-6727351fd459",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48c5a05d-5123-4f78-87bb-d8669b15f401",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36196954-9f27-4d3a-9de9-6727351fd459",
                    "LayerId": "1ae2f082-4d8e-42c5-a5ee-1bf94b691e49"
                }
            ]
        },
        {
            "id": "2c241618-e635-4b37-b6a7-35f5216d97d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bc9c0fa-4811-477e-8e7c-6299feb4f8b4",
            "compositeImage": {
                "id": "21ff63af-38f5-4701-bef7-0aa7f01b7ca9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c241618-e635-4b37-b6a7-35f5216d97d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1148f2e6-1616-49c4-84e2-510a766fc185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c241618-e635-4b37-b6a7-35f5216d97d7",
                    "LayerId": "1ae2f082-4d8e-42c5-a5ee-1bf94b691e49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "1ae2f082-4d8e-42c5-a5ee-1bf94b691e49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bc9c0fa-4811-477e-8e7c-6299feb4f8b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4278190335,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}