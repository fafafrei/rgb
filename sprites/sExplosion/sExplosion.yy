{
    "id": "8890e2df-036d-4906-adb7-7a7c06543976",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sExplosion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ad5b057d-5cb9-453f-900e-e7b186d05c57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8890e2df-036d-4906-adb7-7a7c06543976",
            "compositeImage": {
                "id": "7838f580-8695-42b5-9cc2-ed67b1338c36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad5b057d-5cb9-453f-900e-e7b186d05c57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3744ecd2-b574-4acb-821b-9ceb1d32656a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad5b057d-5cb9-453f-900e-e7b186d05c57",
                    "LayerId": "100f299b-62a2-4c4d-8c23-ef357f8ec508"
                }
            ]
        },
        {
            "id": "9799f9bc-9b7c-4a38-813e-9a2ab3ad8139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8890e2df-036d-4906-adb7-7a7c06543976",
            "compositeImage": {
                "id": "1757e835-5109-4844-8c98-bf135deb7e2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9799f9bc-9b7c-4a38-813e-9a2ab3ad8139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5ca238e-8471-4777-bc79-fb2d260de332",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9799f9bc-9b7c-4a38-813e-9a2ab3ad8139",
                    "LayerId": "100f299b-62a2-4c4d-8c23-ef357f8ec508"
                }
            ]
        },
        {
            "id": "387969ae-7401-4af5-89b1-3fc2406f757f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8890e2df-036d-4906-adb7-7a7c06543976",
            "compositeImage": {
                "id": "b663dfc6-c364-485e-a231-bcdeff6ac280",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "387969ae-7401-4af5-89b1-3fc2406f757f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d64643c-7917-4396-a1d5-7d581b0c9365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "387969ae-7401-4af5-89b1-3fc2406f757f",
                    "LayerId": "100f299b-62a2-4c4d-8c23-ef357f8ec508"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "100f299b-62a2-4c4d-8c23-ef357f8ec508",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8890e2df-036d-4906-adb7-7a7c06543976",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 27,
    "yorig": 46
}