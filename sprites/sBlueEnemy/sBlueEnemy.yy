{
    "id": "7f5ecedb-bd39-4c7a-a6e5-85a305f9c63e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlueEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "frames": [
        {
            "id": "21106d1e-69fe-4d21-b153-7bbcdb993028",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f5ecedb-bd39-4c7a-a6e5-85a305f9c63e",
            "compositeImage": {
                "id": "c6053552-b003-428c-8dde-b7c4206682a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21106d1e-69fe-4d21-b153-7bbcdb993028",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c536233-73fd-4401-9ca8-ae5d3847a301",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21106d1e-69fe-4d21-b153-7bbcdb993028",
                    "LayerId": "d56ccf3c-a8d1-492c-959a-22f76f59d6b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 44,
    "layers": [
        {
            "id": "d56ccf3c-a8d1-492c-959a-22f76f59d6b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f5ecedb-bd39-4c7a-a6e5-85a305f9c63e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 22
}