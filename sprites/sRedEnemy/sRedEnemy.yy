{
    "id": "9dd8ce17-8eed-4cdd-945b-c149c23d31b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRedEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "frames": [
        {
            "id": "da5d1ea8-a68a-4988-a95c-78f73cd9db3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9dd8ce17-8eed-4cdd-945b-c149c23d31b7",
            "compositeImage": {
                "id": "909fab1c-313b-4ef3-8c4c-d97c0f36a310",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da5d1ea8-a68a-4988-a95c-78f73cd9db3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "937bbc41-08c8-43b2-b904-128c6553ccd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da5d1ea8-a68a-4988-a95c-78f73cd9db3f",
                    "LayerId": "90e3b315-4364-44d5-8dad-42ae650ce016"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 44,
    "layers": [
        {
            "id": "90e3b315-4364-44d5-8dad-42ae650ce016",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9dd8ce17-8eed-4cdd-945b-c149c23d31b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 22
}