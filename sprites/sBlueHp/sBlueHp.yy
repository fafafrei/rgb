{
    "id": "dad54724-93e7-4358-a207-c8de6e1c70a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlueHp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8e5786bd-9717-48e8-9add-3c67d17935e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dad54724-93e7-4358-a207-c8de6e1c70a6",
            "compositeImage": {
                "id": "5d1d3b7a-e685-413e-8611-8d8c8f620c38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e5786bd-9717-48e8-9add-3c67d17935e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb176274-e9c2-4eca-83fc-d9861491183c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e5786bd-9717-48e8-9add-3c67d17935e3",
                    "LayerId": "f0fd8c42-8748-4668-b3e5-f8d78663c2f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "f0fd8c42-8748-4668-b3e5-f8d78663c2f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dad54724-93e7-4358-a207-c8de6e1c70a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 15,
    "yorig": 14
}