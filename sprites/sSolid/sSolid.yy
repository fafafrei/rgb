{
    "id": "4c248f53-122a-461e-bd95-37b7499b80e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSolid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6057df48-d701-4c2c-a22e-6aa6bd9711ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c248f53-122a-461e-bd95-37b7499b80e5",
            "compositeImage": {
                "id": "6347f23e-a088-480a-b242-12cddcb45aee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6057df48-d701-4c2c-a22e-6aa6bd9711ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7587a182-964a-4920-be1e-aa9b0aae4bd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6057df48-d701-4c2c-a22e-6aa6bd9711ea",
                    "LayerId": "af40f89a-fcdd-4d54-8add-3270d63b8f11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "af40f89a-fcdd-4d54-8add-3270d63b8f11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c248f53-122a-461e-bd95-37b7499b80e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 61,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}