{
    "id": "ccf11eec-988e-4457-8f62-418c0bb45b9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBlueGun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2b8fab74-9ed2-4aa8-bc31-f6862fa52f95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccf11eec-988e-4457-8f62-418c0bb45b9d",
            "compositeImage": {
                "id": "c83be45f-fe00-43ad-8664-007bb07c199c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b8fab74-9ed2-4aa8-bc31-f6862fa52f95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6aa7e2e-cd45-46ad-9fc9-749a28154cba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b8fab74-9ed2-4aa8-bc31-f6862fa52f95",
                    "LayerId": "cb32e33b-6a78-4ef4-985d-f7da7f186b72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "cb32e33b-6a78-4ef4-985d-f7da7f186b72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccf11eec-988e-4457-8f62-418c0bb45b9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 2,
    "yorig": 4
}