{
    "id": "339921e8-913e-4a4c-9640-5b6a7fd04e17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGreenEnemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a980183a-03e8-4f8a-b190-8030094baa6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "339921e8-913e-4a4c-9640-5b6a7fd04e17",
            "compositeImage": {
                "id": "31df01dd-3c14-425c-87df-8dc2cc71cf33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a980183a-03e8-4f8a-b190-8030094baa6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ba360a2-95c0-4b71-92e2-03a5d35fe5ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a980183a-03e8-4f8a-b190-8030094baa6f",
                    "LayerId": "5ebd5839-0bd1-432f-ab61-f0b9977967ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 44,
    "layers": [
        {
            "id": "5ebd5839-0bd1-432f-ab61-f0b9977967ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "339921e8-913e-4a4c-9640-5b6a7fd04e17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 22
}