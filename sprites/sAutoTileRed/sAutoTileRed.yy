{
    "id": "152bdb49-5652-4fe1-a302-fa01c000e189",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAutoTileRed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2d4816fd-0c4c-4f06-9bde-bccc2a4ef248",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "152bdb49-5652-4fe1-a302-fa01c000e189",
            "compositeImage": {
                "id": "5aa11707-bbc8-4243-8c64-0f319e0daa1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d4816fd-0c4c-4f06-9bde-bccc2a4ef248",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c1c9026-e7e8-4454-8cbc-618742bf2aa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d4816fd-0c4c-4f06-9bde-bccc2a4ef248",
                    "LayerId": "61b4b72e-aee3-47a7-8ec4-3956a3586e9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "61b4b72e-aee3-47a7-8ec4-3956a3586e9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "152bdb49-5652-4fe1-a302-fa01c000e189",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}