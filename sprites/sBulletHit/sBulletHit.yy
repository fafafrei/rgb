{
    "id": "38263b1a-243c-40bb-b064-f45c51556f5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBulletHit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "5e91d849-2dff-4d11-829e-cf434232239e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38263b1a-243c-40bb-b064-f45c51556f5e",
            "compositeImage": {
                "id": "5389d89b-198b-4e08-8de5-771adf00af69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e91d849-2dff-4d11-829e-cf434232239e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8f245f7-271d-4377-9bbb-c39ee7598780",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e91d849-2dff-4d11-829e-cf434232239e",
                    "LayerId": "e7de25e6-7300-4e6c-b496-2e2dd9f4dca1"
                }
            ]
        },
        {
            "id": "f46553d7-a0b9-4552-b7e3-a4f63b46296b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38263b1a-243c-40bb-b064-f45c51556f5e",
            "compositeImage": {
                "id": "4339d9f3-91b6-4a83-b8fc-78b66baf4df2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f46553d7-a0b9-4552-b7e3-a4f63b46296b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "088c37bf-851d-430a-80b7-68004d0f2d6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f46553d7-a0b9-4552-b7e3-a4f63b46296b",
                    "LayerId": "e7de25e6-7300-4e6c-b496-2e2dd9f4dca1"
                }
            ]
        },
        {
            "id": "05b8a9fc-12e5-4f48-811b-63591c112d7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38263b1a-243c-40bb-b064-f45c51556f5e",
            "compositeImage": {
                "id": "eec56085-20d1-447e-8e3a-ab8330d3871e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05b8a9fc-12e5-4f48-811b-63591c112d7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da83a7a0-91dc-49f0-bb0e-6a97dfeb1fea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05b8a9fc-12e5-4f48-811b-63591c112d7f",
                    "LayerId": "e7de25e6-7300-4e6c-b496-2e2dd9f4dca1"
                }
            ]
        },
        {
            "id": "2794a023-c675-48ca-bb4b-88d750229b5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38263b1a-243c-40bb-b064-f45c51556f5e",
            "compositeImage": {
                "id": "a3408e35-2c64-4bdf-b452-2cab71739912",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2794a023-c675-48ca-bb4b-88d750229b5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2255f060-5505-49d3-a1af-91e14f4ae131",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2794a023-c675-48ca-bb4b-88d750229b5c",
                    "LayerId": "e7de25e6-7300-4e6c-b496-2e2dd9f4dca1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "e7de25e6-7300-4e6c-b496-2e2dd9f4dca1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38263b1a-243c-40bb-b064-f45c51556f5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 14
}