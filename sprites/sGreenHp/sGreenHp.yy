{
    "id": "4f841bdd-0813-445a-a559-bb179001fba8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGreenHp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ca2016c0-66a0-4787-b0af-c21f0b4cd50f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f841bdd-0813-445a-a559-bb179001fba8",
            "compositeImage": {
                "id": "303f991f-773b-4165-9600-4359c66da2bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca2016c0-66a0-4787-b0af-c21f0b4cd50f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f82a7d6-69f3-4f46-bcac-d8eabdd568a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca2016c0-66a0-4787-b0af-c21f0b4cd50f",
                    "LayerId": "7130c20e-c317-4410-962a-dab9b67bb962"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "7130c20e-c317-4410-962a-dab9b67bb962",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f841bdd-0813-445a-a559-bb179001fba8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 15,
    "yorig": 14
}