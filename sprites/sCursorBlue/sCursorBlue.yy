{
    "id": "f567101c-46f9-438e-93b2-361914a3e26b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCursorBlue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "209ce603-9e2a-4e4c-b757-6d76e4a957c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f567101c-46f9-438e-93b2-361914a3e26b",
            "compositeImage": {
                "id": "6cfb2f46-cfc2-49c5-91d0-4a9c24b28c65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "209ce603-9e2a-4e4c-b757-6d76e4a957c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c141f465-44ba-4e1c-bdf0-aaa247916a56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "209ce603-9e2a-4e4c-b757-6d76e4a957c7",
                    "LayerId": "bc653556-de91-4aab-8fc3-59a593c5b5ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bc653556-de91-4aab-8fc3-59a593c5b5ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f567101c-46f9-438e-93b2-361914a3e26b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}