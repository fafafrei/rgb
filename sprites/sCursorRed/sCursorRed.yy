{
    "id": "c9a75afd-6345-4c6f-ab78-e85fe93f621a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCursorRed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "02be993f-2253-428e-ac50-67d0bd776ae9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9a75afd-6345-4c6f-ab78-e85fe93f621a",
            "compositeImage": {
                "id": "36ff4df1-3803-4427-8a1f-3e2319350037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02be993f-2253-428e-ac50-67d0bd776ae9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59b551b7-4b8a-4a41-93cc-8d81fc911576",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02be993f-2253-428e-ac50-67d0bd776ae9",
                    "LayerId": "5fe6f9be-75aa-41c9-bd9a-3798ca781b25"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5fe6f9be-75aa-41c9-bd9a-3798ca781b25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9a75afd-6345-4c6f-ab78-e85fe93f621a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}