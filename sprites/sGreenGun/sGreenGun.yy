{
    "id": "31c3d2ec-8a6a-4840-95a4-43562f0243be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGreenGun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1223bf29-137f-457a-b205-b31887ba9314",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31c3d2ec-8a6a-4840-95a4-43562f0243be",
            "compositeImage": {
                "id": "55dad6a7-33ba-46f5-abe9-981bcd2a5f28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1223bf29-137f-457a-b205-b31887ba9314",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75e338e4-1aa1-48bb-9608-64d0689f3e34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1223bf29-137f-457a-b205-b31887ba9314",
                    "LayerId": "0f83576c-c552-4734-acf5-febc69a490e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "0f83576c-c552-4734-acf5-febc69a490e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31c3d2ec-8a6a-4840-95a4-43562f0243be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 2,
    "yorig": 4
}