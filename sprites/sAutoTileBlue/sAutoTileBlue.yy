{
    "id": "9b131d41-8f98-43c5-ac19-923d1dc19313",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAutoTileBlue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "efea158e-3505-487f-a130-1ccd17f422e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b131d41-8f98-43c5-ac19-923d1dc19313",
            "compositeImage": {
                "id": "1f5818bd-4e68-4dd3-80f1-926f2afa9e91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efea158e-3505-487f-a130-1ccd17f422e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da93ad67-23b3-4d68-a702-69346e1165f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efea158e-3505-487f-a130-1ccd17f422e7",
                    "LayerId": "84a23f7a-5a82-438e-8e11-466a47e759a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "84a23f7a-5a82-438e-8e11-466a47e759a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b131d41-8f98-43c5-ac19-923d1dc19313",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}