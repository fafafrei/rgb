{
    "id": "95192899-4e49-4c26-b4aa-b90bb39fabd5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRedGun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "75a3554e-c1b6-4b8d-8da5-ae8d1ffaa1e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95192899-4e49-4c26-b4aa-b90bb39fabd5",
            "compositeImage": {
                "id": "3bcd4091-f17d-4210-9f62-fbc383616ddf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75a3554e-c1b6-4b8d-8da5-ae8d1ffaa1e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7b2b61e-d25a-4762-8c69-b9d86621666f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75a3554e-c1b6-4b8d-8da5-ae8d1ffaa1e6",
                    "LayerId": "f35e790a-96c6-4da2-8383-ba76977a835d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "f35e790a-96c6-4da2-8383-ba76977a835d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95192899-4e49-4c26-b4aa-b90bb39fabd5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 2,
    "yorig": 4
}