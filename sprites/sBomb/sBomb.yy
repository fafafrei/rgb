{
    "id": "9e1234ec-2af9-4f5a-8283-d959e5d83265",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBomb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1ea9eed5-9a5f-4cdf-ae20-d19ac2065dce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e1234ec-2af9-4f5a-8283-d959e5d83265",
            "compositeImage": {
                "id": "db846418-00ef-4ca5-95da-667317ec0f32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ea9eed5-9a5f-4cdf-ae20-d19ac2065dce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abc9a695-5e96-44ed-8534-1b63503d4ff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ea9eed5-9a5f-4cdf-ae20-d19ac2065dce",
                    "LayerId": "58316cc2-9d74-4bca-848b-8d004d0ea93f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "58316cc2-9d74-4bca-848b-8d004d0ea93f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e1234ec-2af9-4f5a-8283-d959e5d83265",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 8
}