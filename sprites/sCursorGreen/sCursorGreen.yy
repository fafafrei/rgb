{
    "id": "299dd554-2d8f-4b53-9050-adb27a68f51e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCursorGreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "704729b5-cc02-4ab7-a140-8f553ddc5984",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "299dd554-2d8f-4b53-9050-adb27a68f51e",
            "compositeImage": {
                "id": "7f4856c0-41cb-4cb0-b28b-63081bd2eefd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "704729b5-cc02-4ab7-a140-8f553ddc5984",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70a27a2d-ab59-4d18-9a37-f5067621b167",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "704729b5-cc02-4ab7-a140-8f553ddc5984",
                    "LayerId": "2a9c694e-8fd9-4514-b74d-2e14d85936fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2a9c694e-8fd9-4514-b74d-2e14d85936fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "299dd554-2d8f-4b53-9050-adb27a68f51e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}