{
    "id": "3b1de6e2-d53f-4ee0-baba-7d7cb0a40025",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAutoTileGreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "eb1f0725-0605-4afe-bb29-fa944edd614c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b1de6e2-d53f-4ee0-baba-7d7cb0a40025",
            "compositeImage": {
                "id": "fdcd57c8-1b84-477e-b7f0-c56743fc0a19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb1f0725-0605-4afe-bb29-fa944edd614c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8c9a1ed-1d9b-45e3-af52-d24019598de7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb1f0725-0605-4afe-bb29-fa944edd614c",
                    "LayerId": "586f57b6-5eea-4db7-a644-bd97cbba8d1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "586f57b6-5eea-4db7-a644-bd97cbba8d1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b1de6e2-d53f-4ee0-baba-7d7cb0a40025",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}