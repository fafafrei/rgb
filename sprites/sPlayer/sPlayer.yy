{
    "id": "54dfcdfe-14ad-4102-90b4-88110ac54413",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "bfce8ecc-eec1-49ef-896d-3aff25e700b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54dfcdfe-14ad-4102-90b4-88110ac54413",
            "compositeImage": {
                "id": "48cfa635-a630-4c0a-be5b-be87389fb654",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfce8ecc-eec1-49ef-896d-3aff25e700b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45b24112-c55a-4394-8281-9aef51c530bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfce8ecc-eec1-49ef-896d-3aff25e700b2",
                    "LayerId": "b50bee18-346b-47fd-ba06-888a4e987028"
                }
            ]
        },
        {
            "id": "b0043dbb-0280-40ac-9d0b-e15877c1cba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54dfcdfe-14ad-4102-90b4-88110ac54413",
            "compositeImage": {
                "id": "e3572784-34aa-4c11-8f1d-e25eec78b349",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0043dbb-0280-40ac-9d0b-e15877c1cba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03bd37f5-2b3e-4dec-b513-96b74fdffe8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0043dbb-0280-40ac-9d0b-e15877c1cba3",
                    "LayerId": "b50bee18-346b-47fd-ba06-888a4e987028"
                }
            ]
        },
        {
            "id": "981f7784-704a-425b-ac23-c0b796e973d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54dfcdfe-14ad-4102-90b4-88110ac54413",
            "compositeImage": {
                "id": "0cc8c7c6-05de-499e-9c21-9ba3c2ebd665",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "981f7784-704a-425b-ac23-c0b796e973d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26b5c8b7-9da4-4deb-8e72-4b1de960a468",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "981f7784-704a-425b-ac23-c0b796e973d4",
                    "LayerId": "b50bee18-346b-47fd-ba06-888a4e987028"
                }
            ]
        },
        {
            "id": "aef01c9e-3017-4bc8-8fb0-171f9a779880",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54dfcdfe-14ad-4102-90b4-88110ac54413",
            "compositeImage": {
                "id": "d3ae4cc8-9f21-4c52-baf9-24103b6b1509",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aef01c9e-3017-4bc8-8fb0-171f9a779880",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e437a3d-5be4-439a-92b5-06de5209e9c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aef01c9e-3017-4bc8-8fb0-171f9a779880",
                    "LayerId": "b50bee18-346b-47fd-ba06-888a4e987028"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b50bee18-346b-47fd-ba06-888a4e987028",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54dfcdfe-14ad-4102-90b4-88110ac54413",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}