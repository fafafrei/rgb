mp_linear_step_object(oPlayer.x, oPlayer.y, 2, oSolid);
//var dir = point_direction(x, y, oPlayer.x, oPlayer.y);
//spd[h] = lengthdir_x(maxSpd, dir);
//spd[v] = lengthdir_y(maxSpd, dir);
move(spd, false);

//Push Force
move(spdPush, false);
if (!place_meeting(x, y, oEnemy)) {
	spdPush[h] = lerp(spdPush[h], 0, 0.1);
	spdPush[v] = lerp(spdPush[v], 0, 0.1);
}

if (distance_to_object(oPlayer) < 48) state = ATTACK;