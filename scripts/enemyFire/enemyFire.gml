/// @description Attack State
if (instance_exists(oPlayer)) {
	if (distance_to_object(oPlayer) >= 48) state = MOVEMENT;

	/// Shoot
	if (alarm[0] <= 0) {
		audio_play_sound(choose(shoot1, shoot2, shoot4, shoot4), 10, false);
		var dir = point_direction(x, y, oPlayer.x, oPlayer.y-oPlayer.sprite_height/2);
		var xOffset = lengthdir_x(25, dir);
		var yOffset = lengthdir_y(25, dir); 

		var bullet = instance_create_layer(x + xOffset, y + yOffset, "InstancesTop", argument0);
		bullet.direction = dir;
		bullet.image_angle = dir;
		alarm[0] = bulletCooldown;
	} 
}
