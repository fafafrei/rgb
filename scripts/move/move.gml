///@param spd
var _spd = argument0;
var s = oSolid;
if (argument1) s = oSolidPar;

if (place_meeting(x + _spd[h], y, s)) {
	while (!place_meeting(x + sign(_spd[h]), y, s)) {
		x += sign(_spd[h]);
	}
	_spd[@ h] = 0;
}
x +=_spd[h];

if (place_meeting(x, y + _spd[v], s)) {
	while (!place_meeting(x, y  + sign(_spd[v]), s)) {
		y += sign(_spd[v]);
	}
	_spd[@ v] = 0;
}
y += _spd[v]