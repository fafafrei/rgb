var halfWidth = sprite_width/2;

if (x > room_width + halfWidth) {
	x = -halfWidth;
}

if (x < -halfWidth) {
	x = room_width + halfWidth;
}

if (y > room_height + halfWidth) {
	y = -halfWidth;
}

if (y < -halfWidth) {
	y = room_height + halfWidth;
}