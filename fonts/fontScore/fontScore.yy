{
    "id": "63d765dc-8862-46dd-9852-64686cfd4758",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fontScore",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Yu Gothic UI",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f329bc56-7b0b-4b5c-a1fa-a66b4d07d770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 65,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 365,
                "y": 270
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "aec606d4-6aa6-4e5a-9215-c1bb5c3dad5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 51,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 435,
                "y": 270
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "bc7c0870-9662-41bb-b9eb-fc8c13b5d739",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 29,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 449,
                "y": 270
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2a4b6b9a-8966-4fd3-ba02-dd9b06006b34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 51,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 199,
                "y": 69
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "02c4bb53-dd81-4b9d-ae88-0ed27adf38d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 53,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 124,
                "y": 136
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5a873ad2-372f-4317-8f9a-86105af06b9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 51,
                "offset": 0,
                "shift": 46,
                "w": 46,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b9260a8d-a149-483b-83c7-5de03370ed2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 51,
                "offset": 1,
                "shift": 38,
                "w": 38,
                "x": 314,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0cb0ca67-c8e7-40e1-9854-fc221ecf9159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 29,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 79,
                "y": 337
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "76f6f8d2-fdaf-423a-b88c-92706c9ab7bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 58,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 331,
                "y": 270
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "cd6bfeda-3303-4f67-92ca-c6219a243a61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 58,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 348,
                "y": 270
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "76854a1f-4410-46dc-a3c1-3db3edfb9cbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 38,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 305,
                "y": 270
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "77cd7bd9-f0c1-42ea-90f0-6432f8fac48e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 46,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 407,
                "y": 203
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0dc91b52-d219-4e99-9afc-608ef20d0f76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 58,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 409,
                "y": 270
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "2176904c-8759-473d-8d99-63cbc758ce1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 42,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 485,
                "y": 270
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "39594efb-fb6b-40e3-bcba-4d890ff49ef9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 51,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 2,
                "y": 337
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "4e57781b-6a67-4959-9c17-80002170701c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 51,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 164,
                "y": 270
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "0289e790-29b3-4bb2-8e5d-3ae74e78c089",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 51,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 323,
                "y": 136
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "fdb10e1f-ff68-4f50-ba36-1940c58b4f17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 51,
                "offset": 1,
                "shift": 28,
                "w": 22,
                "x": 478,
                "y": 203
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "108e31de-702b-47e4-a7dc-04351e9f160d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 51,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 379,
                "y": 136
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "62e909cd-2afc-4a22-929d-b121fb0f8848",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 51,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 435,
                "y": 136
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5adb454a-b995-414e-afa5-4a0fed530d22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 51,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 94,
                "y": 136
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e3e8095a-689a-41a4-8537-962106c99249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 51,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 225,
                "y": 203
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c63230a5-3bc8-4165-9396-7792722ff2e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 51,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 170,
                "y": 203
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2481c4d8-1453-4d6a-a711-aaa00e793d00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 51,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 114,
                "y": 203
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "71b64d07-cb9d-41fc-bf3d-20a4a3a828ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 51,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 86,
                "y": 203
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "956b7def-d085-4504-8175-2f4e3debd3cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 51,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 58,
                "y": 203
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c9b83473-6aba-4bca-84d6-1b303277c7e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 51,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 26,
                "y": 337
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "0941bbac-76cb-468f-89cb-49850a5290c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 58,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 422,
                "y": 270
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a971c809-5e30-49cf-bbc3-12574586ea27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 47,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 330,
                "y": 203
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "93e76423-4be2-4695-b38e-362c5dfab6ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 43,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 68,
                "y": 270
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "1590512d-3bc1-4303-89de-d23097a839fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 47,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 303,
                "y": 203
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6171b5d8-4750-4f67-b7c2-b488ea9db95b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 51,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 277,
                "y": 203
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d1eb208f-ae6b-4bf1-917d-377c4c3ac18e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 55,
                "offset": 1,
                "shift": 43,
                "w": 41,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9c3c2923-c194-4284-91ff-25f73cc73820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 51,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 417,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b838248e-2636-48cf-9153-3df3ddb3496f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 51,
                "offset": 3,
                "shift": 32,
                "w": 27,
                "x": 210,
                "y": 136
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "0479b362-3ac4-4639-970a-b9d233ec96ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 51,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 64,
                "y": 136
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7e4bcbfb-5508-42bf-a134-5b8612d624d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 51,
                "offset": 3,
                "shift": 35,
                "w": 30,
                "x": 392,
                "y": 69
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2a42b851-c910-4666-aff4-f328fa612bcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 51,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 118,
                "y": 270
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e600865c-9bf7-4944-992a-6573434a1821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 51,
                "offset": 3,
                "shift": 26,
                "w": 22,
                "x": 454,
                "y": 203
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d409c7bb-17e6-40eb-9e87-cf44177e9133",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 51,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 266,
                "y": 69
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "0f2e416a-1e81-4f45-9d2b-7bf7ea7b7259",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 51,
                "offset": 3,
                "shift": 37,
                "w": 31,
                "x": 299,
                "y": 69
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "aca5df8b-d37a-4427-a1b6-6d41b408af29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 51,
                "offset": 3,
                "shift": 17,
                "w": 11,
                "x": 472,
                "y": 270
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "215c01d6-2c34-497c-8b9e-4d692a97c64f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 63,
                "offset": -4,
                "shift": 17,
                "w": 18,
                "x": 434,
                "y": 203
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "28310df9-9763-4b39-8dae-9c9fe69e9740",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 51,
                "offset": 3,
                "shift": 33,
                "w": 31,
                "x": 233,
                "y": 69
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f49f5556-5776-434d-aa6f-bca6c4ae8d7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 51,
                "offset": 3,
                "shift": 28,
                "w": 24,
                "x": 251,
                "y": 203
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "eae893eb-bcbc-47b9-9bc3-0890ada20b62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 51,
                "offset": 3,
                "shift": 46,
                "w": 40,
                "x": 272,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "978b9723-a5cf-4d14-8a40-da725db838f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 51,
                "offset": 3,
                "shift": 40,
                "w": 34,
                "x": 2,
                "y": 69
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "97999690-0c66-4e3f-bf24-9caf5d204df1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 51,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 38,
                "y": 69
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "87a9127b-0cdb-4242-b200-28c03c568a4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 51,
                "offset": 3,
                "shift": 30,
                "w": 26,
                "x": 239,
                "y": 136
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "923de65b-33e9-493f-969c-a4edd14a04b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 59,
                "offset": 2,
                "shift": 38,
                "w": 35,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "96cc9bca-f3a5-4e4e-9cd4-d5c17963632f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 51,
                "offset": 3,
                "shift": 32,
                "w": 30,
                "x": 424,
                "y": 69
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "1bd9e0d4-921f-4e89-88b1-772845a6b9d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 51,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 198,
                "y": 203
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c1e3d3be-05d5-44ce-8a05-b5aa12bffa80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 51,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 267,
                "y": 136
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "967a91ed-30b0-4793-9a65-f31c852958b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 51,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 360,
                "y": 69
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6cfca5cc-bc04-4e77-b3b5-b66fe3d59a11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 51,
                "offset": 0,
                "shift": 33,
                "w": 33,
                "x": 74,
                "y": 69
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "9237c0c7-8fa5-483b-bf1a-bd0415a19534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 51,
                "offset": 0,
                "shift": 50,
                "w": 50,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7a2673f0-342a-4170-9985-186178d4f314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 51,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "903ee4d5-e4ac-4d64-9a16-b71ac8c73306",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 51,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 165,
                "y": 69
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a0e046e1-2296-4e10-914f-4ec34db4f539",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 51,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 181,
                "y": 136
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f8ed32d4-544b-43ff-96a3-c5f6dcc3cdda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 59,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 394,
                "y": 270
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3fd0295e-bdfd-4620-858f-4274cf30b3c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 51,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 141,
                "y": 270
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5a9a1aa9-1c92-4ac0-b4d7-03808d64890f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 59,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 379,
                "y": 270
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "fd6a4c23-7f69-4fd5-9ab2-36d573fadc04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 39,
                "offset": -1,
                "shift": 25,
                "w": 27,
                "x": 187,
                "y": 270
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a8735fb3-2327-41eb-9c70-6b99323a38a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 60,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 332,
                "y": 69
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d3fb6282-08c5-4162-989c-4ba181e7144f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 6,
                "shift": 29,
                "w": 17,
                "x": 60,
                "y": 337
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c0b6d3b8-0446-4ba7-92e0-9dff33e032f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 51,
                "offset": 1,
                "shift": 30,
                "w": 26,
                "x": 351,
                "y": 136
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5b2971f7-40d1-44f1-a745-a85f4aadb9c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 51,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 463,
                "y": 136
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8eee6a55-f185-4291-8f7c-990f36c8c122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 51,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 382,
                "y": 203
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "66829a0f-e44a-4d56-b6c3-fcc84d3089e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 51,
                "offset": 2,
                "shift": 31,
                "w": 26,
                "x": 30,
                "y": 203
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8a398732-e4b7-48fd-b227-4d5697a2fa17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 51,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 407,
                "y": 136
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3039b3be-e3b8-4dc5-9257-7ba5c05b0829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 51,
                "offset": 1,
                "shift": 20,
                "w": 21,
                "x": 95,
                "y": 270
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5052ed73-567b-4238-8836-c19774e5453d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 63,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 386,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "332af69a-fd3f-406e-9621-68461a68c152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 51,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 142,
                "y": 203
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1862a3fe-b76b-415e-ad2f-9c8bc4e408a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 51,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 14,
                "y": 337
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "84c2809d-3bbe-4412-aabb-abc380b65775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 63,
                "offset": -3,
                "shift": 16,
                "w": 16,
                "x": 260,
                "y": 270
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "9bfb619a-50a6-40e8-b0d0-b81e5105c2c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 51,
                "offset": 3,
                "shift": 32,
                "w": 29,
                "x": 2,
                "y": 136
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "766990c7-67ce-42b0-947d-15a21a5acc45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 51,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 38,
                "y": 337
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8d47a619-75f0-47da-b961-a416b2e02593",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 51,
                "offset": 3,
                "shift": 48,
                "w": 42,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c77c161d-0fdf-4b44-818e-5ff089637bc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 51,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 2,
                "y": 203
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c816c7a1-b61f-47f9-88f3-0d664c0b3fea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 51,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 152,
                "y": 136
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "92937a77-31a9-4fc1-89e4-54643f3cfd00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 63,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 109,
                "y": 69
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6617e4b0-7a9f-4183-a129-587f1ac4aa33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 63,
                "offset": 2,
                "shift": 31,
                "w": 26,
                "x": 137,
                "y": 69
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "5e89f5a4-d71a-42de-a164-12203b297158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 51,
                "offset": 3,
                "shift": 23,
                "w": 20,
                "x": 216,
                "y": 270
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ea0b94d7-824f-4849-af1f-28f5f1b22642",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 51,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 357,
                "y": 203
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a40b7813-b3a9-4c91-9be7-b8d00d32f259",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 51,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 238,
                "y": 270
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f08502b7-1d81-432e-929f-810f825e7989",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 51,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 295,
                "y": 136
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0b3ebb9d-9460-4699-b7bd-ae420934806c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 51,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 33,
                "y": 136
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d9e4184b-744a-4a70-b9ac-8fe32260feb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 51,
                "offset": 0,
                "shift": 44,
                "w": 44,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "faf56eee-e31f-4812-9521-b361ebf30f24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 51,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 456,
                "y": 69
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "455a6903-59ef-4d49-87ec-774ffe6812c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 63,
                "offset": -1,
                "shift": 29,
                "w": 30,
                "x": 354,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "3b00dc03-d1d8-4508-bbf1-f24a0cfc7809",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 51,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 2,
                "y": 270
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d604532c-5a31-426c-9b60-9548f5f277ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 59,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 47,
                "y": 270
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "87ea3109-3c1b-4606-9b4a-a9218ea900c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 63,
                "offset": 8,
                "shift": 24,
                "w": 8,
                "x": 50,
                "y": 337
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b5e2ef99-d957-4571-8fe9-825da9dbb5d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 59,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 26,
                "y": 270
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "e2b3502e-892c-4edd-ab64-733e88b975cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 39,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 278,
                "y": 270
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "e5d3e7f0-e9f0-4166-a6e9-7f2a630753c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 65
        },
        {
            "id": "364415e2-4a6a-4e3c-a72c-f0fcb9ea4a73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 84
        },
        {
            "id": "223b9171-48d1-4ade-ad41-2ed9f9e7f38d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 86
        },
        {
            "id": "6be57843-ccd6-4522-9cf1-4b470bf2d233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 87
        },
        {
            "id": "7b3c6a69-f418-4b9a-a2f1-0736599f5691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 97
        },
        {
            "id": "a26e7dd6-de97-4762-9010-582a1981043b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 99
        },
        {
            "id": "1b17554f-6a5c-4b20-8452-f85e30ab024e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 100
        },
        {
            "id": "fc69d3fc-e9b6-48b6-9d8e-d59732425e09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 101
        },
        {
            "id": "cbf2a2e6-27e6-4ccf-a1df-1889605e342c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 103
        },
        {
            "id": "3e180b43-34ed-4e42-9e66-c24fa0675b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 109
        },
        {
            "id": "8662f76a-d39c-4b2d-b0fb-c27c5836036a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 110
        },
        {
            "id": "71f6a368-2bfa-4eb4-814e-d6aefaf81f79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 111
        },
        {
            "id": "baf4faf5-4721-49ba-ac7a-828aae1bcf7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 112
        },
        {
            "id": "7222575a-cce5-47d0-94f8-467368530ec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 113
        },
        {
            "id": "10bf80a2-89ed-4c8f-b542-f4e4b6d21081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 114
        },
        {
            "id": "09dd5e33-2ace-4771-a2a6-3fd5cc3e4c2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 115
        },
        {
            "id": "d4aeddac-7810-4af2-bb2e-d6db5aaefe92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 117
        },
        {
            "id": "9cb3758f-eac4-4663-b663-2ea7e24e84bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 192
        },
        {
            "id": "bea38e74-46f3-4ca6-8529-4f4768d7bef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 193
        },
        {
            "id": "2169ffb6-3ff1-445e-8872-17c6619a5490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 194
        },
        {
            "id": "2b2c920a-464a-428e-88e9-6d8da89c15d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 195
        },
        {
            "id": "98b02882-c5a8-4c44-9480-c86414a22ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 196
        },
        {
            "id": "2fd43268-5636-4a6e-8421-0d1ba48806c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 197
        },
        {
            "id": "39e3b0cd-1d98-4ace-9b01-9c81535e061c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 224
        },
        {
            "id": "bad1e46f-142a-4839-8070-e0faea404f90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 225
        },
        {
            "id": "c521f657-2634-44ad-8e41-d8ee706263ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 226
        },
        {
            "id": "f1333d0b-3414-4f71-b11d-4198f8c48deb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 227
        },
        {
            "id": "c79c1d56-f8c3-45ec-802f-f774b4b69251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 228
        },
        {
            "id": "7938fdec-f7c9-4b61-9dae-c492c2f944b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 229
        },
        {
            "id": "45193692-4d5a-4ae9-ac5b-437bd2163dcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 230
        },
        {
            "id": "50f0e2f1-9a83-41f8-b8c0-d2b390ab9035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 231
        },
        {
            "id": "52a419cb-7144-43a8-a37b-201b5bdef6c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 232
        },
        {
            "id": "7a3f1360-565f-4326-8f34-f23c777bde91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 233
        },
        {
            "id": "a7e273c3-127c-42a0-9fb9-159b90cd5e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 234
        },
        {
            "id": "0e82ce91-1067-45ca-ab6e-06731355b92f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 235
        },
        {
            "id": "bd736e6c-a664-4857-b449-55b03bfc10f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 242
        },
        {
            "id": "68a2551b-0de8-4db9-9fea-72dfef7782f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 243
        },
        {
            "id": "454854fb-d0f3-409e-8bd4-55e408cbf473",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 244
        },
        {
            "id": "65b25a6f-7972-425f-87b7-042037787759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 245
        },
        {
            "id": "714e264e-020e-4e49-aeb5-6f066aaf90e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 246
        },
        {
            "id": "7183950f-e570-4ca1-baac-8d17b51330d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 248
        },
        {
            "id": "65de5eec-1f39-46b3-b4dd-53a5b20d5df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 249
        },
        {
            "id": "74c433b6-6fc5-421d-bfba-49128275013c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 250
        },
        {
            "id": "73c0d67e-53cf-4a02-b3e2-1fcc5cf7a5f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 251
        },
        {
            "id": "2e8b23b6-3ae5-453b-ab8b-8b53abd713ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 252
        },
        {
            "id": "64185876-a1eb-4b4b-b6ad-14e3d61933bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 256
        },
        {
            "id": "0ed9986d-f69c-4567-a301-bf9a9f761f17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 257
        },
        {
            "id": "9eaa615e-f31b-4229-aeff-707d9d78be92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 258
        },
        {
            "id": "50724038-ea7b-47cd-9a70-99481dc50b0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 259
        },
        {
            "id": "c646fe12-672c-4225-a882-b0c93314844e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 260
        },
        {
            "id": "f44d6577-a027-4094-bc94-ea983554ed24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 261
        },
        {
            "id": "72e02c06-30e7-4bd9-bb62-18f98f6b25f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 263
        },
        {
            "id": "a90451da-7cd2-4a1c-8d39-8abd760048b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 265
        },
        {
            "id": "36f6c3b8-b123-4cb8-98ab-3f8308dda240",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 267
        },
        {
            "id": "66d63509-a6a3-4610-9713-59b14bc2b10f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 269
        },
        {
            "id": "a1e77981-429a-41c1-bfa5-f7d3e8f11e21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 271
        },
        {
            "id": "b5f859c7-8cc1-4f78-8043-0ff1e1974f1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 273
        },
        {
            "id": "9cb2d37e-6b4e-483d-a742-42c80e6695d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 275
        },
        {
            "id": "3e098840-9818-442b-bdff-a162a4770310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 277
        },
        {
            "id": "469bb4fd-3050-44c8-bd26-9fc4eb18a688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 279
        },
        {
            "id": "e34dcde8-c7fc-4800-a26b-2bad5ad6b63c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 281
        },
        {
            "id": "94907e3e-f949-4f58-b8f3-7607e9a9d676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 283
        },
        {
            "id": "f6d48afd-0412-4c8d-9c5b-7e5d907579c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 285
        },
        {
            "id": "f66c5891-8ecf-44c6-b44c-d8b409ba981e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 287
        },
        {
            "id": "6a687cf4-a706-4d0d-8824-3b22c6f6db7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 289
        },
        {
            "id": "55b5587b-e5d3-46dd-8b77-0966bffb8485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 291
        },
        {
            "id": "60af911d-c261-4241-8903-5c76c3387629",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 312
        },
        {
            "id": "173a34b1-d7a5-4872-9649-8c09247512e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 324
        },
        {
            "id": "ffe071a3-4093-4c8c-8067-e29c23f43059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 326
        },
        {
            "id": "7496d0ee-6cab-41ee-a20a-94d62c512bda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 331
        },
        {
            "id": "e7a00afd-d8d2-48d1-8d4f-92144f19d010",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 333
        },
        {
            "id": "f20f0c11-243e-40ac-964b-2361932c6738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 335
        },
        {
            "id": "cc82c406-03f5-43af-8f7e-39c253a7e468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 337
        },
        {
            "id": "fff21438-a001-43da-8c27-1805ff1051d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 339
        },
        {
            "id": "2f025ca3-3fb8-4ceb-a188-424dbc848637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 341
        },
        {
            "id": "1c4253ef-9f3b-465c-b25f-c2465e20f01d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 343
        },
        {
            "id": "59df8a61-7196-4579-8dd1-22af0a86987c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 347
        },
        {
            "id": "8020cc49-20ac-4888-890f-0a62bd49f8db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 351
        },
        {
            "id": "ad3feab8-6f45-45f0-984f-327f761c1d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 354
        },
        {
            "id": "3f1f0016-7643-48c1-9a7f-4dcc6132f381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 356
        },
        {
            "id": "471b04f8-4fae-49bb-9bf8-b93538c679b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 361
        },
        {
            "id": "e8de85c7-1ceb-4253-b94b-8087ad54867f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 363
        },
        {
            "id": "e2886733-7e46-4c0e-b4f2-57f9fb8f1d6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 365
        },
        {
            "id": "deeb9206-298f-4598-937b-22d68e8c2179",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 367
        },
        {
            "id": "ad4345aa-da49-43b8-a14e-920d7cc6d135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 369
        },
        {
            "id": "bc79bbc2-d960-4a1c-af8f-87cd39f7b995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 371
        },
        {
            "id": "cb47ecaf-8f44-4f19-8772-30c6623c81e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 372
        },
        {
            "id": "45157e02-391d-488a-ba3a-a63b02a14286",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 417
        },
        {
            "id": "3d4fe34a-b008-40a4-95ea-f97ede8385d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 432
        },
        {
            "id": "ea1c74b1-17f3-48c2-8793-fb7c2cd32f65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 506
        },
        {
            "id": "4863860e-fb2a-49b5-b667-47a6e9739878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 507
        },
        {
            "id": "413d46a2-0002-4a7e-bbe0-57d006ae1098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 509
        },
        {
            "id": "d9946b12-3b53-4329-87b0-00d4fd6eff59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 511
        },
        {
            "id": "0223965c-cad4-4f09-9a82-c77e4b535bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 537
        },
        {
            "id": "fe88b850-4615-410d-8c0f-d783d3b88220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 538
        },
        {
            "id": "0c05672b-d76e-4140-9b6a-13feb79be73c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 902
        },
        {
            "id": "b6a023f1-daa3-458e-bb9a-c2a1d687c51b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 913
        },
        {
            "id": "3452ac46-bd8b-43b0-a3b5-daf1ea2f71f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 916
        },
        {
            "id": "8f917ee7-1d5e-488f-bbb4-e6869cc0af1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 923
        },
        {
            "id": "5d354cc4-7a27-45f2-83cb-9b2f572dc03e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 940
        },
        {
            "id": "6df6ad80-b0b6-4e1e-95cf-73ef44fc2993",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 941
        },
        {
            "id": "290e654e-1c03-4db0-928f-f44cb44bd80b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 942
        },
        {
            "id": "83c4aa80-b4ff-43c0-94c9-3bc9ae478838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 945
        },
        {
            "id": "a2b296f8-7255-44f9-b427-bc8e8b60b230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 948
        },
        {
            "id": "0ddd580b-1904-4038-936c-b677fbf50463",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 949
        },
        {
            "id": "efe884ae-df03-475d-aacb-046457d2693e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 950
        },
        {
            "id": "8fb50190-aa64-4a65-bcb3-1c6f84056527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 951
        },
        {
            "id": "a786c606-3c7c-46fc-8438-ae8de349904e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 954
        },
        {
            "id": "2099d045-f61c-4dc8-948a-820ab17e2542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 956
        },
        {
            "id": "f0ae4509-75fa-4d0f-a825-7a2731abb669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 959
        },
        {
            "id": "0ee3d00c-4cc0-4e8a-9d60-1d16bc482d1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 961
        },
        {
            "id": "81f4cdd3-2d4a-41bb-b01c-efc53a9e4cc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 962
        },
        {
            "id": "63d335d2-17ae-44bb-bde6-3878623cfba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 963
        },
        {
            "id": "62b55446-eb6c-44ff-8e1c-f7b01604401c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 966
        },
        {
            "id": "f92ce92b-ba1f-4059-a477-26107fe93e95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 972
        },
        {
            "id": "0cc6e3de-98dd-4eca-aab8-caa643bf65a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1033
        },
        {
            "id": "49f8c458-ecfb-43c7-a99c-7cf93d0a0717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1040
        },
        {
            "id": "8b1f41f6-00a6-4ec8-98ac-f04309fa2ff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1044
        },
        {
            "id": "37d5f9d1-3da9-4f77-ac9c-34f9901879db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1051
        },
        {
            "id": "0061b1a4-097d-4427-a3da-0014447b017e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1072
        },
        {
            "id": "d1599c07-ddec-4907-a13c-d60396ed7e45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1076
        },
        {
            "id": "35d564f5-5d2b-4c68-9968-5d3bc65dbc02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1077
        },
        {
            "id": "1789f5e0-abbb-45f6-b225-ed3388d1037d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1083
        },
        {
            "id": "4ab689f1-c95c-4494-8239-126df67fda58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1086
        },
        {
            "id": "b22f8467-fb38-4cc7-a3b1-7eaced0b667d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1089
        },
        {
            "id": "0834983e-5fd4-4501-ad31-d1228cf26aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1092
        },
        {
            "id": "e49a3bfd-bcc3-4ffd-af33-a6ee8e635c97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1104
        },
        {
            "id": "e5c6e5ed-ed1f-48f1-a61a-435eca14320e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1105
        },
        {
            "id": "bd689fbc-36f1-4825-97f4-747ac1b79630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1108
        },
        {
            "id": "bacd6b60-0496-41cc-8528-122fe4c0bae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1109
        },
        {
            "id": "8448594d-227a-45fc-9161-53da9ff19f1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1113
        },
        {
            "id": "8ad84290-2bdb-49cd-9faf-65cd9f4d7047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1126
        },
        {
            "id": "3272adcb-bcde-4cd9-b2fd-e6bba3215b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1127
        },
        {
            "id": "53614b30-5d20-4ea2-81cf-46c2b1abe83e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1139
        },
        {
            "id": "70921ebe-1a66-4997-a7e4-51599c505b2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1145
        },
        {
            "id": "e356babd-16de-476a-8d3e-c3cff6323766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1147
        },
        {
            "id": "7c165173-085e-4364-97f7-e16912c1c6b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1149
        },
        {
            "id": "d92d5aa0-9405-4297-b258-0a5cc4d191b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1153
        },
        {
            "id": "7de6018c-b49a-4d89-ae62-df6ad9aeae49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1193
        },
        {
            "id": "d2a5468c-7267-42c3-9248-e93e72b5f413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1195
        },
        {
            "id": "1077c553-4650-4ebf-b337-8f2b666da032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1221
        },
        {
            "id": "ad9b5587-e0f6-4d2e-83f6-291c220fef79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1222
        },
        {
            "id": "b2ca50a1-61d3-4027-abf5-7d9c25e51857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1232
        },
        {
            "id": "d6115838-3171-440c-ba86-244b79ca7678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1233
        },
        {
            "id": "cd2949aa-ea06-4d14-9abe-6806a944e62b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1234
        },
        {
            "id": "6bbc6ba3-66fc-4ab6-a5a3-65e512d3fd2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1235
        },
        {
            "id": "2934885c-13c9-4336-a072-c52ce714da6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1236
        },
        {
            "id": "ee4b1906-0a2c-46af-9d8d-b08f1fc2992e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1237
        },
        {
            "id": "cc75dcfc-fd8b-43ed-ad0f-a295d04aff59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1239
        },
        {
            "id": "d40beca1-30c6-4bfa-8943-8ff09a187b02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1241
        },
        {
            "id": "24c6541d-14ab-4ab8-88b6-337d50f73841",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1243
        },
        {
            "id": "75c94b54-8713-4236-8262-1f1601050f13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1255
        },
        {
            "id": "d2b93087-a4d3-4d54-9903-5dc2a617f3c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1257
        },
        {
            "id": "4c209b76-7673-4614-9c51-3b75b4f22297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1259
        },
        {
            "id": "4908cc2a-4751-41a8-a816-df26bf0b61a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1280
        },
        {
            "id": "3dc076d9-8d80-4de3-a1ab-c43a53718db8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1281
        },
        {
            "id": "a3fa91a6-b5d7-463e-a486-536dc906958e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1282
        },
        {
            "id": "5c6e5c16-a7e3-4af9-821b-d10ffd2b01ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1283
        },
        {
            "id": "489c0956-1f1c-43ef-8ddf-748d52d6f40a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1288
        },
        {
            "id": "167f8892-aee1-4051-b68d-e09f345a6017",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1289
        },
        {
            "id": "41f33978-65ba-4b08-94e2-c4abbc17c5f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1293
        },
        {
            "id": "af98e0c8-802f-4088-a706-ff938a5a9556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1297
        },
        {
            "id": "f9533d46-3174-473a-b501-1f7270e2556d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1298
        },
        {
            "id": "b07bc2aa-b90d-4297-adc5-2eb9e98adb00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 1299
        },
        {
            "id": "3ba915ed-66c8-4ccd-80de-19b23f030b12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7680
        },
        {
            "id": "94321f37-6a61-4ab7-ba64-2eb5539e007f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7681
        },
        {
            "id": "ee2eb197-420d-477a-ae29-2f0f65c5b0ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7743
        },
        {
            "id": "44f29a53-b7d4-4e7c-8381-c6b6d0e04c48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7808
        },
        {
            "id": "ba767b3d-f9be-4e22-99fb-eb355b6004f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7810
        },
        {
            "id": "1d90a66d-c340-4859-ac39-e24d88668f3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 7812
        },
        {
            "id": "c4aaebcc-0474-4a06-bcd1-8b40b48552eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7840
        },
        {
            "id": "2aaea861-bed6-459b-90ee-b25c78b0179d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7841
        },
        {
            "id": "726052f8-330f-43ae-93ed-15694c27c74d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7842
        },
        {
            "id": "6f541b11-74d7-4ab2-8f47-23faf72c2938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7843
        },
        {
            "id": "3fb735dc-ff66-4f05-808d-127b9f40fbfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7844
        },
        {
            "id": "41d2a0c5-dc16-41cd-968e-afd8b92a102b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7845
        },
        {
            "id": "bb2e8993-dcbc-485f-bf2f-7a6acb1a5761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7846
        },
        {
            "id": "999ede19-bb21-48cc-9d22-78c4bb721b66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7848
        },
        {
            "id": "d1211d0c-aa21-46f6-9124-674fe6c57c7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7849
        },
        {
            "id": "73e5deb6-629b-4a98-a2ff-f6c451ce4327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7850
        },
        {
            "id": "efdf88ef-8166-494b-b52c-92882b0fa2f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7851
        },
        {
            "id": "e7380347-c4ac-4bf7-bc4d-9a73fd182218",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7852
        },
        {
            "id": "5fc3a7ef-efd4-4ecf-9d4c-9dccf1b335a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7853
        },
        {
            "id": "b4381484-4a6e-40d8-8691-9869fb6c4a2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7854
        },
        {
            "id": "97cdce35-b05b-45a6-83b2-a367cc301c7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7855
        },
        {
            "id": "574976fa-0983-47da-816d-c861fc6ac9ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7856
        },
        {
            "id": "a4adeda6-d3a6-4f29-ba83-32214433d72f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7857
        },
        {
            "id": "5a9c6a86-979e-4fbd-abab-b948e07f190b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7858
        },
        {
            "id": "e808091d-308e-4b55-938e-9513dbd5594b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7859
        },
        {
            "id": "801b93e9-95a0-4017-bdeb-659d3d018835",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7860
        },
        {
            "id": "58d777a0-a955-4b25-866f-cd8da5b4af32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7861
        },
        {
            "id": "93c781cd-a27e-44fa-b7ef-b3be5b39fa81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7862
        },
        {
            "id": "51364f1b-bc6f-45d9-a9b6-e1b0f0a81c84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 7863
        },
        {
            "id": "e414abf4-2565-4e47-a393-61a65aaca756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7865
        },
        {
            "id": "89059c7c-2c7c-48cf-9844-f592121a8821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7867
        },
        {
            "id": "5f888cd4-d1f2-4f5b-bf0f-3fcf71b30b12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7869
        },
        {
            "id": "2e8b7d44-33b9-41e0-b1b8-f45da7f44e25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7871
        },
        {
            "id": "e21b8c98-1d9d-476e-8781-7ec6726875da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7875
        },
        {
            "id": "2fb08a6d-744b-4242-981c-0b4fe0652663",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7877
        },
        {
            "id": "7db4c5f5-d6b8-489d-b4c7-848e66fc3be1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7879
        },
        {
            "id": "0aa3f90b-d7df-42b1-8b5c-ee5a4093d7e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7885
        },
        {
            "id": "0960ba4c-e079-4023-b7a9-428a386f1255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7887
        },
        {
            "id": "1486bdd9-2b4b-4e4c-914c-159c7866f125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7889
        },
        {
            "id": "0d487838-80f7-4a76-b224-14e4858e158c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7893
        },
        {
            "id": "dda3d674-206c-43cc-aa7f-0543fd9b0b55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7895
        },
        {
            "id": "4b0dfdde-bcf5-4e3c-821e-fec4fbf248e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7897
        },
        {
            "id": "8ee3a185-439d-4068-be72-e85ba9628aa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7899
        },
        {
            "id": "718f901e-2150-406d-a9df-da7ed7d9501d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7901
        },
        {
            "id": "e0dbe05e-856f-407d-b590-ef0cb7c7be72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7903
        },
        {
            "id": "0f2a3924-a0ac-4929-bf15-3eef3b861d4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7905
        },
        {
            "id": "c225d6c2-486a-4eba-bf0e-13e3af574516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 7907
        },
        {
            "id": "fbb15cc4-73f3-4d93-8561-0972f966a484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7909
        },
        {
            "id": "093cc7ab-1fc1-4cac-8e6b-3439b5228343",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7911
        },
        {
            "id": "4af76e0e-2871-482a-82fb-bc4d37e4ee0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7913
        },
        {
            "id": "9e2b92d1-c889-4725-bd07-82f76f8d58ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7915
        },
        {
            "id": "ab097510-00c6-4dfe-99f8-cd7bd1041e58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7917
        },
        {
            "id": "d3aa160a-0170-488f-94ae-dc0381d9f186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7919
        },
        {
            "id": "d818d332-359b-4f91-84d7-c0ddedee20ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7921
        },
        {
            "id": "62b7852b-0b82-49c6-91a5-a56e7bc55c55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 65
        },
        {
            "id": "70f76730-1e15-4fdf-97a4-2f2721fd8526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 84
        },
        {
            "id": "d3191179-8a09-4836-b2fb-a08cff865788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 86
        },
        {
            "id": "875dfdca-c88f-44f8-8de2-b0cb6438320d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 87
        },
        {
            "id": "e165e688-0804-4fcc-ad16-901c73178bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 97
        },
        {
            "id": "d9457712-27c6-46f6-b693-324589ded557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 99
        },
        {
            "id": "4618cbfa-c8d0-4e72-a8ec-bd2704f9e7d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 100
        },
        {
            "id": "48221299-64ba-41db-a58b-f3bb62e6c829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 101
        },
        {
            "id": "09eddaf6-944a-46ff-b832-13b1f8a0315a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 103
        },
        {
            "id": "256abbc0-5239-4e76-93a5-1e78566059d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 109
        },
        {
            "id": "b749318e-63d5-4967-a6ca-329722f8a484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 110
        },
        {
            "id": "93f70b4b-c290-47dd-ad71-63b96728d532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 111
        },
        {
            "id": "137dce7e-99dc-40e6-993f-ec28bf727491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 112
        },
        {
            "id": "bf57e6eb-65fe-4afe-a9f2-99e2f0af09b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 113
        },
        {
            "id": "9ca76b4f-1ee6-4a90-95e0-3e4fea8a394a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 114
        },
        {
            "id": "bdda9973-fda8-4a48-b0ab-25519e71ad81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 115
        },
        {
            "id": "4661335f-bec4-4090-bdee-c5ae9976618d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 117
        },
        {
            "id": "f5703ace-87b6-4de0-aaed-beb1b6532d4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 192
        },
        {
            "id": "808834d2-ec2f-403a-8297-fd1e88853f79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 193
        },
        {
            "id": "c5c9ee3b-3f5f-4ae2-afc9-88cc331627ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 194
        },
        {
            "id": "1c9aca28-b93e-4f5e-9747-3ac0b2f2cbba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 195
        },
        {
            "id": "c2717225-0077-4b6d-b6bc-21bb40cd493f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 196
        },
        {
            "id": "a2995004-c0ef-44f9-8472-d612ce850868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 197
        },
        {
            "id": "777eee2c-e034-46c3-a2f6-7ba46ab8f446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 224
        },
        {
            "id": "31a42cf8-41e1-439d-900e-b8ace445fcd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 225
        },
        {
            "id": "3d6050ea-20cf-4fd5-9214-7ac9eb8b4611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 226
        },
        {
            "id": "260dabf1-3f01-43b0-a5ab-ad4650886fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 227
        },
        {
            "id": "b9390e5d-7fe4-4674-93f8-b3c7afb9cf04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 228
        },
        {
            "id": "877cfcda-9ad5-416b-a804-7c18ad721f9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 229
        },
        {
            "id": "eeda96b2-bebf-4101-b05b-48ef1c4ec36e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 230
        },
        {
            "id": "06682ed8-175c-4fd6-ac0e-eac46a0af7fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 231
        },
        {
            "id": "4c7727d0-84a1-44e9-b4e7-49036a57d551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 232
        },
        {
            "id": "5221141f-717c-471a-9129-9d24b9d2a649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 233
        },
        {
            "id": "291bdb48-12d6-423b-af1c-b9722068a86c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 234
        },
        {
            "id": "d2e2c0ce-ed1e-41a1-8aa6-12c61dddc126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 235
        },
        {
            "id": "506842e7-fce1-46c8-aec5-702669a4e7cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 242
        },
        {
            "id": "deb90b92-2bf9-4347-877d-e027766982b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 243
        },
        {
            "id": "2f5106d8-dc2a-467c-a2ad-d8fb220d50a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 244
        },
        {
            "id": "9354659d-16a4-46e9-a694-3e4ed46399bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 245
        },
        {
            "id": "df3c8b0a-5d1e-4241-b0b0-2b5c9d5c5cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 246
        },
        {
            "id": "fc4b5564-7484-444e-889a-4e3bdf8e4d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 248
        },
        {
            "id": "0e6d0630-df58-43cb-8f49-91978d4eba64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 249
        },
        {
            "id": "0269d383-b10d-4a83-81cd-b9d1eba4bce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 250
        },
        {
            "id": "acc9d227-9553-4c6f-bba1-5025e1d605ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 251
        },
        {
            "id": "a02d28c1-2ead-4bc6-a054-a3836d834dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 252
        },
        {
            "id": "a5bde068-0039-4cd5-bcd7-c83e57b3dfe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 256
        },
        {
            "id": "95b5643a-8a2c-40cf-b53f-15c11449575c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 257
        },
        {
            "id": "8663a029-3442-4bb0-b663-7299838313df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 258
        },
        {
            "id": "c94dd9f6-6965-48f7-afe1-11f1dba21e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 259
        },
        {
            "id": "f0f6b775-f9f5-47b0-9006-f15fea04d443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 260
        },
        {
            "id": "d2c5ebb1-d128-49be-9918-5b880ad909bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 261
        },
        {
            "id": "71425f61-c394-4b07-ba95-e69a8be43d91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 263
        },
        {
            "id": "00facdbd-f34f-4c62-8e27-714ed9fb8f8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 265
        },
        {
            "id": "53bed96b-2db4-41eb-a15d-90b6ee829583",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 267
        },
        {
            "id": "df205cf7-94db-4b56-9bcc-18b9f5f8efc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 269
        },
        {
            "id": "54f8e6a1-da2e-46aa-bd9f-d6c1e6e45d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 271
        },
        {
            "id": "e901a1d1-51d0-4571-8f68-766cbc71b94c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 273
        },
        {
            "id": "3e1dcdda-d19a-46a5-a0f3-ba636da6d4ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 275
        },
        {
            "id": "d955ad53-d4a9-47d6-8da2-793767e8fad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 277
        },
        {
            "id": "35f061f2-ef5a-4730-ae47-9cc5341265b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 279
        },
        {
            "id": "8900681b-a9b7-4151-8416-64d3793d72f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 281
        },
        {
            "id": "56d26b91-d36a-486d-ba67-e2a178601df4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 283
        },
        {
            "id": "7e7065bd-b79b-43a8-921a-deeea8338227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 285
        },
        {
            "id": "276acec6-92b2-4943-9942-e9d7451f588e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 287
        },
        {
            "id": "559c7adc-74b6-448e-94ff-e47c3bea6dc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 289
        },
        {
            "id": "5ac4dfe2-03fa-4c7b-a68e-78e47c842d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 291
        },
        {
            "id": "c799af64-ca4c-487f-91be-01254d4abdce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 312
        },
        {
            "id": "8dae49d4-d4fa-44f5-81ef-017063cef9be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 324
        },
        {
            "id": "24d41717-352a-45ea-a804-be74c4e24905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 326
        },
        {
            "id": "e8d581a7-7af3-44dc-85a2-4df7aa1a7431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 331
        },
        {
            "id": "26d84eaf-a928-43f2-b666-d935f21415f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 333
        },
        {
            "id": "31cdf2e3-fb9a-478e-b4b1-7b8985e2e045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 335
        },
        {
            "id": "d4c1b433-514f-47b3-9d9e-78d89cc2bf83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 337
        },
        {
            "id": "5b1dd86c-8e21-47fd-960b-859b25fdddf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 339
        },
        {
            "id": "b692e275-0f26-4c14-8f25-d8ed903fe2c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 341
        },
        {
            "id": "1481cdc3-6df3-464e-8102-fab9a0825b7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 343
        },
        {
            "id": "500766cc-3d91-4a46-9e3e-81c8cc98b9aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 347
        },
        {
            "id": "abe9ea58-0bf2-4c78-95af-38aa3ee8287b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 351
        },
        {
            "id": "de0929c4-dc29-489d-9723-4590749135db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 354
        },
        {
            "id": "dfadeb9f-6b1e-4567-a03d-0a89a9a890f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 356
        },
        {
            "id": "3e3781ec-1517-4cb7-897e-657f07c08a63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 361
        },
        {
            "id": "a2b45481-0678-46f8-94a2-73d0933f099f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 363
        },
        {
            "id": "dd05cd54-8808-45fa-a821-c4389b9bb019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 365
        },
        {
            "id": "09d2ff6b-7408-4d4b-aa00-f9e5c14408f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 367
        },
        {
            "id": "e5074baf-9a1b-4fb0-ab78-89f414be417f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 369
        },
        {
            "id": "80972501-1ebb-4f59-9e47-e061fba050d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 371
        },
        {
            "id": "e7199073-cf38-463c-8841-8017968d7d0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 372
        },
        {
            "id": "59ba0788-496c-48ab-b6e3-be7f862b8f3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 417
        },
        {
            "id": "29adfdab-b641-4b08-9ae6-a3f130472dec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 432
        },
        {
            "id": "305cee0a-0481-4117-9b34-541953ff27aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 506
        },
        {
            "id": "fa5f7395-1459-437e-84d9-7d7064f0dcb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 507
        },
        {
            "id": "8976c228-6fb7-4ec5-a20f-781c7b80bb11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 509
        },
        {
            "id": "f3c76dc1-3233-4dcb-8df0-7935fd1457a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 511
        },
        {
            "id": "29e4977d-a252-434f-9edd-718ede57257c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 537
        },
        {
            "id": "d47dee34-0344-4128-9450-2cec8eb1061f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 538
        },
        {
            "id": "394a10d5-95b6-4f29-bc84-59cc490539f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 902
        },
        {
            "id": "075aac6c-25e6-4ae9-b15b-ff809485b99c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 913
        },
        {
            "id": "317b2e00-3849-4bcb-84f0-f476a5643787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 916
        },
        {
            "id": "d4c80913-0d15-479e-8bf8-13f042a2ec96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 923
        },
        {
            "id": "72c2b3c2-983c-4f5f-8d51-538a39542f62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 940
        },
        {
            "id": "fa371409-3070-4ec0-88fe-d8d2fdf90517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 941
        },
        {
            "id": "a3ba3609-4bdb-4a25-941f-d366264e9e3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 942
        },
        {
            "id": "d056dd63-3797-4488-86f3-168af8afb77f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 945
        },
        {
            "id": "b693258f-1f4d-4a8a-aea7-dc51ad84fa22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 948
        },
        {
            "id": "78b6e1c3-fa7a-4a04-ae33-71c499bec23a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 949
        },
        {
            "id": "de901b96-29ad-4da6-838a-80c78f212701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 950
        },
        {
            "id": "57717518-6ddd-4f4d-8ab5-7f81ca237556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 951
        },
        {
            "id": "533390f5-d0da-43fe-a377-57e531e552f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 954
        },
        {
            "id": "d2ee25e9-c235-49e3-ac35-910b95e3a877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 956
        },
        {
            "id": "72a49069-f18d-4f8b-9931-e57adc09333e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 959
        },
        {
            "id": "9b6adb39-6f7f-49c2-aa1a-ea87a06e91c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 961
        },
        {
            "id": "8d99bb3f-ecc4-49ed-9f39-6184c336953e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 962
        },
        {
            "id": "df1d2adb-1110-4a4a-a785-0f9f852c7138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 963
        },
        {
            "id": "28f85c7f-4cda-4907-84ab-613be24e1503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 966
        },
        {
            "id": "8c9e3eed-6454-4b37-9558-961adb11b56a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 972
        },
        {
            "id": "c9ac6382-2b76-43a9-a06a-d3f9032d1ea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1033
        },
        {
            "id": "cd34136b-0128-419d-a7b9-0fa72d87a11d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1040
        },
        {
            "id": "62f8f39e-5d06-4ad1-853c-11ffdb58b49b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1044
        },
        {
            "id": "2ba35aff-5918-4349-9d9f-201a032896a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1051
        },
        {
            "id": "0eacc035-6e2e-41e1-9003-ea63c1046743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1072
        },
        {
            "id": "7b9df25e-39ef-4f76-b944-10692a270eb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1076
        },
        {
            "id": "a7a5f7ed-7aa7-40df-9a08-34bf26643af0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1077
        },
        {
            "id": "de89d355-3e3f-49c9-9948-c86daee894f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1083
        },
        {
            "id": "b3175f37-2db1-4f3a-a765-d2ca987fde7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1086
        },
        {
            "id": "26c78688-0445-417d-a69f-8d412f73f4e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1089
        },
        {
            "id": "8e77d68a-ef30-4d6b-8ca1-407e923b9f17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1092
        },
        {
            "id": "b2d9986d-49d8-4ced-86cf-1e163690cc28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1104
        },
        {
            "id": "059aafee-6edd-4b05-bf1b-b70ef1c32ed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1105
        },
        {
            "id": "9c4a1908-f64a-4c15-84c8-86c431c1bddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1108
        },
        {
            "id": "bc763988-0ee8-43c3-893b-2aa6e309f616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1109
        },
        {
            "id": "10cc2403-e357-412b-ac1d-9c490eaab6e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1113
        },
        {
            "id": "1af18075-f232-494a-a529-bf613e1ab3cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1126
        },
        {
            "id": "a3b74cb5-5732-42b6-96bd-f7ec2a4c865d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1127
        },
        {
            "id": "08855c76-7d4f-48ca-9cc3-551a7ceb4a2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1139
        },
        {
            "id": "72f49c51-55bf-4ada-9e52-862d5ad19413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1145
        },
        {
            "id": "8b22e835-dc90-43a3-9d0e-f62cd7d107dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1147
        },
        {
            "id": "206f5e77-5e11-4c8a-b1c9-4aaa824952fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1149
        },
        {
            "id": "4cb832af-f82c-41f3-bdd6-e2d24f822bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1153
        },
        {
            "id": "71a5b84e-1aa0-4b74-aea8-4f1ad6985696",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1193
        },
        {
            "id": "7cbc756a-a01a-4adb-9dd4-1353d1118d32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1195
        },
        {
            "id": "9238559d-2d6b-4202-b2a8-dcd4b6cc57a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1221
        },
        {
            "id": "47fd86c6-a1c5-4c72-887f-d8ad212cf2b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1222
        },
        {
            "id": "92b68eec-a56e-496f-87a7-d25b116376f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1232
        },
        {
            "id": "e0ac7e4a-5751-4870-b22a-44b97d088d70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1233
        },
        {
            "id": "831ee583-6e34-4624-82a4-8e5f5023ad30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1234
        },
        {
            "id": "49aacb2e-9da6-43c9-83d4-276c2e86ae52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1235
        },
        {
            "id": "58216389-40a3-41c6-8203-6cefeab8a8a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1236
        },
        {
            "id": "b9ab7e43-6383-455b-883e-755d0c913353",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1237
        },
        {
            "id": "8f8af16b-c555-4647-9e8a-ddee522ad776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1239
        },
        {
            "id": "0283ba08-c31e-4e74-a166-1754a4fb6b2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1241
        },
        {
            "id": "02b491d9-2374-4dcf-ad36-4410b29bb356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1243
        },
        {
            "id": "b3779deb-a674-4a45-b9ad-8b0c8865ddda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1255
        },
        {
            "id": "449fb71a-271b-44ac-8387-c2b8651953e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1257
        },
        {
            "id": "d2f8d349-8e9d-46a7-9d26-5183bf7c3836",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1259
        },
        {
            "id": "10a0c9fb-7af6-4b77-bbe9-0faf136e3d8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1280
        },
        {
            "id": "74cc815d-d04e-46dc-a41b-815a97cd9f1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1281
        },
        {
            "id": "a62b4b73-c58b-4439-8fe3-1134111e68b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1282
        },
        {
            "id": "784a28ff-8119-41b7-95c3-61a0df93291b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1283
        },
        {
            "id": "4be7dbfb-6409-4141-81e1-e3dda56d8b37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1288
        },
        {
            "id": "594492fb-2466-4407-8baa-72a00ad07085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1289
        },
        {
            "id": "bc3ce64d-11af-44a5-8606-3eb62b0ba303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1293
        },
        {
            "id": "6699442b-e766-4988-bc5d-97324871963d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1297
        },
        {
            "id": "2fe9a86c-9f5d-489e-8f37-5980feba9e86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1298
        },
        {
            "id": "ba4cd78d-03af-4724-a6a5-5120a492300a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 1299
        },
        {
            "id": "586b8c3d-af04-4a17-ad3c-d41980ac2893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7680
        },
        {
            "id": "55292a66-517d-4e17-a1b5-2553772a0ff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7681
        },
        {
            "id": "390f2ebf-7ea4-479a-8987-d977c09921e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7743
        },
        {
            "id": "4e2fdb88-7024-47ca-a664-c175cf1770f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7808
        },
        {
            "id": "48203bce-83eb-430c-96c0-999a87c0d876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7810
        },
        {
            "id": "6e2020a5-965c-47a5-abc4-ec5159680403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 7812
        },
        {
            "id": "1abeb624-39ef-46c1-91e9-7a7a4471c751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7840
        },
        {
            "id": "1042f3b4-43c1-4b51-aece-423dce2fca55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7841
        },
        {
            "id": "33612191-e0c7-4625-848f-63aa510d4414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7842
        },
        {
            "id": "79ae3d8c-ddf1-429f-ae3b-de8cd17275a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7843
        },
        {
            "id": "cf2c7d34-804f-44f7-93ad-77aaa1ec0122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7844
        },
        {
            "id": "c0194e5b-3fba-4bc2-89d5-e57663053838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7845
        },
        {
            "id": "becf530a-1239-4e3e-b175-bcacdc6dfb24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7846
        },
        {
            "id": "9bfda402-fbd3-4069-b116-589f93af9f98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7848
        },
        {
            "id": "8003c01b-888a-4bed-a743-7e5c03d815a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7849
        },
        {
            "id": "94505dbb-3aa1-48f1-839a-180a45de9800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7850
        },
        {
            "id": "e965d743-b286-42e5-bd99-66d7144f51c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7851
        },
        {
            "id": "427e22ab-4b28-46d7-b62a-57507eda47ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7852
        },
        {
            "id": "680d0109-64cc-442b-93c2-c1a36ae7bc0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7853
        },
        {
            "id": "67500335-42d5-4c61-9708-24fb6e2016a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7854
        },
        {
            "id": "0cd81a25-dbec-4879-9b1c-0dd635cefd5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7855
        },
        {
            "id": "f34a2a31-796d-4e9d-95f8-806b3189291e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7856
        },
        {
            "id": "15330420-55a0-41e8-b877-92d7f512fd4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7857
        },
        {
            "id": "e038db75-1903-43e8-bf50-5e6904d87ac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7858
        },
        {
            "id": "54d7c71e-b94c-4780-97be-76591047c56c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7859
        },
        {
            "id": "928757ed-b12d-4f09-afc2-5bb58fc49449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7860
        },
        {
            "id": "e36717cc-c159-4898-ab82-21d5b57893fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7861
        },
        {
            "id": "1abe5503-210e-4ff5-a72d-d2ac36be8875",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7862
        },
        {
            "id": "f0bcfd75-efdd-4c92-bd07-37fd89ab8f13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 7863
        },
        {
            "id": "ac7f7fcb-36aa-4331-abb6-c84cf68d799b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7865
        },
        {
            "id": "e5f8d995-04da-43e9-b1d2-ca10eee56824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7867
        },
        {
            "id": "5df2c728-7d38-45f0-903e-8e105567b376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7869
        },
        {
            "id": "eaabdbad-aaea-4334-9433-02c794608712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7871
        },
        {
            "id": "2f47b8e0-008d-45ae-ace0-d8d7090c4882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7875
        },
        {
            "id": "c6117a70-65fd-4c04-b101-b7983667be65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7877
        },
        {
            "id": "f3a560e7-f74e-4f7f-9f03-81bee29c9fdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7879
        },
        {
            "id": "f86e1a5c-35c7-4855-9237-6cd46064d9ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7885
        },
        {
            "id": "af7958fd-f03a-4a0e-accf-a30684687938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7887
        },
        {
            "id": "52787269-591e-4186-82ba-bbe10a343c04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7889
        },
        {
            "id": "6b384318-532c-449a-8d57-e12dc92cdc7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7893
        },
        {
            "id": "918895ca-e82a-4d5d-a91b-885d3083d1bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7895
        },
        {
            "id": "ef0820ce-8850-46d3-9804-d9ca8b3907c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7897
        },
        {
            "id": "3c63e8b5-1a18-4ddc-b560-7e7de66aefa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7899
        },
        {
            "id": "e3430e16-3acc-428a-b0af-8f37478a7c23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7901
        },
        {
            "id": "58f002ee-62e2-470c-8355-90e3a5e9e78f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7903
        },
        {
            "id": "bbc81846-9319-42fe-af43-cbda599ea25e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7905
        },
        {
            "id": "5ae0c7f5-4635-412d-accd-cd3d677e7770",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 7907
        },
        {
            "id": "b85b4062-ca15-432c-9cfe-4a3449c30fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7909
        },
        {
            "id": "6b25e292-f3d0-4c08-a991-77c962998570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7911
        },
        {
            "id": "4cd88714-6ca2-4c26-9a70-4304ba0306b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7913
        },
        {
            "id": "9e244887-68a1-43a2-a8b4-ffda70c19330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7915
        },
        {
            "id": "d4060f44-9c6e-4d06-b900-ba7a5675706d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7917
        },
        {
            "id": "d0e015f3-d70d-40ef-bbde-34726a40936f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7919
        },
        {
            "id": "a6c46342-12a5-4f0c-892b-f7f13448696c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7921
        },
        {
            "id": "70a1fcfb-d968-4757-bd89-f675ec51bd0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 40,
            "second": 74
        },
        {
            "id": "ee9e28ce-1c5a-4ab3-8ea0-c13175f6293e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 67
        },
        {
            "id": "29da9204-9615-4a9f-9743-bb2c47bf1431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 71
        },
        {
            "id": "86b02ef9-26e2-40f7-ab99-8be635e4cf71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 79
        },
        {
            "id": "c87f2920-ca1c-448c-8239-0a0687ce1157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 81
        },
        {
            "id": "2443e28b-d5ab-48ed-84e9-0fa9aa179df7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 84
        },
        {
            "id": "112a946f-8f5a-4928-9197-8dfc0ce5e4ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 85
        },
        {
            "id": "8aa3ab0d-1163-4084-873d-0633a611c7e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 86
        },
        {
            "id": "b7c83f09-bcf9-4c7c-9240-567fb3352fb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 87
        },
        {
            "id": "a22e0dd1-4b82-4006-aa28-18a8e35dcacc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 89
        },
        {
            "id": "77dfcec2-1bd8-4817-b885-129ef64e2939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 199
        },
        {
            "id": "22abe6ee-8f9c-4af7-a4dc-4ab5e17f502e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 210
        },
        {
            "id": "774c329e-8e42-4b6f-aa45-8633f6c960f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 211
        },
        {
            "id": "2d5f0081-e09d-42d9-aa6d-a10aeb049a2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 212
        },
        {
            "id": "23b19ccb-1735-4d65-81cf-3c7ad48320ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 213
        },
        {
            "id": "14deb527-d2a0-4634-bc1e-ea662159837a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 214
        },
        {
            "id": "9ccabf33-15e7-478a-a711-2ebc5a0f3e58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 216
        },
        {
            "id": "858dbe64-b024-4ff2-a4a3-bd00940dbc68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 217
        },
        {
            "id": "ca0e88b9-fc46-4c4e-b55a-37e9cc2a2e1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 218
        },
        {
            "id": "f0764a3e-32a9-4283-9808-e589ef566a76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 219
        },
        {
            "id": "27a6d741-f13a-4143-8c12-bc1bd8daafd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 220
        },
        {
            "id": "091567a4-2090-4ecd-ae2e-a53217e953d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 221
        },
        {
            "id": "abade774-d89e-43ee-84c4-1232c52e737a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 262
        },
        {
            "id": "d0845952-d7e1-48a5-be97-29b6510febfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 264
        },
        {
            "id": "afed096b-37eb-4d9f-8535-d00e4055fa97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 266
        },
        {
            "id": "7f946946-7fca-44e4-bfde-a850de2b924a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 268
        },
        {
            "id": "5b8b2bd3-40cc-493a-a60e-65a7d6e9d11d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 284
        },
        {
            "id": "f05bbb50-4102-4919-a23d-e9ba7d278f09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 286
        },
        {
            "id": "3da0411c-fb89-481a-a106-06801ff4d8c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 288
        },
        {
            "id": "cef0cd23-cf06-40cd-89cc-1134910735ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 290
        },
        {
            "id": "4902c13b-46c9-4dfe-abee-88b88a5ca904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 332
        },
        {
            "id": "d0cbb301-4389-465d-ae63-d75c850b6d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 334
        },
        {
            "id": "f8dcd9f5-edfd-43be-82d3-6b3abb4bd310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 336
        },
        {
            "id": "93af1e10-5b61-44b9-b897-638e0e297604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 338
        },
        {
            "id": "fd209ff6-d5b8-4d8c-b3e3-63ea191d1593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 354
        },
        {
            "id": "8aaf268f-dbd3-42a5-9428-66e68cc6f18f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 356
        },
        {
            "id": "edd6edb8-5648-4215-b6ef-4c50e13fa19b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 360
        },
        {
            "id": "43a2fa7c-529e-4b05-a073-08d3be3f2832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 362
        },
        {
            "id": "3aec3109-9596-4707-97c6-bb9217a4a816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 364
        },
        {
            "id": "5acd51d7-218f-4208-a973-ef44666eb40d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 366
        },
        {
            "id": "ab8d60de-730a-4420-a648-f0ce74674994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 368
        },
        {
            "id": "6dddca3e-2cdf-4835-9541-b3a5edc8249b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 370
        },
        {
            "id": "a4915574-7eba-4389-a43d-26fb51eef626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 372
        },
        {
            "id": "cbbe4142-ff78-43a4-afbd-1f4d7b0fc6d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 374
        },
        {
            "id": "edd644d7-4960-4e5e-bff7-f776ffd0f472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 376
        },
        {
            "id": "f3036018-9ccd-4551-8a25-51133b60a3fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 416
        },
        {
            "id": "07966f88-5500-4cbc-a944-4f23f959cd58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 431
        },
        {
            "id": "e3acd3c0-e7e0-427a-974e-5794385edfd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 510
        },
        {
            "id": "91ac7adb-1daf-474f-9da8-ae943e9e82f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 538
        },
        {
            "id": "7f38438f-4dae-47d3-884f-6a7dd90ee7f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 920
        },
        {
            "id": "25d7ae27-5966-4482-a284-637dba47e6c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 927
        },
        {
            "id": "1abbfc87-6630-4870-887d-1221550f9cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 932
        },
        {
            "id": "fabf1d80-8d04-4f07-9224-a32a46813830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 933
        },
        {
            "id": "70781d22-88de-4673-96a6-0c4d683a2682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 934
        },
        {
            "id": "0d5eb048-da99-4667-bcd7-1c56f2e065ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 936
        },
        {
            "id": "282aac4d-916d-4dea-874c-604c1cea377a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 939
        },
        {
            "id": "f7549056-875a-4a35-983d-c301ee12028f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 952
        },
        {
            "id": "d899c0b9-5ae9-43d7-abc4-9f9e818c357e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 978
        },
        {
            "id": "b8434907-d8ae-4391-99d8-1ed115409e39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1026
        },
        {
            "id": "85b0a8fb-bd70-4e74-a89a-6aa6225a1cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1028
        },
        {
            "id": "6bf49c94-e058-4585-8f49-4b2fe56e427c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1035
        },
        {
            "id": "7e066b0b-1f44-4f9e-9b7f-45082af843d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1054
        },
        {
            "id": "435561a2-f673-47b2-8c12-16ba29242845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1057
        },
        {
            "id": "d1b21e99-fd8a-4068-b882-a70d8201fbd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1058
        },
        {
            "id": "f01b1cb0-752d-4d09-bb3c-d9e1f787b5c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1060
        },
        {
            "id": "95debd8a-1e9b-4f33-b9f2-9beaa299e443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1063
        },
        {
            "id": "40d71e14-ef39-4996-b82b-1fc6d832470c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1066
        },
        {
            "id": "ade4b5fe-fdd3-4990-a26b-d4421af303bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1090
        },
        {
            "id": "15c2bf8c-ed10-47f3-bed6-c14d883b7eae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1095
        },
        {
            "id": "897d1d7d-9016-41fb-a6e5-b1ff0883dc85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1098
        },
        {
            "id": "18252893-f2ea-4c6f-8caa-7caf79fee0f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1120
        },
        {
            "id": "a4350bde-faf6-44db-b7f2-521ddb1ad8f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1136
        },
        {
            "id": "aa017fd3-c29b-4f58-8187-5d5dbe4f1599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1138
        },
        {
            "id": "1001c4b6-dc3a-44c4-b06c-36b1faa7a46d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1140
        },
        {
            "id": "9c7c4f4e-b9cb-4212-9ddd-f686988c5566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1142
        },
        {
            "id": "7bd5c092-0b71-4a2d-bb96-0d566e9d7bb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1144
        },
        {
            "id": "67b6a80f-b24a-4d4a-ae72-4919cc7a22eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1146
        },
        {
            "id": "e8b79e11-5216-4c65-b5f2-cedbf3b80d90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1148
        },
        {
            "id": "f76357dd-26f8-40ce-b31d-4a0b56b62651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1150
        },
        {
            "id": "a2e2088c-e40c-407b-ab33-d7b88dc6e84b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1152
        },
        {
            "id": "fcc9cdf4-1597-426a-9d42-db128aff404d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1184
        },
        {
            "id": "f1f10d18-ee2a-441f-b220-ce9e3fcf006e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1185
        },
        {
            "id": "773248eb-1d95-4844-b5b7-ee87c15c547b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1192
        },
        {
            "id": "f28be98e-af61-46a0-9dec-a413447ff7bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1194
        },
        {
            "id": "c52a9666-9375-4f71-9361-5aaeff1c7774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1196
        },
        {
            "id": "6a10da15-928f-49e6-8c2f-f93568b81cc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1197
        },
        {
            "id": "610d7f3f-1f45-4cf1-9e10-38e6d1483929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1198
        },
        {
            "id": "2c8ab336-2b5f-45b3-96d1-a7d7f1e5d3e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1200
        },
        {
            "id": "68f27a79-45eb-42f3-8fb0-d292cfd41672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1204
        },
        {
            "id": "980ee25b-f54c-4677-ac58-3f6439e57dd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1205
        },
        {
            "id": "61c66497-d633-49be-896e-2cfb3ba3a86c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1206
        },
        {
            "id": "50bad88e-165f-4430-a446-b30b04f957b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1207
        },
        {
            "id": "1b1c65e0-3e12-4347-a247-994a60803e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1208
        },
        {
            "id": "4d52b24d-deae-4310-bf80-4b0bcec71085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1209
        },
        {
            "id": "1274bd47-14c7-476d-aec7-c6d8a9337c9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1212
        },
        {
            "id": "70ff281a-335b-4d62-9fa1-adec7b559ab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1214
        },
        {
            "id": "36c70e13-bb22-427d-ad69-346f06f8f0b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1227
        },
        {
            "id": "4803f9d9-e9f7-4d0d-ba94-36d60332b156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1228
        },
        {
            "id": "8be22405-5ade-4773-ad84-bd4be9e487c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1254
        },
        {
            "id": "effecc70-d81b-49b4-99c2-392542584cf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1256
        },
        {
            "id": "dfabc331-0616-4122-8b93-5e4a92f38623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1258
        },
        {
            "id": "7094769f-639f-428e-af08-92934e7467f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 1268
        },
        {
            "id": "a29569f4-6304-4df2-950c-90dee38a3012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1269
        },
        {
            "id": "303ed78b-462b-47f1-9886-177b94bba442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1284
        },
        {
            "id": "c5d2bb85-2fc3-4b82-a0a5-4e2fc5a24083",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1285
        },
        {
            "id": "d0dce80b-149e-4ad5-b5a7-9de8a2ef1e88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1286
        },
        {
            "id": "84860c4d-15af-40a3-9325-48ce31c12eed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1287
        },
        {
            "id": "bd4a34bc-5df9-476a-ac85-8f355b18c150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1292
        },
        {
            "id": "5fa66fdd-b831-4336-920c-8eee1937ec3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 1294
        },
        {
            "id": "3696c16f-beee-4086-8bc2-75d1392ea114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1295
        },
        {
            "id": "31fbbaba-d672-4bc2-a162-75645ce61da4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7808
        },
        {
            "id": "c98d65c8-a393-492c-91d6-e9374fa0b98b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7810
        },
        {
            "id": "42569af7-3fff-4f7b-9f26-15064c2fe7b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7812
        },
        {
            "id": "5eb45e85-e25e-41cb-bf16-d4fd44f94099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7884
        },
        {
            "id": "3fda2eeb-b7e5-4555-a1d4-ba1ab03bcec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7886
        },
        {
            "id": "cc984ff7-1256-4486-b6a7-1e345d9a7703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7888
        },
        {
            "id": "d9c38ebe-3d4c-4ee1-b592-afdee40d6dff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7890
        },
        {
            "id": "19ebbfad-522c-4270-8b2c-7cac78def403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7892
        },
        {
            "id": "8ba206b1-69a9-463d-b8c1-a7a450ace533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7894
        },
        {
            "id": "1423b8fa-c275-46e9-af84-70ac6ee1f1d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7896
        },
        {
            "id": "2d8a3bbf-d007-4158-89df-d1f1b6c7aec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7898
        },
        {
            "id": "17259a92-a9fb-4d5a-a60d-07dcbf27baf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7900
        },
        {
            "id": "01a312c3-366a-49f0-b3fe-196f47a877b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7902
        },
        {
            "id": "9e80b2be-becb-40e5-8fd9-579fd5744ed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7904
        },
        {
            "id": "20caa04b-05aa-4134-a8bc-b8174d012da4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7906
        },
        {
            "id": "1a04fc0a-abd7-4b00-beaf-f6abd34cd56e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7908
        },
        {
            "id": "52bce855-8ce4-4226-8823-06ceede57988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7910
        },
        {
            "id": "2fd8e030-f796-4640-b7bd-e879197c6f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7912
        },
        {
            "id": "7738ceb4-1f3c-4f3c-bc46-0d3e7c62d533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7914
        },
        {
            "id": "796b0156-7bed-4de3-8bac-1270f963a115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7916
        },
        {
            "id": "7c745d91-bda8-413f-9d78-a43529c33a22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7918
        },
        {
            "id": "7eabca08-00e1-4898-b727-2a5f9e8fa5e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7920
        },
        {
            "id": "68b8400c-7712-42de-953f-1e69e103851a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7922
        },
        {
            "id": "31b7be05-27f0-4bde-b4ea-a702df16f163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7924
        },
        {
            "id": "64c46319-7d91-4c75-a156-9595b28a845f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7926
        },
        {
            "id": "62b84802-b164-4703-8588-caa508785600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 7928
        },
        {
            "id": "c0d0abfe-c30c-4f42-ba3c-776266c76619",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 84
        },
        {
            "id": "285b3e3b-eb9c-44a0-88df-103755120d61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 354
        },
        {
            "id": "74ae91f3-1cf9-495e-972f-db39de13baa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 356
        },
        {
            "id": "1bb5de3b-d819-4718-893a-0a862e12a8ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 538
        },
        {
            "id": "ab8411aa-3d6c-4d59-a519-25d97036064e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 932
        },
        {
            "id": "dbc381c5-5bf4-48f4-9229-da754250479d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1026
        },
        {
            "id": "f8f76b00-1f05-4270-a6ef-5368ec925d02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1035
        },
        {
            "id": "cd00b7b7-acca-47d7-af02-65aec7c8c2dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1058
        },
        {
            "id": "5d7bb396-3be5-4337-a745-e03f2903f681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1066
        },
        {
            "id": "3ed27a93-0641-4749-8e7e-62923d06e52d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1090
        },
        {
            "id": "2486f2bd-1186-4cfc-9618-3d6c381e1477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1098
        },
        {
            "id": "c2996036-da09-4e80-ac07-01624534b90b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1184
        },
        {
            "id": "b3b87ebc-2656-48ed-bd78-8808f0bd6e10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1185
        },
        {
            "id": "395b5230-5298-4b85-ac0c-40b0235a2e45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1196
        },
        {
            "id": "74fdaf76-6a87-479c-9a17-41009cf53cd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1197
        },
        {
            "id": "2ddf9766-f7e3-40fe-b669-c400ebbefc6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1204
        },
        {
            "id": "12a1ee45-9d28-4da6-9b07-a1943e0ab7b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1205
        },
        {
            "id": "b68af8d4-2b93-4fdc-84f6-f515d7f33699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 1294
        },
        {
            "id": "f1a84883-f936-4dcd-ad03-9733e18f4df7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1295
        },
        {
            "id": "10f6d28e-0c73-42d4-9a7c-216d018e56e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 67
        },
        {
            "id": "4f7eaab8-a28e-45ba-a842-53c476f6949c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 71
        },
        {
            "id": "931c6d55-4cbc-4dc1-8d27-7baa52c1656d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 79
        },
        {
            "id": "4ae5a6bf-015f-4cd2-9a17-6a5fea214c51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 81
        },
        {
            "id": "bc3a9504-1205-45e4-ad56-122edeeff935",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 84
        },
        {
            "id": "9ca90a3a-e8a9-4d4d-a39f-c5027072dfee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 85
        },
        {
            "id": "7f70f4d8-668b-4e31-93df-8b89a3af9160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 86
        },
        {
            "id": "8121110f-dcf8-40c6-a34d-f6bddb94d40d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 87
        },
        {
            "id": "1ad3ad23-fd11-4a70-9e07-c80bcd3528e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 89
        },
        {
            "id": "6ebd9bce-384a-4ab3-93d7-f752e3ab7e0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 199
        },
        {
            "id": "4bf7d9c5-2eac-4117-8e87-5631fbeacfed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 210
        },
        {
            "id": "087e85b3-f9d8-4c14-823e-4b68a405373e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 211
        },
        {
            "id": "66fca640-d621-4779-ba02-85899585da35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 212
        },
        {
            "id": "283b6781-f3f1-40d1-a866-bd4abbfcbbfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 213
        },
        {
            "id": "0e74231a-09d0-4089-9839-0d9563960ee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 214
        },
        {
            "id": "5c1bae88-f470-41bd-9c3e-cfc7026fee61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 216
        },
        {
            "id": "e3ef9817-781b-4d2e-99ef-f6c34cd4b70d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 217
        },
        {
            "id": "3679c830-6a47-4cce-95a4-5fee454ddc05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 218
        },
        {
            "id": "41022588-869f-4e37-b1ff-daf95b7b9f41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 219
        },
        {
            "id": "e03c41ba-d714-42df-a1c5-5781b43ad4c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 220
        },
        {
            "id": "33d74ba0-9c45-45b3-a59c-dd75133a7b3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 221
        },
        {
            "id": "77763ade-aaac-4c5a-9859-00c76a8a0c66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 262
        },
        {
            "id": "d43a9452-cd9f-4b03-b141-69a14dc77bd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 264
        },
        {
            "id": "61ebb819-4f41-4bc6-b456-d0a763379615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 266
        },
        {
            "id": "640070b2-c034-40f3-b477-303f6c0a4d2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 268
        },
        {
            "id": "2fb14bc9-7af2-40c7-a52a-6840da22d484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 284
        },
        {
            "id": "2219cd63-f7cf-4a5f-bd04-4a56f22bab94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 286
        },
        {
            "id": "201988d7-625d-4a64-8c1b-7a53f80056a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 288
        },
        {
            "id": "d33dc4f3-2b4a-437e-a47b-6188263af9df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 290
        },
        {
            "id": "44cf7aa5-b471-426e-afa2-e2384611b82b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 332
        },
        {
            "id": "f26f42c3-2ebc-463a-912e-bb76fd793336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 334
        },
        {
            "id": "6907e286-3adc-42c5-9518-4627cb7807c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 336
        },
        {
            "id": "e0a4e978-5007-4dc6-b917-8882cb2bfc26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 338
        },
        {
            "id": "7979e319-b691-4107-8297-25a5d2ffab2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 354
        },
        {
            "id": "782f264e-93fb-48b4-baa3-ce0aba8c05f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 356
        },
        {
            "id": "f7391758-09d1-4670-8937-321f6acaa161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 360
        },
        {
            "id": "368e01a4-85d5-45c0-8f6c-01ccf44c3c2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 362
        },
        {
            "id": "07bb85b1-07a2-4b98-b37b-362db012d449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 364
        },
        {
            "id": "92cf1a59-0c9e-4f89-8b30-2d7c6a229575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 366
        },
        {
            "id": "ae8eb73e-89f6-4a9c-a6b8-eebacb3d5919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 368
        },
        {
            "id": "20f9c85e-c4c1-4900-ab63-970a4c3a1404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 370
        },
        {
            "id": "df89535c-62b1-4e79-b71a-5818b7bdb8b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 372
        },
        {
            "id": "b11cd6db-d46b-4056-9992-6fedaccad46c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 374
        },
        {
            "id": "38ceb80f-a734-4f82-958b-7152ee458fb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 376
        },
        {
            "id": "a5bf155e-be79-498a-b505-0cf9a8d18095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 416
        },
        {
            "id": "d4262d0c-138a-4e26-8b26-e809428b45d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 431
        },
        {
            "id": "9b5abf4f-ec93-44aa-a9e2-7fbf75294e93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 510
        },
        {
            "id": "36c592b8-77bc-4fca-aa49-cd835b8ae009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 538
        },
        {
            "id": "647519fd-376f-42a2-b40d-8d2ac29bf41e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 920
        },
        {
            "id": "d2b6f488-e2c3-4628-bbc3-f826e8978fac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 927
        },
        {
            "id": "d0ac75f9-cb9a-4cca-ace7-db92eec2d2c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 932
        },
        {
            "id": "4359be45-491c-43b4-8d42-72a538e586ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 933
        },
        {
            "id": "64dd9ca3-6425-45c6-9a56-fe5d6d04ff05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 934
        },
        {
            "id": "481371b2-23eb-412d-b7a9-5547718f0558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 936
        },
        {
            "id": "72452dfb-2fcc-4673-8ea1-8de1a3d0df77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 939
        },
        {
            "id": "fa822359-f576-4483-8f04-b070d4140dee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 952
        },
        {
            "id": "dc690d85-d99e-44d1-8dc4-37c35e51463a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 978
        },
        {
            "id": "1dec064e-6283-4e1a-99fd-6046867a38e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1026
        },
        {
            "id": "840d1e19-3733-471d-a15b-53a492d61d7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1028
        },
        {
            "id": "7345eaf8-c73d-4344-98ff-ef09bafa16fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1035
        },
        {
            "id": "2429abba-add9-49b2-986d-b8b51e257709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1054
        },
        {
            "id": "9e3c05b0-3c7a-41f7-88e4-229dbf6138b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1057
        },
        {
            "id": "0658d8d3-9d1b-4618-a383-72199f9a6574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1058
        },
        {
            "id": "5fc9e471-f5a4-4675-be00-d6e84c5a0031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1060
        },
        {
            "id": "30cc19a5-e23d-4bf7-9b2d-18948ae516b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1063
        },
        {
            "id": "69144bfe-06db-44a5-be38-1243c9adc1a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1066
        },
        {
            "id": "9995b201-b0f5-40c0-8298-eceb25c93748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1090
        },
        {
            "id": "4f53a289-7b34-492b-a044-91b4606b57d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1095
        },
        {
            "id": "d215a6bd-52f5-4be8-a5d1-1c26a3211d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1098
        },
        {
            "id": "6c4e5378-0b24-4a71-96c8-9849bc64bb00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1120
        },
        {
            "id": "c80565be-c429-41d2-97ad-0c3087320d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1136
        },
        {
            "id": "c68a7d55-53f4-41c9-8b14-d91df52f01bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1138
        },
        {
            "id": "98b1c6c8-9129-44aa-b77e-4205d0647af0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1140
        },
        {
            "id": "4f077d1d-46a2-4c89-bf44-29e3d6ee8ec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1142
        },
        {
            "id": "8a14ae8b-9604-4581-ae86-988797b403d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1144
        },
        {
            "id": "2fd69349-fdf0-46fb-a3c5-d3e981f4a400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1146
        },
        {
            "id": "7c329a91-a5fb-4c74-af8c-0daa23513607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1148
        },
        {
            "id": "659ff488-a83b-4858-a36f-4c341cb9b9d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1150
        },
        {
            "id": "c7c20973-7a42-4002-9b14-8bcf021d3164",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1152
        },
        {
            "id": "66d90830-1d2e-452e-8d7d-077f1ed1f367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1184
        },
        {
            "id": "51e1dc35-0856-46b9-bf86-fad0f20a517c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1185
        },
        {
            "id": "6fa57756-7c9f-4830-a2aa-1feeea107528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1192
        },
        {
            "id": "dbba32cf-8326-452d-896e-5119f353f7da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1194
        },
        {
            "id": "a9f6339e-af0e-4d7e-aae5-ef473a2e7d17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1196
        },
        {
            "id": "cac29f44-2892-44e4-a1a7-4b38fda526f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1197
        },
        {
            "id": "baaac201-f4a0-4cbf-88bf-d99736029d9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1198
        },
        {
            "id": "c8e14572-4610-4aa9-acf3-5da0b4dfb136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1200
        },
        {
            "id": "744c18c6-49d8-48a5-9938-4a2d5a912cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1204
        },
        {
            "id": "0d0fcfa2-7ac4-4b8b-a8fe-52eb37521921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1205
        },
        {
            "id": "c251cf3e-af63-4c9d-8c1f-62e93a875ebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1206
        },
        {
            "id": "8a9f2901-9a88-4443-b857-32d0aa5fc354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1207
        },
        {
            "id": "999c509a-0846-4133-8946-d566d39700c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1208
        },
        {
            "id": "b1c79262-18b5-480e-a108-ab11dc425f4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1209
        },
        {
            "id": "2ff6cc0b-5b99-4edb-a2dd-7896374d2190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1212
        },
        {
            "id": "2811b0bf-f56d-42ce-a5a4-14aba97cfa72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1214
        },
        {
            "id": "3d5c8dbc-3535-419e-b650-8a011c1cb338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1227
        },
        {
            "id": "d67e6774-fc55-4768-b243-9f664f6fa01e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1228
        },
        {
            "id": "1d1d9047-9292-40a6-a2e6-3e69ef72b912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1254
        },
        {
            "id": "c9a90475-554c-40af-8605-0cf8f15291d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1256
        },
        {
            "id": "3bda2186-4015-4920-b831-8838ac5a476a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1258
        },
        {
            "id": "058b6bda-c8e3-4587-821c-8d76639c71c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 1268
        },
        {
            "id": "88edd29f-32c8-4c94-a9ef-140b50bc7683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1269
        },
        {
            "id": "73597bc0-f75f-403a-bd14-27bac3f2d2b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1284
        },
        {
            "id": "3a11d679-2681-4104-a984-4424af4d8f99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1285
        },
        {
            "id": "e3102b87-d500-41c2-8434-90b4381415e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1286
        },
        {
            "id": "93474e92-4592-406c-bb50-5d6c998e2ec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1287
        },
        {
            "id": "b6e1fd50-4d3f-4564-a5af-f651ba85d422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1292
        },
        {
            "id": "11f6a7e1-e352-4baa-8c01-2764ffa1e461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 1294
        },
        {
            "id": "b06bc323-bcdd-4182-a659-1470911af810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1295
        },
        {
            "id": "d010028b-81a4-4d2f-a0d5-40b23bc34e20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7808
        },
        {
            "id": "09714785-1a10-4701-8aa6-5710af400315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7810
        },
        {
            "id": "3967e76a-f8ed-492d-97ab-46da09c41c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7812
        },
        {
            "id": "e3d1c9d2-8084-4613-8ac0-0ffc20711688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7884
        },
        {
            "id": "13ac4ee1-52a2-4e8f-9e61-82e1fa8048e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7886
        },
        {
            "id": "3a441b69-6d40-4adf-b1e2-8797ad6c42dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7888
        },
        {
            "id": "ac228bda-da50-49c6-8f6b-becf8a29d4d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7890
        },
        {
            "id": "8886f1df-198d-426b-83e1-2d83d1daecef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7892
        },
        {
            "id": "ddd8952a-5315-4f09-b0b1-6b8f967f3c63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7894
        },
        {
            "id": "f7a4f56e-2ced-4369-ad26-34237c1e5ca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7896
        },
        {
            "id": "533f8613-65c7-4db5-9c37-81b4fa25ede4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7898
        },
        {
            "id": "6751e8b1-a5d2-4868-bb28-cd348b1eebd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7900
        },
        {
            "id": "a3ba68bb-5073-413a-84b5-7c47ed9e3df5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7902
        },
        {
            "id": "dce9091a-a2f3-4b86-9806-3db9b78a4223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7904
        },
        {
            "id": "7aa590e3-932c-453e-bf92-eef63b9e7a5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7906
        },
        {
            "id": "76bd0eda-9a86-4322-a9db-a84288dacec4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7908
        },
        {
            "id": "11746fbb-bbd7-4250-8281-2d5262230324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7910
        },
        {
            "id": "a711e97a-b488-45ea-8e47-4af59c3b59a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7912
        },
        {
            "id": "3a1c939d-a1ab-433c-bd30-9d8cce4654d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7914
        },
        {
            "id": "7bd7fffc-5796-4449-a3e1-36752e4d75c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7916
        },
        {
            "id": "be8e9154-3b16-4214-b868-6ac2f3d2c4f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7918
        },
        {
            "id": "ea645860-1175-4048-aa3e-f06a5ce192c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7920
        },
        {
            "id": "05d67ab5-849c-45d6-a689-631cb59e9317",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7922
        },
        {
            "id": "fbae3c92-e074-43d5-ae42-71dbb5da4cdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7924
        },
        {
            "id": "3d0af468-b10a-4535-81f3-432d8bdd5489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7926
        },
        {
            "id": "dfd4cc0e-f7dc-4480-9f04-f0b636de842b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 7928
        },
        {
            "id": "861c7664-5385-4098-a259-bf04836b558b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 34
        },
        {
            "id": "01124691-4c57-4167-b1a9-33ecd575f6ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 39
        },
        {
            "id": "d723c3f3-1187-4853-a019-358afb9c662c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 67
        },
        {
            "id": "fd0f2900-3cd2-41fb-88a6-0a3cb616a6bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 71
        },
        {
            "id": "3541d6b2-3d8b-43e9-b736-d501b19a93cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 65,
            "second": 74
        },
        {
            "id": "9f7396f7-bd73-44a8-9458-2c0bb231233c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 79
        },
        {
            "id": "a0eabef9-e93d-42a2-934f-db59425cd7a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 81
        },
        {
            "id": "0eae8eb8-2875-4fce-a27c-6dd20b8de4db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "43fdbab5-56e1-4b03-9bcd-165420401394",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "2ba2e977-7c42-4547-9540-477ff9ba8499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "a34cf2c1-d373-414b-bde0-c7fe26c18f3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 89
        },
        {
            "id": "69844c0d-d5c9-4d43-85e7-e9a95b70dd65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 199
        },
        {
            "id": "14e75fd3-c671-4e0b-b976-e92ede203a00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 210
        },
        {
            "id": "be848180-5002-450f-94fc-f853f96051e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 211
        },
        {
            "id": "50b28230-7e45-44ef-915e-c8d36f2cda1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 212
        },
        {
            "id": "81d97000-105d-4cb2-8706-da0219101d51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 213
        },
        {
            "id": "6a82e657-9f3c-4a94-97f5-22eeadd78ade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 214
        },
        {
            "id": "6635e930-7278-410a-a1ab-abd78d62f3a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 216
        },
        {
            "id": "e38cadb3-0bd2-43be-bce5-8c5fddd21b88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 221
        },
        {
            "id": "ae5a20d6-5f42-44ab-9c7e-e021546e2365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 262
        },
        {
            "id": "92260c84-2a5f-47ae-9946-a094a1baf34b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 264
        },
        {
            "id": "8ae31183-c406-42e6-85da-9f006315d7e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 266
        },
        {
            "id": "93694940-bd7e-47f3-bd5b-f76ed6b001b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 268
        },
        {
            "id": "75e45e02-8ba4-491d-b4fd-39f1880acdf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 284
        },
        {
            "id": "6b0d0ba6-bd46-4a8d-a12f-a3cc03d4677b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 286
        },
        {
            "id": "554f654d-13b3-4e07-a600-5cc2f73d9b72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 288
        },
        {
            "id": "2cf56ffe-16ef-4949-8a1e-b9f9eab68834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 290
        },
        {
            "id": "e09e6cf3-4450-42f4-a6f5-9d1a3d821202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 332
        },
        {
            "id": "fae768eb-9646-4330-9326-2d2e49e7c70f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 334
        },
        {
            "id": "c6dacbf5-58cf-4dbd-8feb-8a6d265c61da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 336
        },
        {
            "id": "5c351e6d-6ba3-4307-9ebf-fca85f6d2495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 338
        },
        {
            "id": "9d8cbaac-5cf0-4052-b5a6-446d7ebf5a4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 354
        },
        {
            "id": "3b74af34-29b2-4f97-9898-d2fab954a807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 356
        },
        {
            "id": "73bb991b-fd75-430f-b5ca-2fd5e8297986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 372
        },
        {
            "id": "81a37dbf-d9ea-4923-ba84-daffedb8e18e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 374
        },
        {
            "id": "38f7d142-627e-4020-bc93-bdee08174625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 376
        },
        {
            "id": "2a9b3228-17cb-4db3-8306-e3c25628c4bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 416
        },
        {
            "id": "b7463c15-9ab3-40c9-a66b-c448ffc35bc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 510
        },
        {
            "id": "1e7ecd72-11e4-4090-8d03-025b243e0e55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 538
        },
        {
            "id": "9842e486-c876-46bf-99d0-8e3facde6506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7808
        },
        {
            "id": "1be77cca-e09c-488e-bf50-9cc4b2da7406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7810
        },
        {
            "id": "4166928e-9961-486f-b85b-f9d8b2e2c033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7812
        },
        {
            "id": "d0d92123-99a7-43c0-8647-9657e860dca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7884
        },
        {
            "id": "1719cc51-1d5f-47d3-99c2-1cdc33758324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7886
        },
        {
            "id": "97308e2e-b2c1-46cf-ace3-45d1a18f24e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7888
        },
        {
            "id": "65286360-b304-4199-8b4e-9419143c0f92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7890
        },
        {
            "id": "14437d58-cee5-476d-b0f8-d772e73109b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7892
        },
        {
            "id": "ce59dcc6-3ee9-4d63-a99e-0794f114b2ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7894
        },
        {
            "id": "db697eba-7230-4785-85b1-f8ad5d32e6ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7896
        },
        {
            "id": "1fc2c36c-39b4-47fe-bef2-523b41b00a0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7898
        },
        {
            "id": "0dbc0c8a-6151-4e59-8f19-57fbc8563516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7900
        },
        {
            "id": "80bcd670-0493-4e36-a80a-2e60ebd1f7a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7902
        },
        {
            "id": "6f3ede04-d1fb-4da3-8db0-549db848ba31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7904
        },
        {
            "id": "ac2e5bec-fe14-427f-93b2-a3340d0bfcb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7906
        },
        {
            "id": "e78a8eba-d968-4443-b5a9-b743f060e369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7922
        },
        {
            "id": "bbedab5b-a70d-4b47-9b05-c6264578e64c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7924
        },
        {
            "id": "90992106-62d1-4815-8a9c-1c595e731b33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7926
        },
        {
            "id": "d57227b0-754e-4d37-8750-fe7d391b6802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7928
        },
        {
            "id": "043f719c-652e-46a1-b681-45e356c9aad8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "9d13dd0d-c405-44e5-8bb7-53b2e88764d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8221
        },
        {
            "id": "f4e60718-6c91-42a9-af83-3c38cc741f5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 44
        },
        {
            "id": "28f37340-1ef7-4623-bfea-a686cfa1c2e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 46
        },
        {
            "id": "04efb9a3-a1c2-494e-b517-71439afc00fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 65
        },
        {
            "id": "40ddbd7b-a08b-4c47-89b2-fe27117b5e5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "21a177e5-039f-443f-a26a-48cdac997fba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 88
        },
        {
            "id": "a2468376-af71-44cc-af90-cf095b22621f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 192
        },
        {
            "id": "563ca76a-4e59-4dce-962e-eba305b5edde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 193
        },
        {
            "id": "b8c7b4fd-24f6-417b-8cae-5c8d596ff803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 194
        },
        {
            "id": "3eb165e5-be96-4b6a-964c-bb2c96688c6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 195
        },
        {
            "id": "fff2557a-0f18-4dc6-ab41-5b92b5415ad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 196
        },
        {
            "id": "e0b418ed-0597-46f6-9260-8c24fd4e3d20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 197
        },
        {
            "id": "0f4933e1-7ef2-46c4-81dc-f08a6b67dc47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 256
        },
        {
            "id": "086ca8be-190b-4d69-b9c4-7ebe593e5866",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 258
        },
        {
            "id": "a3154932-1c73-48c4-a9b3-080c9391ebf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 260
        },
        {
            "id": "4d12dc60-ccd6-459c-8d4d-dc41bea4f5aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 354
        },
        {
            "id": "8bcb84d8-0960-4237-8f0c-62c45158eafd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 356
        },
        {
            "id": "b7f0932f-8e41-47f7-8d4e-fd43776aebef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 506
        },
        {
            "id": "b36deb2f-a915-46fa-96cc-382effca254f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 538
        },
        {
            "id": "537ca2c3-5444-4e6a-8e51-0a873766ac4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7680
        },
        {
            "id": "0e5c1f41-3fe1-47a4-903c-1e7cac1e00eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7840
        },
        {
            "id": "5dc41990-b410-4653-af1c-23d2ddba636d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7842
        },
        {
            "id": "35c28b62-274a-4586-aa92-d5c1a02c3d94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7844
        },
        {
            "id": "3167d7d3-f0e3-4fa5-9859-5cbc25d2e666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7846
        },
        {
            "id": "864d2563-4c92-4969-8eb6-e7abd72a66c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7848
        },
        {
            "id": "13249ee7-cbcf-411e-85f2-e66a85efe483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7850
        },
        {
            "id": "0ea7d3f9-22c9-4efd-bec2-52cc6bacd23f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7852
        },
        {
            "id": "138c2878-1c75-4f69-abba-6d66c894b409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7854
        },
        {
            "id": "a4796f99-7b31-4044-97c2-d3016cc3149b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7856
        },
        {
            "id": "0d93b26b-3c51-4f5b-8d17-d2039ec3f379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7858
        },
        {
            "id": "9900ac65-eba3-4ee9-a233-d1ef2c1da76a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7860
        },
        {
            "id": "fd5b1f9e-b173-4ede-b0e1-5b6b6f1ad699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 7862
        },
        {
            "id": "9055730b-de0d-454e-bf66-3a36532ceaed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 8218
        },
        {
            "id": "be226223-03e8-4fb6-b49e-97d4edd98c9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 8222
        },
        {
            "id": "4dd8462f-ccc5-471b-b80a-543bd314d25e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "3cc04d5c-b9b1-49a1-85d7-2fec401cb5e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "89b64c1b-4908-4776-9d36-33aed497feb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 79
        },
        {
            "id": "1bb59e21-0d1a-4905-8099-3470d977da0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "1383f18e-f090-4620-95e4-cba8704141c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 199
        },
        {
            "id": "927e34a5-4f38-4b00-bbb9-320c3130b872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 210
        },
        {
            "id": "eb70004e-aedb-42ea-8cfb-e8b3d83e66c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 211
        },
        {
            "id": "f462e58b-853f-4a62-9685-d381405269f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 212
        },
        {
            "id": "e4744502-27e4-4366-a6c9-e029c6ffc2e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 213
        },
        {
            "id": "4f4c6ec7-24fc-4cbd-b8e7-614bba92e5c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 214
        },
        {
            "id": "40aed090-0cd2-4474-9ee8-dca1fb586b2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 216
        },
        {
            "id": "b76870c9-391a-4263-b9fc-a51f61322e0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 262
        },
        {
            "id": "b3b57453-89c8-4d8a-88d1-99e22f4d9813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 264
        },
        {
            "id": "7d81d6f3-1b2f-4fbc-a5ea-85f68b45ec01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 266
        },
        {
            "id": "a48a7e9c-ff59-4b8b-b101-ccaf7e5c8dd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 268
        },
        {
            "id": "1c9c3835-9cd9-47a9-8f3c-20ee427fc1b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 284
        },
        {
            "id": "d506d1ed-34e8-4043-b4d6-36f33ac8423d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 286
        },
        {
            "id": "0732c657-d934-4d06-87a0-7c5f119d5982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 288
        },
        {
            "id": "916c5ebb-3130-4505-aa3e-d48021d9e1f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 290
        },
        {
            "id": "d4e53d39-8b24-4a23-be41-c998ee7d08ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 332
        },
        {
            "id": "40946d28-e7a5-45f2-97e1-9672f3c8e678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 334
        },
        {
            "id": "9ecea0bd-5626-48c4-9b6b-4287fa88beb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 336
        },
        {
            "id": "ecf6fb3f-1c66-4b6a-8fd9-cd4fb4a0116a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 338
        },
        {
            "id": "80b41184-f6ae-4bb5-b3a7-1af7c0fbc132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 416
        },
        {
            "id": "c35afb96-29aa-435a-adcd-d2483cb44208",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 510
        },
        {
            "id": "7638fb10-89ab-4651-bba9-6398755a6367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7884
        },
        {
            "id": "7555436e-8e3f-40ef-856d-6a18ad6f5ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7886
        },
        {
            "id": "03f579ef-de74-4808-8883-5e95043c6d10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7888
        },
        {
            "id": "3394b2bc-2ff3-4fd0-a96e-07f1e7b07250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7890
        },
        {
            "id": "470fa6c9-40f9-46a7-bc72-bf2011b9d732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7892
        },
        {
            "id": "ccbabd31-0137-43b5-a4fe-196d855de871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7894
        },
        {
            "id": "475b728d-0913-43a4-a103-65ec50d6c264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7896
        },
        {
            "id": "804e303b-460e-48df-8f54-b927bda55ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7898
        },
        {
            "id": "a6fc30ca-3dec-46c0-a1e4-1704afe43292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7900
        },
        {
            "id": "5421879d-3e61-4853-84a0-4f22af7b32cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7902
        },
        {
            "id": "f9b4597f-c78e-477d-8b34-3997ba03da9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7904
        },
        {
            "id": "353161a2-b20a-48f8-84aa-58fd2dda119d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 7906
        },
        {
            "id": "fa04baf9-dbc4-4627-8300-cb4fbea4c170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 44
        },
        {
            "id": "60f02710-d0fd-4afa-9faf-bf551d353e6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 46
        },
        {
            "id": "3c9b3918-6246-4891-9d56-150e77f8ebbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "3b0e1bae-a4b0-4e0b-8002-3b33f1af3975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "7a0f6ddb-a05f-4736-8d45-27899d8dbbba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "d16826d6-3406-4b9a-8726-7be44c1e9585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 192
        },
        {
            "id": "a8da16a2-f4f1-4b50-aa87-2e63d4ede707",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 193
        },
        {
            "id": "6abeff76-e568-4f6e-aab7-23c9242538f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 194
        },
        {
            "id": "ffa53199-068d-4100-8e90-bb9e9c5c923e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 195
        },
        {
            "id": "6a09ca5b-30eb-4fd0-9aa2-dc88ab6c6a6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 196
        },
        {
            "id": "4c843886-c5c7-4d9b-a048-e68b95e5dc6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 197
        },
        {
            "id": "fec52e7d-b498-4c15-83c9-8662b6674909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 256
        },
        {
            "id": "ee0df76b-7f9d-4742-b3b6-ae6578cc8f0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 258
        },
        {
            "id": "cccfafec-8fd9-4c14-a219-0d5255ce43fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 260
        },
        {
            "id": "28fc2335-ab35-4b5b-943b-c4213baa5a3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 354
        },
        {
            "id": "d49e430d-6a6e-4556-9c55-467d8a2dedc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 356
        },
        {
            "id": "e111e963-b905-4215-b39d-5eea878d4803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 506
        },
        {
            "id": "522a41b0-1ea2-428c-ab70-2891764ab2ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 538
        },
        {
            "id": "60dbf74f-38a4-482b-b480-da84bf30996d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7680
        },
        {
            "id": "36b79165-a043-4c26-a1a3-3512758494c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7840
        },
        {
            "id": "0a48ed8f-a3bb-4496-8faa-46429b346c72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7842
        },
        {
            "id": "ad56f7da-087b-4dd9-8725-e6acce6843ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7844
        },
        {
            "id": "3d551ba5-a973-4c48-a53c-e2f6442e956b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7846
        },
        {
            "id": "00a6483d-ffb3-49af-91f4-ef29d0b87f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7848
        },
        {
            "id": "6b3dc239-a3a1-4ac6-8b9f-a8abf06f6a47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7850
        },
        {
            "id": "1033c489-0333-44a3-9942-9cd6bdb827bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7852
        },
        {
            "id": "42c7fb86-d175-4166-8eb0-f40017f4b8d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7854
        },
        {
            "id": "ddcc2970-0d01-4857-9a6c-ad7f191ff879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7856
        },
        {
            "id": "d29a2cbc-17e6-480d-a963-3d84227dc5cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7858
        },
        {
            "id": "1108dd34-1fad-4c95-982a-d02712d7e24c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7860
        },
        {
            "id": "b3474c47-6ce2-4a6c-90b8-7d22e0e98007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 7862
        },
        {
            "id": "f9db022b-86d9-488d-bdb7-632e4ffbf944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8218
        },
        {
            "id": "d4dfa418-2c9c-4ed9-8918-d51bbed31def",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8222
        },
        {
            "id": "cb08a756-d725-4432-8c74-07c2dac44752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 69,
            "second": 74
        },
        {
            "id": "3ab7e0a0-7f77-4706-9e5b-b500fe7b1c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "75188d0a-b739-4596-a752-6344e773da21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "cedf8333-2a62-4caa-9ecc-4320c224ec2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 63
        },
        {
            "id": "ee57a1a5-d617-42ca-9f42-2c685758316a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "670ec763-b496-4580-b505-a298cb3e69cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "69bd0c92-aca6-4b27-8489-21d781de489f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "2c5ad201-bc7a-43c3-bd8f-659330191d31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "cb524eb5-cb06-4141-82c2-9fda88ad3c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "a8a905bb-fcb8-4138-8208-e77ba5d3be48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "6a97eba0-e34d-4c53-813b-352aeb171bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "29a383f9-92e5-4ff8-a3f8-823378cf71d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "ea84fc44-ce49-4378-be8a-48885240f38b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "0a682eef-26d7-4245-b0e5-97311df1dd96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "fc249ec0-ce3c-481b-89cc-90e1075afe41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 506
        },
        {
            "id": "9732a5be-1c2d-4f67-b00b-5b56b345aed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7680
        },
        {
            "id": "10d5683a-3cb8-4dda-9b28-34afb8297db0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7840
        },
        {
            "id": "7e8ab8fa-e217-4d43-b65f-ef92cb9e58c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7842
        },
        {
            "id": "790abe87-4722-4713-ba84-129d3a599e81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7844
        },
        {
            "id": "c3183394-0743-4bbd-b4fa-9211cd783cfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7846
        },
        {
            "id": "187f5d8c-9e5d-4794-b3b5-d2feb2b3653c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7848
        },
        {
            "id": "6fa96e9e-5630-4752-8b4b-08019f5b70e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7850
        },
        {
            "id": "3a1d9e6b-a6ac-4552-b8d0-df4c5f56633c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7852
        },
        {
            "id": "db823e56-9a30-4880-88eb-5aeefe0d698a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7854
        },
        {
            "id": "63832b28-f1f3-42be-a177-f34f3040dd95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7856
        },
        {
            "id": "44754692-b93f-42f0-8b56-96da15ce18b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7858
        },
        {
            "id": "ced8338a-3c36-43d5-9c56-b0cc7627bfc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7860
        },
        {
            "id": "e5254c67-14ee-4c27-8016-a0dd8a8d4a05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7862
        },
        {
            "id": "edf76ca0-4d06-4bc9-9e74-8249375faaa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 8218
        },
        {
            "id": "17c1841c-c76a-4dbb-8862-9b1a5aa2f022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 8222
        },
        {
            "id": "e3e805bb-b51c-4b58-a319-a0bd5af963c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "1dfe2fc0-e893-4544-b4d3-5925aba5fa72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "6636c810-b050-4880-baba-f6e015fcfbeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "86d7ca00-4168-4057-b8db-3081db122f52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "1c558652-6257-48f5-9d03-ded6910bac05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 199
        },
        {
            "id": "e2ea3f14-e225-42c5-836d-8237b2a18a6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 210
        },
        {
            "id": "4963fcd7-08e0-4424-9ccf-1d023527c641",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 211
        },
        {
            "id": "da8bd15f-4eca-4cff-942d-119a4f073e9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 212
        },
        {
            "id": "123ee70b-fd96-4cbf-9371-9623116fe86e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 213
        },
        {
            "id": "9099b7b0-bd28-4873-8d62-984cebbd6f15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "72087190-5760-476a-adff-7254d9aacadc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 216
        },
        {
            "id": "31da82ac-6036-4829-8d29-5da32f636d51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 262
        },
        {
            "id": "9cdf0db7-9993-4aa1-adc0-24a4547ff9ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 264
        },
        {
            "id": "5ed8583b-33f3-43b8-93f9-cd4f58296721",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 266
        },
        {
            "id": "fcf87f95-85a1-4670-9501-6006a2ccf1d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 268
        },
        {
            "id": "9cd440a7-f4d8-4026-bcb9-7edd33616dcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 284
        },
        {
            "id": "978ee12e-27a1-4a9a-9dc5-93448f95fefb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 286
        },
        {
            "id": "88a871b8-15f2-47db-a845-693563657d14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 288
        },
        {
            "id": "88fb1858-1d5b-49a6-a01c-4140bbd7bb80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 290
        },
        {
            "id": "1fe5e867-3e8d-4d5c-aa68-e2187a79b6c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 332
        },
        {
            "id": "1e348232-d621-4071-a8ae-95f2dcdff67a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 334
        },
        {
            "id": "d7bb07a0-e986-473d-8e62-644978d73d6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 336
        },
        {
            "id": "7053ac4a-a76e-4b1f-bd3e-bd69913526b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 338
        },
        {
            "id": "1c5791a4-27eb-43a3-bd14-7c07d20aa893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 416
        },
        {
            "id": "3d48601c-ceac-4fa9-95e2-705e269f9bf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 510
        },
        {
            "id": "4258bdcf-c268-46f3-900a-492e0346f37b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7884
        },
        {
            "id": "f37815e2-159b-4423-b1fe-bc62b60a840e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7886
        },
        {
            "id": "d8d54da0-836b-41e0-9bb5-13000ea41de7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7888
        },
        {
            "id": "20867d79-a750-4856-aebc-d7d80c822df9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7890
        },
        {
            "id": "4abb35f1-e675-4e6d-ba72-c53b8e136abc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7892
        },
        {
            "id": "557df058-464f-4652-856d-e71dc22a71f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7894
        },
        {
            "id": "7c842bea-46e7-49f2-8110-fd1db660f5ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7896
        },
        {
            "id": "733a0961-320c-4ec3-abe6-cdd1734f79ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7898
        },
        {
            "id": "e1039456-b43e-4d6a-886f-a7ff05e2cf03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7900
        },
        {
            "id": "411b5871-b9e7-4599-bb38-426bec1c3701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7902
        },
        {
            "id": "3dbeb6bd-c8c4-4bef-8313-614ad748b574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7904
        },
        {
            "id": "c6081138-7966-40ed-9f11-7f5cba1d9f08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 7906
        },
        {
            "id": "3dd4d2c3-95d2-472c-a3a8-590390e1f043",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 34
        },
        {
            "id": "014ba3f2-cd91-497f-b45f-08575e2731df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 39
        },
        {
            "id": "77c3b82f-2968-426d-b65f-83af5ca5d6f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "1f9beb1b-1561-4ef4-9709-1547618dbc45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "3adb2b54-9896-45f2-86a6-1607cb9b0699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "f1394ecd-4a34-4375-ad1e-248e21c8f124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "4a58ab72-3f44-4eb5-988a-469688557ff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "f06c30ed-10c7-4fab-b52b-d4e4ccbf632e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "9fbdba63-99aa-49dd-a26c-20df4e944dd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "858b31e3-9519-45da-910f-a9a7624a48c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "35007667-69f8-47e1-a9c4-754a8e919ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 199
        },
        {
            "id": "549f39c4-548c-4758-823b-a395eb201aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 210
        },
        {
            "id": "f23af4aa-d58b-4ca5-ba9f-173df1273d94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 211
        },
        {
            "id": "53f575ee-a8e3-4399-93a2-427e7d59149d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 212
        },
        {
            "id": "c7871747-0dfa-4198-9186-9cc145ad34a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 213
        },
        {
            "id": "4aa27114-4780-42f1-b3be-5efd4234d160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 214
        },
        {
            "id": "55e96f32-9d85-41bf-9930-41f909d004e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 216
        },
        {
            "id": "4d8bb9fb-eba6-4d9d-b087-d1f9fc104514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 221
        },
        {
            "id": "c7ba876b-e8fd-4f6c-9528-32dba2799c9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 262
        },
        {
            "id": "0df26f0e-a021-4fad-a609-2893d51903e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 264
        },
        {
            "id": "540ffff9-ba62-4bba-931f-777b822d905a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 266
        },
        {
            "id": "8caccbe1-2bb6-4a5f-bdef-3bd5bc3bb323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 268
        },
        {
            "id": "a27464d1-f543-4eee-8ec2-9b28c20003f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 284
        },
        {
            "id": "648981bd-7198-4fe3-b0a3-98f3469264da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 286
        },
        {
            "id": "34d9fc62-97c0-4dcb-8540-1e3ccb2aa4e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 288
        },
        {
            "id": "cf4d11ee-33ab-4ff7-a7a1-de8624f1a8bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 290
        },
        {
            "id": "f0a86354-ed14-4038-8a4b-39ba73dbff51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 332
        },
        {
            "id": "1f0e5246-ff92-4508-841b-4689f0289e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 334
        },
        {
            "id": "d48abc10-0300-46b7-8b97-58c5ebacf362",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 336
        },
        {
            "id": "66fc1e4b-5caf-43ca-99d7-ad4f8f2e0787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 338
        },
        {
            "id": "668e1949-5558-4b58-8599-dd160b1942d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 354
        },
        {
            "id": "1d262620-b85f-4654-9027-905c79738b6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 356
        },
        {
            "id": "858cba73-3cc1-4d7d-9e42-faf2b2b17b41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "9308e6b5-9cd9-434e-ad62-78f3ed82acad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 374
        },
        {
            "id": "9443dc71-512a-4015-a2e8-88290bcf903e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 376
        },
        {
            "id": "0a769a4c-119a-4a61-a6b9-1da8c444db82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 416
        },
        {
            "id": "31f35769-e11a-48b1-aa54-083f9e67c428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 510
        },
        {
            "id": "7534839a-c546-4004-910c-8b7f66e90827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 538
        },
        {
            "id": "eddca065-8ed0-4e5e-adf1-813da1406c7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "4149830b-2757-4472-89a9-581c8d983a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "388e4750-b6ff-4097-bcfe-67d439c2a1d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "b97552be-024a-4881-b55c-b5ea216d506a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7884
        },
        {
            "id": "6902a4fc-e593-4c5a-8478-41039397a5a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7886
        },
        {
            "id": "7e9180bb-7ec5-4263-800c-c69686562173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7888
        },
        {
            "id": "b2b50841-c1a1-48c1-b49f-f859e3a26570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7890
        },
        {
            "id": "d028ff5d-5560-4d53-9620-2ce22c7cdbb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7892
        },
        {
            "id": "c196b860-6ed3-4060-995c-1ea3a63f72cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7894
        },
        {
            "id": "6eb62e03-28cb-4a2c-8ce3-a9917f823de2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7896
        },
        {
            "id": "b60eaf5a-746d-4208-8661-1748dc16b1e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7898
        },
        {
            "id": "af0bb985-d69c-45a4-aa6d-b012a434c0e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7900
        },
        {
            "id": "dec4aee5-bb0b-4af5-9c90-48ae6941a628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7902
        },
        {
            "id": "988bf46f-a25b-4bf8-a807-ca9bf7376033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7904
        },
        {
            "id": "d63c52b9-10f7-48c7-97cb-acf6535dc104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7906
        },
        {
            "id": "39ce7548-f604-4388-8960-6da45ebd7d3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7922
        },
        {
            "id": "73f873d0-ba1b-409a-b6c4-c8414cdfb779",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7924
        },
        {
            "id": "603ab42c-1b4d-4322-b9a4-7b6e6ec9fe57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7926
        },
        {
            "id": "dbaae608-06bb-491b-94cf-1733ab8ba876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7928
        },
        {
            "id": "89a44bb9-5c89-45e2-bb89-cead323248e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8217
        },
        {
            "id": "4893388d-2d97-4902-98d4-a525b4256090",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 8221
        },
        {
            "id": "d6b6e6f3-c324-444e-8ae3-529b3e07ba1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 44
        },
        {
            "id": "7da67324-773e-4af8-95fa-48399856aa0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 46
        },
        {
            "id": "4176dc25-949f-4f2e-99a2-c0e9005236c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 65
        },
        {
            "id": "89682190-59e5-42ab-88ac-c557becc4811",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "c188924e-ca50-4375-86ce-99eb36896859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "1efadba5-0b37-4054-a132-398b1510ebd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 192
        },
        {
            "id": "f76a3143-2906-4d7d-9431-b6acf96e8063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 193
        },
        {
            "id": "3e25b92b-e890-4444-adff-f919c135667f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 194
        },
        {
            "id": "8628c0b0-46aa-4ad0-aa84-de0398832825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 195
        },
        {
            "id": "5413272f-9f23-412c-9b17-7ae0aa98ed58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 196
        },
        {
            "id": "b77a1aaa-75ca-4578-9bee-7ea68146913c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 197
        },
        {
            "id": "058babb3-b5fd-4503-819c-28aa9d419661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 256
        },
        {
            "id": "1f7a81b1-0fe0-46f4-8097-3c222ffa591f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 258
        },
        {
            "id": "577521b4-caea-4185-9235-3a913d4564b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 260
        },
        {
            "id": "6e302c31-1065-45eb-878b-5749864b41cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 354
        },
        {
            "id": "94dde7ad-c50d-4580-8232-fe9e7b55e9af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 356
        },
        {
            "id": "d9474891-1e57-4288-81cf-db88316023ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 506
        },
        {
            "id": "09914199-f37b-4ff7-b840-58aefa50c6a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 538
        },
        {
            "id": "d3bc698e-9fa4-46b8-957d-22a59f4a57d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7680
        },
        {
            "id": "730c7343-f649-4854-a48a-03520c1c2fa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7840
        },
        {
            "id": "a1844d53-08a1-4500-86ca-003e0c6daf6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7842
        },
        {
            "id": "8bda7e7c-4d3b-4480-9adb-a1c98a798fec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7844
        },
        {
            "id": "82be56a9-fa17-4fea-9362-8cadfcbcdd6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7846
        },
        {
            "id": "c1b93b6d-7afe-40cc-aee8-5579b8a6c131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7848
        },
        {
            "id": "62596d62-99fa-4877-8a07-f7244982e6c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7850
        },
        {
            "id": "9d68a819-db57-4180-812d-73b527524c1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7852
        },
        {
            "id": "33ecf83b-4715-4c1b-bce6-8c398e47aec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7854
        },
        {
            "id": "0a353a89-6c86-4d06-b049-739e95978935",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7856
        },
        {
            "id": "d4741c80-b540-486e-bea6-671d58634ef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7858
        },
        {
            "id": "d1d45a8c-4ec6-4575-b9f1-18cf169694b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7860
        },
        {
            "id": "aa4375ad-5127-4b35-8194-b1b385bc7186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 7862
        },
        {
            "id": "435e4d36-0544-4ebd-bc9e-407c6c3a6da5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 8218
        },
        {
            "id": "cd3ce46b-8745-412e-ab51-30893d8df3fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 8222
        },
        {
            "id": "5c62c714-82d9-4dcc-ab55-d2512d712845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 44
        },
        {
            "id": "8528a98c-302d-4ede-8c33-694d8cd64808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 46
        },
        {
            "id": "f4a9485b-b9a4-461b-b528-ffbf1b465472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "a331d777-460f-4f0c-b74e-239d22c73377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 88
        },
        {
            "id": "2e347146-eaa7-46d7-b24c-ddb7ea50920a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 192
        },
        {
            "id": "09ce4e7d-37c9-4f54-b224-d7f7b9feba37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 193
        },
        {
            "id": "f9581992-5b63-42ee-8a1a-67b17613d804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 194
        },
        {
            "id": "d0e8baa5-606c-496d-930f-1764eb2dff46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 195
        },
        {
            "id": "4dc920e3-9a20-46ba-a594-e3f7ed9af855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 196
        },
        {
            "id": "e25dc6d8-51c8-4963-b8f4-09b578629176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 197
        },
        {
            "id": "d5aca925-5aa1-43c8-b74e-8cf2120238a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 256
        },
        {
            "id": "f54055d4-9fbf-40dc-aaec-548aca5644c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 258
        },
        {
            "id": "387ed422-45cd-46c4-a95d-75fddc8124cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 260
        },
        {
            "id": "698b348f-4a31-46ea-83fc-7793bef2e39d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 506
        },
        {
            "id": "c44b7c9e-8e18-459a-9953-12a5118e7caf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7680
        },
        {
            "id": "7c62727c-a23a-48d5-b7eb-a3b2322831dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7840
        },
        {
            "id": "381a28b3-acaa-40c1-87d9-346e0ceeefd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7842
        },
        {
            "id": "4ff9368d-5948-424b-afe3-56c0bfd8d16b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7844
        },
        {
            "id": "f94433f0-6416-4ce9-ac6b-176ba9bc23b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7846
        },
        {
            "id": "3388a0e3-9986-4635-ab61-cd9e7bc91727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7848
        },
        {
            "id": "becf8088-9931-4c5e-9c11-58e14fc62146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7850
        },
        {
            "id": "3267897d-21c1-48c1-9772-56fca0895af7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7852
        },
        {
            "id": "7334c416-9b6b-4a5d-a804-5b23bed84549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7854
        },
        {
            "id": "1a96e0c8-6dc9-424d-b4eb-404ca86e3ee3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7856
        },
        {
            "id": "41649e6f-023e-48b8-9ee0-68908709fdc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7858
        },
        {
            "id": "9e46cdc9-d36a-43f7-8263-a890d749fc8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7860
        },
        {
            "id": "dd3c6315-9ca3-4cd3-a6e7-c9a82e962df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 7862
        },
        {
            "id": "a51e666d-65be-4d18-a773-d98f87222074",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 8218
        },
        {
            "id": "59f5f016-1cfd-4978-b856-7a3ec70f769d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 8222
        },
        {
            "id": "1c2bbe02-d81a-471d-b7c3-047293bfbf94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 44
        },
        {
            "id": "c91d55cf-23b2-4fca-9c3c-dffe2d6ee18d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 46
        },
        {
            "id": "5b1c6155-d9bd-4359-82b4-60bab70d1cc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 65
        },
        {
            "id": "b325494a-d7ea-4a1f-a3ea-2b9d26ed82b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 84
        },
        {
            "id": "27c33f06-161c-45e2-86af-4ae34d2fda00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "89078cf5-75fc-45ed-84f7-332ee28857e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 192
        },
        {
            "id": "7c9abfcf-870e-4bcb-a020-ed0e67cf5d3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 193
        },
        {
            "id": "48292135-2c99-4671-9c7b-f1682912c9ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 194
        },
        {
            "id": "1bfeda42-90ab-4496-ae9a-36f2ae5e8412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 195
        },
        {
            "id": "c8cbe507-6895-4fd6-8c26-35331ff0100b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 196
        },
        {
            "id": "56c9ed16-0b7e-4bbf-b131-245d9b8fc3de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 197
        },
        {
            "id": "1122a0a0-8f7a-4dd7-8930-611353f2a5f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 256
        },
        {
            "id": "f6a7973d-d23c-4872-b39c-2c90bbc430f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 258
        },
        {
            "id": "8beeab03-04bb-4023-908f-61a17df34768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 260
        },
        {
            "id": "c3a6d2f4-eea7-4b97-8e2f-c53582b302f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 354
        },
        {
            "id": "87bb43aa-d05c-46ce-904b-d389242c53d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 356
        },
        {
            "id": "d8a1eb69-daf9-4359-a42f-f8e0f28b823f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 506
        },
        {
            "id": "c06a0c4e-ab12-422d-85bf-fcf3ecfbe3c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 538
        },
        {
            "id": "ecba3f3d-b6fe-41b4-a657-b030ebc3d576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7680
        },
        {
            "id": "4b089677-b61f-4b92-bc65-585dd833112a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7840
        },
        {
            "id": "d0bef344-20e1-45e4-a5a0-cc7d0a75ac62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7842
        },
        {
            "id": "99b71199-5819-47fb-be4d-47d99c77d7f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7844
        },
        {
            "id": "3631fc8f-d431-4cf3-a35a-db3e253e8765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7846
        },
        {
            "id": "5135911b-5057-431b-b44b-7cdbdaf382f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7848
        },
        {
            "id": "cca7f126-6c1d-4609-861f-6f4bfd34ad50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7850
        },
        {
            "id": "2335e3e1-e201-42f0-ac9c-8bfcec2f9578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7852
        },
        {
            "id": "a560f0d0-506b-43a0-8faf-e2cfd7da7b1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7854
        },
        {
            "id": "d7745c72-2932-40f0-a50a-53e719e83605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7856
        },
        {
            "id": "bd874e2a-90ff-4609-9bba-043c951731e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7858
        },
        {
            "id": "c0b5dbc6-b15b-44e4-b202-f680d6a4aca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7860
        },
        {
            "id": "bfb5de75-7f76-4cd4-87fa-c1b81b4b365c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 7862
        },
        {
            "id": "83c6c812-22fe-4f79-8846-634ab3b50bc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8218
        },
        {
            "id": "eb096c12-6478-431a-8295-ab45c2dce196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8222
        },
        {
            "id": "3ff973e2-9589-4622-837d-a1704920972e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "d5ea0365-fa7f-4487-aa81-541b725341e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "3fafc25b-7a6b-4963-9287-4456e96e5fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "d741159b-3415-40bd-ae13-b6787ecd2c1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 63
        },
        {
            "id": "c79b8e7a-7cfa-4a63-aa0f-45bbe529a527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "868a9350-0a2b-4f70-b149-dccb457d7e86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 67
        },
        {
            "id": "ee81ef54-d2a3-4d48-bcc2-d4d9a07b0321",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 71
        },
        {
            "id": "2339ffbd-77ea-43ed-b999-dc4e702def95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "3ecd0a7c-50c7-4fa3-9603-0e8fdea70b88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 81
        },
        {
            "id": "ebcd6e17-39be-44f9-a9e5-d8cc23d15726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 84
        },
        {
            "id": "3e3970cb-b403-48b4-9b18-ad191baf5a28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 97
        },
        {
            "id": "9511786c-19ed-4aa5-a083-bdc21c67b24f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "8858ebfd-527f-42e0-8fe0-34e322aea41e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 100
        },
        {
            "id": "3dc21828-48ca-4f57-861f-369bb27571c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "f69281f0-274d-4b61-8925-09d319075be4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 103
        },
        {
            "id": "75aadda3-6738-42e0-9b63-d1c129734ccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 109
        },
        {
            "id": "089fecfa-d71d-4921-ab8b-9eb85aee3376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 110
        },
        {
            "id": "6a2f26a5-fde6-4703-96c1-0d2f8720bf8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "7f2f1195-e00d-4c6b-8c65-f1a7ea9738fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 112
        },
        {
            "id": "34afff93-9de9-4cc7-b647-8369bde38174",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 113
        },
        {
            "id": "1fbc13d6-1186-4b05-93e7-ecb2ae68e36d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "6c20bbd6-10c4-441a-a554-1e99ff91a768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 115
        },
        {
            "id": "398530ba-1442-4950-9404-d7241d496c19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "cdc4e041-a31d-48ac-ba9e-f777cee29f32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "d1ff5888-911d-4d37-8ef6-18180f0b5ef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "c9fa9908-74a3-4049-932f-db6ad5eeef65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "2becbb9c-f2ca-4d19-bb02-2a0aeb2dcf6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "9f6a8efe-97c4-462a-b019-9dd36c48ecb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 122
        },
        {
            "id": "f8c900e7-266e-4f94-bbcc-b8a25019bc7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 192
        },
        {
            "id": "237f2703-3810-4cde-9868-b223b198e7e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 193
        },
        {
            "id": "073ea228-0baa-44c2-b742-884aef2c1e50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 194
        },
        {
            "id": "53214726-113e-4b76-8b63-949736105098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 195
        },
        {
            "id": "553ed1d9-680e-4e92-a3eb-8b9bcf73a32c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 196
        },
        {
            "id": "641c406e-0b2f-4e2f-906e-95236dadc296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 197
        },
        {
            "id": "abc9f3e4-eab0-4c16-ae9a-193ee7be426b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 199
        },
        {
            "id": "9c5bd65a-6c62-4cc8-85b8-c563fb08d775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 210
        },
        {
            "id": "1ee2c115-b852-4392-9e16-5fc7caf37529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 211
        },
        {
            "id": "85c6c2d0-eabf-4a4d-97c8-22a7eb802b9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 212
        },
        {
            "id": "9e5f3622-a18f-41fc-9a1f-01e469012d11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 213
        },
        {
            "id": "7cea7d35-9eab-4006-bfc6-dd428b89daa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 214
        },
        {
            "id": "1225fba5-d1e1-420f-b731-45846c4c9660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 216
        },
        {
            "id": "3f106844-9794-4e87-9ccd-84fb50393393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 224
        },
        {
            "id": "5943bde2-b69f-4199-89ee-c76f83776eb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 225
        },
        {
            "id": "7f5234d0-c517-4f38-babd-108941ef466f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 226
        },
        {
            "id": "fc8bd593-e5d0-468b-a924-fd0a9429dbbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 227
        },
        {
            "id": "0081a450-2e45-43bb-85b4-da3dd01cd710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 228
        },
        {
            "id": "f17184c6-c3c7-44de-848a-a4b73545d191",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 229
        },
        {
            "id": "7757a9e9-0a69-4085-ac93-d3dfe0525852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 230
        },
        {
            "id": "312dad53-00ab-42d1-81d2-43423dc7529c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 231
        },
        {
            "id": "3ea48aab-17c9-4d92-84f9-cb20e25d1baa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 232
        },
        {
            "id": "f55fd25d-90fb-40bf-ac7e-0a229d95c29f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 233
        },
        {
            "id": "880e1db5-bde8-47f0-b46d-12727a05d34b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 234
        },
        {
            "id": "58647b76-42c2-4870-9327-9afa9a11e9ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 235
        },
        {
            "id": "d96a3ea9-9adb-4126-b1e9-ca6a4ae5e33d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 242
        },
        {
            "id": "44c4b46e-2809-4c5a-8585-00a1117262c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 243
        },
        {
            "id": "cceceb4c-3043-424f-9813-0deae3bfb016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 244
        },
        {
            "id": "1805e98e-20ec-4e40-acd4-de17fb087adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 245
        },
        {
            "id": "245a4f2e-75c4-49c3-8e60-1c4f286e95ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 246
        },
        {
            "id": "688ca24e-a610-4d3f-ad6f-178e9ded71d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 248
        },
        {
            "id": "35b5e2e2-cfd6-47c5-9128-269187da4f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 249
        },
        {
            "id": "80344f12-6306-4112-93da-a08c1a39dbd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 250
        },
        {
            "id": "c9ce77bd-22df-4ef3-aadb-1f5657e4328d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 251
        },
        {
            "id": "837bad84-b43f-4704-b297-81a9b38cfcf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 252
        },
        {
            "id": "51630a5f-6d6c-4d1c-8394-eb77f9244fd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 253
        },
        {
            "id": "97faebfa-13e9-4362-9dc1-81f44c236464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 256
        },
        {
            "id": "81256f0f-bd5e-40ce-9cb8-66d349a12092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 257
        },
        {
            "id": "c1047612-a41a-4fa1-b0b6-f0e256808d89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 258
        },
        {
            "id": "4b2d2fec-b2ab-4935-8f97-886b27f7224f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 259
        },
        {
            "id": "079de9aa-3e80-4b3e-a596-aae31e219308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 260
        },
        {
            "id": "d3286db1-d3d2-4fe3-a82d-67a44cd6aa2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 261
        },
        {
            "id": "800b1cc9-9351-4210-a024-6e755ca4912e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 262
        },
        {
            "id": "947b99b4-f62d-4807-86b2-55c8d1b9acb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 263
        },
        {
            "id": "d9490a62-db28-4f9a-93a9-f4dea23ea7e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 264
        },
        {
            "id": "b3be936f-89c2-44ff-8b0d-58519629ad45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 265
        },
        {
            "id": "66680fe6-3942-49a9-8ae4-3dc49d243bda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 266
        },
        {
            "id": "35548bf8-5b2a-4653-9bc0-47b903e8b84c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 267
        },
        {
            "id": "ce01df1c-94b2-415b-885e-c14e4deeeddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 268
        },
        {
            "id": "d1def268-de43-4a40-b2ba-6456c079258c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 269
        },
        {
            "id": "fd469320-bf5b-4b20-9cbf-ee8a1d243391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 271
        },
        {
            "id": "8bb775a6-7a18-4b76-964e-15786fd7c450",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 273
        },
        {
            "id": "02ca865f-9486-42c7-9076-27fbacbd7586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 275
        },
        {
            "id": "8e5a7b82-5420-4bac-9846-b5f403e35381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 277
        },
        {
            "id": "2710b67b-63f0-4491-a489-66dec6dd5ded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 279
        },
        {
            "id": "5c635aaf-d554-4f02-bd14-a85fadd100da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 281
        },
        {
            "id": "633d6cf5-11da-4ff0-b46c-b837b254cc02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 283
        },
        {
            "id": "3d254103-9ec6-4b54-aed0-4fcd5e3ca261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 284
        },
        {
            "id": "76f1672b-ceb0-4e5a-a782-2433553fba42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 285
        },
        {
            "id": "d35b66fe-59c8-4c88-8fb6-1d6c175b8b9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 286
        },
        {
            "id": "6bfbe76b-28cc-4181-85b8-8285239cf714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 287
        },
        {
            "id": "83dfd4ab-451d-49f5-b60d-07c53bd035bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 288
        },
        {
            "id": "1f83919b-796c-4dc7-bc9f-353c74855df8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 289
        },
        {
            "id": "fefc06a6-ffd4-45ec-90a5-6f7c4d70ace4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 290
        },
        {
            "id": "b1a6457f-689a-420d-8f89-f0d5e015027f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 291
        },
        {
            "id": "ca5942c8-4e88-40cb-b195-34da44161982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 312
        },
        {
            "id": "300f31b1-dede-4e8f-869d-432f21fccd2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 324
        },
        {
            "id": "fa2fd6d3-0355-40aa-ab84-53d1bb8d90e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 326
        },
        {
            "id": "286a3cb5-7e6d-4951-9208-58b2e1025b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 331
        },
        {
            "id": "cd31a9cd-f27c-407b-b2ef-3bd8323924c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 332
        },
        {
            "id": "402881d9-a871-4adf-8b55-cf4fea734bc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 333
        },
        {
            "id": "d2a7461f-bc5e-4ad3-b3fb-a6887b220c18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 334
        },
        {
            "id": "447db75c-14e1-443c-8cb1-e9b5d4744814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 335
        },
        {
            "id": "83aab83e-3bfa-43bb-8160-371e1b54cc24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 336
        },
        {
            "id": "eab0d0c1-79d3-4218-842f-a488615759fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 337
        },
        {
            "id": "8c25f04b-ee25-49de-bd9d-a64841a4baf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 338
        },
        {
            "id": "8ace4a0b-b628-4bf2-81a2-f9f8d05865dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 339
        },
        {
            "id": "3c19f4fc-2738-4a22-aaa7-f60a8c9b3c56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 341
        },
        {
            "id": "6779acf9-e7df-4e86-a055-3f08aa23a0ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 343
        },
        {
            "id": "684e790c-53c6-4d78-839a-b05a2cb1b1db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 347
        },
        {
            "id": "489bb301-50d7-4e0e-8897-6d4ac7c32c58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 351
        },
        {
            "id": "3e8300fc-3ce4-4b96-934d-ce2e590648fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 354
        },
        {
            "id": "972d3f77-609f-4333-ae4d-a0d54c248959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 356
        },
        {
            "id": "2c54802b-ba56-4886-97c2-fbe0d15f6296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 361
        },
        {
            "id": "f2977748-989d-490d-93d9-6a42b7a7bf1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 363
        },
        {
            "id": "2770fb6d-5508-4478-8bf2-6726a0657871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 365
        },
        {
            "id": "3d1ce057-c77f-4042-bb03-bacd7e25918e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 367
        },
        {
            "id": "fa834dd0-b4ca-44f1-90dc-ae61e39e1566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 369
        },
        {
            "id": "34a5d742-dad2-4263-81a8-73676f3c176a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 371
        },
        {
            "id": "c075e5d4-4a94-480f-badc-dad16a4b13b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 373
        },
        {
            "id": "c98ffe2c-5768-4ae9-a979-1a7b623226db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 378
        },
        {
            "id": "544a9e16-32cb-4245-b820-981a3a5a5bab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 380
        },
        {
            "id": "de4ebe5e-5524-4758-9fbf-3f8c326ff0b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 382
        },
        {
            "id": "a780aead-706d-4a75-9154-9b0326f90aa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 416
        },
        {
            "id": "4a8aec64-42e5-45c6-b517-ffa767c4e0a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 417
        },
        {
            "id": "3856c7e4-bc8b-4769-a752-e06546598858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 432
        },
        {
            "id": "3e2976e0-2379-43f6-ad6f-2f2bc926a339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 506
        },
        {
            "id": "540a760f-2e66-4115-b185-8532c8351c7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 507
        },
        {
            "id": "a9578531-91e4-4871-b400-b3679acb0c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 509
        },
        {
            "id": "29bcac63-1e45-4027-bcc0-cd728bf7c68f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 510
        },
        {
            "id": "ed98f590-f00a-45ea-a957-4baec2d2163f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 511
        },
        {
            "id": "4e3b26c3-78b4-46ed-b9a4-5dca5be2b5ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 537
        },
        {
            "id": "75285916-72cc-48ac-baf3-4cc643547e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 538
        },
        {
            "id": "8e20f19d-8a0b-4f20-96d3-9c3edc0ae035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7680
        },
        {
            "id": "4b0bfc8d-ee86-4910-a7b1-9153c52fd815",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7681
        },
        {
            "id": "0e2d9bce-f099-483e-997e-88d759d0dfdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7743
        },
        {
            "id": "12854bad-947d-42ca-bfc4-632861b8b776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7809
        },
        {
            "id": "5d29a852-36e9-4eff-9775-4bab696ddc93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7811
        },
        {
            "id": "024838ea-aac3-4da4-9531-2fb91df4be05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7840
        },
        {
            "id": "e43302eb-b937-40be-92be-fe8279951091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7841
        },
        {
            "id": "c50a802b-6de2-440d-934f-dab8fd480ae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7842
        },
        {
            "id": "c699cb67-d70f-43fb-8da5-43efc50d4931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7843
        },
        {
            "id": "c4ff1cda-2f34-48e9-bef7-ff348fbc0ddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7844
        },
        {
            "id": "7dbd3585-b605-4e75-87cf-a33fe4f073ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7845
        },
        {
            "id": "d6e8a068-da3f-4df3-9629-b6efd301b359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7846
        },
        {
            "id": "da8fa850-1540-4aa8-ab6c-10ceec0b50d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7848
        },
        {
            "id": "cad88d41-bfde-4b6a-ba44-eb001e96b6cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7849
        },
        {
            "id": "44a49b76-5bea-47e3-bba0-b3b51e76ffe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7850
        },
        {
            "id": "4149a6ef-86ad-4055-b182-bf2159af24ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7851
        },
        {
            "id": "f1e625ac-4d20-49d0-b29a-fe5d49f985f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7852
        },
        {
            "id": "8540e9fb-d799-4c84-b68c-0c05ee2be277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7853
        },
        {
            "id": "c54cd7c1-6de2-4854-abd4-755fb4b7fb11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7854
        },
        {
            "id": "802f1a2f-6c93-4743-938d-9222d312060a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7855
        },
        {
            "id": "c12b7111-b795-44f9-b394-3608711659ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7856
        },
        {
            "id": "1d1465b8-ad9b-46ad-a470-3559a79c81b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7857
        },
        {
            "id": "d4d21cb2-85b8-4e1b-b854-005d8cbfed85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7858
        },
        {
            "id": "9feaac80-d623-4cbc-8e83-a925bd73c1ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7859
        },
        {
            "id": "38390c95-fa09-4267-b9f5-0d47fe0aebb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7860
        },
        {
            "id": "1ccc86b9-9850-4b68-a0b0-c01ec303bc8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7861
        },
        {
            "id": "08d4648d-8f08-4be6-a3be-9d8e9a354450",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7862
        },
        {
            "id": "a391a6dd-f8d4-4e37-9a25-a3e08ede46c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 7863
        },
        {
            "id": "64cf159e-52c1-4a49-8588-a64b1425410a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7865
        },
        {
            "id": "a250d21c-dcba-41aa-b91a-5024a6e6b51e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7867
        },
        {
            "id": "e90ab20f-04b2-4a4f-bbf7-96a3e8ff19aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7869
        },
        {
            "id": "d73a0a8a-4cb9-49d0-8bd6-4414984202d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7871
        },
        {
            "id": "92840e82-5ae4-471a-9ba6-9b19321a1959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7875
        },
        {
            "id": "f0c58ef4-5eac-4e65-9678-ba43807232bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7877
        },
        {
            "id": "5bb08018-9095-4303-8c9f-6302d9bc5a41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7879
        },
        {
            "id": "88036ee6-a3d8-49fb-9a40-2b2983210e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7884
        },
        {
            "id": "fefbfb3e-e025-48d1-97ce-9331a7f6ccca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7885
        },
        {
            "id": "8415f3be-f31f-4f50-b7c1-403b93758295",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7886
        },
        {
            "id": "1389c1a8-808e-46d0-a78e-0b01a6c7f0d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7887
        },
        {
            "id": "c93b875d-89d5-498d-ae50-03f8a7454e68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7888
        },
        {
            "id": "ea73c433-f9b6-44ee-8891-567e42fe20ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7889
        },
        {
            "id": "e1c17643-bd8e-49f3-bff6-4fcdcab53062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7890
        },
        {
            "id": "43ae33a1-4dbc-4c0c-a896-e9248b3b637d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7892
        },
        {
            "id": "4edf0a8f-b756-4752-9790-1e3c4556070a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7893
        },
        {
            "id": "95d439ca-e4e3-4bdb-8c96-b382fc35a4cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7894
        },
        {
            "id": "f52292a7-163e-4877-9a33-1b4b1c959988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7895
        },
        {
            "id": "f59d9036-3124-4183-843f-73d39f24a2b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7896
        },
        {
            "id": "c2423985-6aa5-4f0c-b8c0-bf5e142deef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7897
        },
        {
            "id": "2444efce-22ca-442e-98bc-1799624cb1ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7898
        },
        {
            "id": "5a053a58-06ad-4252-a7d9-bafe35cdfbce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7899
        },
        {
            "id": "49c2a18b-d032-4e9d-892a-23d27b00963a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7900
        },
        {
            "id": "496cad41-c674-4664-8779-4fdded0d2114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7901
        },
        {
            "id": "221316a4-c41d-44fa-b86c-27a88bdf670f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7902
        },
        {
            "id": "3a57d6e0-61f4-4d1c-a6ad-8f13de2d7af8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7903
        },
        {
            "id": "bcc061fe-2f2f-4943-afa0-aad87642dc39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7904
        },
        {
            "id": "a1284ff7-c970-4f49-9c30-54152d9952f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7905
        },
        {
            "id": "f55d2cfe-6067-42b1-8c05-a13e97575c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7906
        },
        {
            "id": "7ea63aff-694c-40e4-8c11-4daaab60d16a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 7907
        },
        {
            "id": "374fb774-6d34-4466-9700-ffc291fe9d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7909
        },
        {
            "id": "2a118fa7-e6c8-4dc1-9eb5-836ae33c82e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7911
        },
        {
            "id": "a313af8e-8d9d-43cd-9dbd-18168e94588d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7913
        },
        {
            "id": "cb0ce52d-cf46-4c91-921a-6d52cc0bd67d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7915
        },
        {
            "id": "572c2e75-66b8-4db0-97d5-d73e0883e7f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7917
        },
        {
            "id": "db9d3470-febc-4674-b54a-0cbc664b20ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7919
        },
        {
            "id": "68659518-beb3-4d3d-870e-43c5f5c9a976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7921
        },
        {
            "id": "44ab1f04-3cda-4482-a663-2e64c966571f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7925
        },
        {
            "id": "aa483b4e-63f7-4cfa-92b8-c8d7fc312785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8211
        },
        {
            "id": "6b5ff781-05b6-417c-9402-a4b4188f2f90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8212
        },
        {
            "id": "38645690-048c-42b6-a398-fbdf007ee589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8213
        },
        {
            "id": "0ddda2e5-36f3-417a-992e-0bd7b1fd63e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8218
        },
        {
            "id": "803a465a-5c84-457a-acb5-e63142631412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8222
        },
        {
            "id": "2bf80811-0196-465c-997d-b79a1622fc02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 44
        },
        {
            "id": "91eda1d5-972e-460a-92ec-eb6c51910b6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 46
        },
        {
            "id": "2ae6d1ac-0685-4947-aade-577bf8748a1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8218
        },
        {
            "id": "35f19359-4b3e-47ad-b82b-b0e5ad3adc0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8222
        },
        {
            "id": "698bac71-9b56-406e-a6b5-2042e9d1e480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "ecb5bfdb-5706-46dd-939b-7d53f8415bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "e0d393c6-477a-41c1-b459-2a3c05a59a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 63
        },
        {
            "id": "539d8c86-9aee-463a-97d4-25e2be9792ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "cf6409aa-1844-4855-877a-6ebe35f5d85b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "0b1ac532-ae88-43c9-9f2e-5317f7c732ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 99
        },
        {
            "id": "060d063a-9e56-4407-b4aa-5ad299e421d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "a98ae1b0-8e65-4c22-a999-6a6a131e4a89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "09bd7b56-1e06-4e4d-9f8e-7c1e479f37fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "132f251f-b688-41b9-91a0-34d943cb73b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 113
        },
        {
            "id": "d910ac70-6b2b-4bfc-b70f-ee1ece0bfc60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 192
        },
        {
            "id": "ae6b5ca0-e7c8-4bef-a782-e43b4d52009b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 193
        },
        {
            "id": "f0075270-6d06-4a5a-935c-498f45451bcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 194
        },
        {
            "id": "60257a0b-ee77-43b6-8d42-1acf0f8169ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 195
        },
        {
            "id": "9f4b1796-1e76-4656-9d1e-ba378fb70e60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 196
        },
        {
            "id": "3d6b67aa-4495-4df9-a8b0-eb2520f39cdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 197
        },
        {
            "id": "53b43ed9-6fe0-41ac-93ab-96199068be05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 224
        },
        {
            "id": "fa23d2ce-d418-45a1-b2f0-2ea3ce0ce456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 225
        },
        {
            "id": "86e82a4f-4807-414f-9562-afc34e9624db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 226
        },
        {
            "id": "6a487e9e-4b9a-42a0-9d68-8d37cd09e732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 227
        },
        {
            "id": "008b1841-428a-458b-8784-6c6e77ff3eea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 228
        },
        {
            "id": "4a1d7487-9b32-490e-bc65-b3c09f0e08de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 229
        },
        {
            "id": "58a3484b-09ed-4c06-8b8d-73a6df5866be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 230
        },
        {
            "id": "f223bac2-a642-439f-860e-76f277f76e75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 231
        },
        {
            "id": "7829926b-92f4-464d-abd4-7383647f848d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 232
        },
        {
            "id": "d991bcdd-4243-4d50-a5c8-1c5e3c25c52e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 233
        },
        {
            "id": "5392e64a-d447-43f2-87a1-b16b6bb3c110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 234
        },
        {
            "id": "b360d3cd-3cde-4a19-929c-51fedf5e25e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 235
        },
        {
            "id": "43103d52-c583-41d1-87fa-97f09c82662f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 242
        },
        {
            "id": "fb037b34-c493-4f34-b767-e4517e348769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 243
        },
        {
            "id": "78b138d4-7bd9-47f7-8009-0c99887c6a58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 244
        },
        {
            "id": "f651f74d-810e-4d98-afeb-336718d07171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 245
        },
        {
            "id": "4d04774d-44d9-42fe-b007-efcf5b7277c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 246
        },
        {
            "id": "414f107a-1431-403d-b6b9-52e01432437c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 248
        },
        {
            "id": "630c0265-e512-4c8f-8fb8-a354227e3e1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 256
        },
        {
            "id": "18282781-14a2-4952-ba07-1e4d54a2323e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 257
        },
        {
            "id": "d9f76356-4b87-43dd-b348-ef0e224834ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 258
        },
        {
            "id": "dbedb6e9-a600-49d7-af90-969ace5855be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 259
        },
        {
            "id": "0fe26ce4-d51c-4ad6-9983-50dd38221db9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 260
        },
        {
            "id": "cfc8922d-cc62-4175-9dba-35ca40474dc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 261
        },
        {
            "id": "fc5816db-e171-455c-a1eb-1fb8299d598c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 263
        },
        {
            "id": "0eb0f55c-1f12-4a37-ac55-9a287dcdc032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 265
        },
        {
            "id": "ded6a46c-f52c-4ec4-b718-0c03aa919b33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 267
        },
        {
            "id": "681f3132-de5c-4d11-87ae-d51d0194af1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 269
        },
        {
            "id": "bcbf767d-03ee-454b-a7a0-14add90ce879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 271
        },
        {
            "id": "e31edbc6-9a23-4323-966a-f45d3cdffcb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 273
        },
        {
            "id": "7b4de0e7-2b89-47ad-be80-68b0852d923e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 275
        },
        {
            "id": "69a4a30b-c20e-45ef-86a9-35ad11382b6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 277
        },
        {
            "id": "5931dcd5-bcdd-40da-92d1-483178e15e8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 279
        },
        {
            "id": "80348281-4757-4430-b2d1-0a80856469bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 281
        },
        {
            "id": "b627e6bf-f38e-4ef1-a57f-437c1a595972",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 283
        },
        {
            "id": "cbd54c48-4b9e-4156-95f5-61b1aac453a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 333
        },
        {
            "id": "0ec94528-a08f-4603-b0eb-f22cd1d02ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 335
        },
        {
            "id": "6bd76f0e-cc73-4b38-b7d9-7fb96e223128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 337
        },
        {
            "id": "af5c00bd-9c44-4410-88c0-915bcbe39f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 339
        },
        {
            "id": "f4afee8c-425a-49f6-a360-0bae5bac2832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 417
        },
        {
            "id": "df3ea194-9001-4f47-b673-461ce394355d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 506
        },
        {
            "id": "f281d4c6-88c7-40ef-91f6-9c1fae5c7677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 507
        },
        {
            "id": "007da01e-daa3-4a74-935c-ea4d7499e0f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 509
        },
        {
            "id": "ebb5cab2-494d-440f-9963-4f73208e58d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 511
        },
        {
            "id": "9e4c713e-4e14-4e0f-8916-bbb044585e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7680
        },
        {
            "id": "d63ab3f6-e814-4cab-b238-020a74190e63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7681
        },
        {
            "id": "f9c9f967-15e1-4f13-bf8c-c4b7e8cb80be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7840
        },
        {
            "id": "3ece1115-5651-4ed6-b218-2d975709ca45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7841
        },
        {
            "id": "78ed6e55-02f4-4c88-9b79-fd9f10b8668c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7842
        },
        {
            "id": "20ba2d8d-85a3-4c2f-bbb0-d48fc994c18b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7843
        },
        {
            "id": "0cbb3f32-bcf9-4aa1-85e4-ea41b5ddb910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7844
        },
        {
            "id": "cea8536c-0e25-4142-a5ec-a6a1057c93b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7845
        },
        {
            "id": "d809b296-0b9f-4dfd-929d-07c2b064d402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7846
        },
        {
            "id": "0c61d7d2-a202-4893-93a8-6265e7488a41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7848
        },
        {
            "id": "fccf27a3-80a3-4b81-880f-f5b14664461a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7849
        },
        {
            "id": "0449fc51-0c8f-4c76-8ea5-67a8adfea0f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7850
        },
        {
            "id": "cc40acf7-410b-49bb-b4e8-73ec23ec8b8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7851
        },
        {
            "id": "99f0aa5e-94f8-46af-a254-32334cc27a0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7852
        },
        {
            "id": "164329d2-7e35-42b1-94f1-207f7eb0c6ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7853
        },
        {
            "id": "3347d384-5014-416d-a185-1ca476710622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7854
        },
        {
            "id": "188ca55b-2476-458d-a547-78c054d0d8e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7855
        },
        {
            "id": "7f760b11-6457-499e-80a8-03924390021e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7856
        },
        {
            "id": "072b4dc8-e46f-4fe4-b901-b26fb08fced7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7857
        },
        {
            "id": "b1298626-70c5-4cbf-ab21-97013fef9d85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7858
        },
        {
            "id": "e732e9c1-a092-46a3-9951-314a365d731b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7859
        },
        {
            "id": "60635459-6d2d-4f31-948d-b6e3708bc448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7860
        },
        {
            "id": "81d4dc7b-e03a-4de5-b414-9fc86513fd3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7861
        },
        {
            "id": "b675cc72-d4d2-4332-8194-20fc76dc5bf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7862
        },
        {
            "id": "324b205a-9d25-4d17-b535-a4bbf3cc8173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7863
        },
        {
            "id": "cbc03750-e3bd-4e0b-85f5-178e2795d402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7865
        },
        {
            "id": "51326558-c403-4f4c-8ac4-74c2ffd948b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7867
        },
        {
            "id": "21b22dd8-9f17-4c1f-ab71-4721f5f91b5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7869
        },
        {
            "id": "eb62023b-c517-4e7e-966c-96c20737b53a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7871
        },
        {
            "id": "4f6995f9-4b67-4ae5-8f92-6abf13884859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7875
        },
        {
            "id": "971dbc8a-9829-4456-969c-64bec8099312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7877
        },
        {
            "id": "b5a5c99b-8d0e-4768-b2ba-3054c6d4c0d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7879
        },
        {
            "id": "a3477b66-e90c-490e-8dc2-4290eab2aead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7885
        },
        {
            "id": "26ad0de5-3f9b-4549-bd9e-58cceef7f796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7887
        },
        {
            "id": "9fc0bf84-2662-4881-b510-0f77084d10cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7889
        },
        {
            "id": "f29e1042-4e48-471e-9282-69fc4f67d186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7893
        },
        {
            "id": "4581aa5b-a4cd-4c45-805b-659068641106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7895
        },
        {
            "id": "d55eaf74-cd02-413a-a390-0f4854689b25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7897
        },
        {
            "id": "ef29e97e-b7b5-44eb-8e1d-fb96918ec089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7899
        },
        {
            "id": "a86cfe6d-1048-4e33-adc4-1460c83d78b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7901
        },
        {
            "id": "5c4854ad-445d-47ba-b8b6-22576d010b64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7903
        },
        {
            "id": "67f1c81b-2d7f-4200-824d-6d320622a4a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7905
        },
        {
            "id": "8731b699-16a6-4c94-9307-3b1a32986966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7907
        },
        {
            "id": "70745dca-f194-4840-b904-a31289258d4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8218
        },
        {
            "id": "839dfd68-c069-41aa-9e38-edbdb7fdcd1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8222
        },
        {
            "id": "9d631b27-27a1-4a04-9e63-24c33e5c9f73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "8c1765a8-6ceb-44b9-ac25-2dc626dda26d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "972541bc-bcfc-4af8-936b-18f9b4669133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 63
        },
        {
            "id": "c15ec746-0a3e-40e5-8683-130205c2b1bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "1081c465-2d4c-453f-a71e-f5a1611ddfc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "d19ca8fc-fb14-46aa-9b01-9a4df69be108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 99
        },
        {
            "id": "1539159e-7303-4c97-a3d0-4c0a47eae240",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "294a89ab-b67d-4f39-aaf4-49cc372fa29e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "6cdec058-fbef-436a-bfef-25fb90f31159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "6c071149-ccf3-44cd-9e70-9adc639c46a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "eb6087e5-90ec-4f85-9afe-05d682a51964",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 192
        },
        {
            "id": "5ad32093-98ff-4414-8bad-eb21d789f39d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 193
        },
        {
            "id": "24894b83-7638-434d-b8f8-d675ddfc2dd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 194
        },
        {
            "id": "afcf69d7-89d3-4221-9c3d-c3e9992b762a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 195
        },
        {
            "id": "3ce6893c-7176-4115-8743-9f96f611c2b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 196
        },
        {
            "id": "19f6ed75-a56d-4410-b920-01b2a563198f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 197
        },
        {
            "id": "33db38ed-74c0-43fd-bc3b-6f4dac157180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "c125503b-1ae3-4dc4-9c87-f2fe4f12bb16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "5de41712-5836-40a1-b7f7-d39c49b8bbf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 226
        },
        {
            "id": "2a2e4877-0843-44a6-8b14-75fc58eca5f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 227
        },
        {
            "id": "35070b0d-aa47-4900-86d5-a2923fa248e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 228
        },
        {
            "id": "c672dd67-630d-4afb-b93b-f3a438e2ea94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 229
        },
        {
            "id": "a9d7fdaa-71ba-45a2-9acd-268148eae28e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 230
        },
        {
            "id": "6fc88c13-c05e-4d44-a75b-afab57875cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 231
        },
        {
            "id": "f3aed22e-a701-463b-93e1-b6de331444a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "a6219be1-385c-4596-a25d-cd429bb77de0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "34008600-13fb-4614-a257-01f96f2e1ab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "2c9f03a0-d1f1-4981-8ed4-0cb3bbfaf947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "a1c217e4-b2d4-4c72-a991-861caeadcdd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "c7cb97f7-b850-438f-a664-9d619664723d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "d04ec18c-e4a1-45b0-8a2a-63a3655f592d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 244
        },
        {
            "id": "1d633410-6492-4104-8c33-b8919846d347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 245
        },
        {
            "id": "37305202-20a1-4330-b79f-d8cc0edc2439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "d54409df-9ddf-474e-9b7a-dbb12a5eb3b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 248
        },
        {
            "id": "53ea8bd3-7701-4ac2-a744-be8f980dfee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 256
        },
        {
            "id": "a54abd5c-e42c-4ae0-b4d8-8182f532c75f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 257
        },
        {
            "id": "8990e0df-8176-40dc-89cc-964797a3ca71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 258
        },
        {
            "id": "fd6caf9c-6825-490b-b45e-d93d43bbfd59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 259
        },
        {
            "id": "49724f7d-113b-4245-b762-618a92234677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 260
        },
        {
            "id": "670ea8cc-650f-4f0c-a203-4b6dad6c5d37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 261
        },
        {
            "id": "247908d9-4cf7-459f-b3a7-0325d357c3a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 263
        },
        {
            "id": "0ca07e2a-aacf-43fa-9f75-2d94c1f2653c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 265
        },
        {
            "id": "8f70aaf7-22a5-466a-b45f-27e0b96a8513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 267
        },
        {
            "id": "420495c0-914c-4211-8707-e772cdc9fdf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 269
        },
        {
            "id": "3bd8cfab-ba9e-4be8-b218-521fef899f9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 271
        },
        {
            "id": "e7b23d83-1e1d-46f0-8afd-0c97e394b071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 273
        },
        {
            "id": "0ae82bf0-ec93-4783-911a-1fe9021a249e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 275
        },
        {
            "id": "bf3957b2-e5fe-4751-9da8-7b0707c47046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 277
        },
        {
            "id": "039c74ba-4c92-4700-b129-4480adfcdb95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 279
        },
        {
            "id": "2e8eb7a2-2f33-4a63-868c-e25c240db9b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 281
        },
        {
            "id": "0c34e126-17ae-4057-ad5f-ab06bc091886",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 283
        },
        {
            "id": "825ee156-3011-4f3d-89e7-e222460d3e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 333
        },
        {
            "id": "06ac3816-1662-428e-a2f4-2d59b9f57f34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 335
        },
        {
            "id": "4d1ad190-f36b-4a76-aaf0-d847b33e795d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 337
        },
        {
            "id": "ac71d8ae-912a-424d-aa91-060ff7396a08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "59ba82a1-8850-40ad-b8a1-7fac7ef39330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 417
        },
        {
            "id": "a935a85b-5a53-428b-a9d0-f75a61500a35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 506
        },
        {
            "id": "b8be081a-a116-47b5-b76d-06559cc87411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 507
        },
        {
            "id": "783dcde0-8245-422a-b680-0f307bfd9712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 509
        },
        {
            "id": "4230f090-4541-49a6-a26a-216caa2e1726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 511
        },
        {
            "id": "75e6cada-3946-4c11-9b41-727ed372bedb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7680
        },
        {
            "id": "edf5cf1c-4639-4a6f-afff-9279677e7524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7681
        },
        {
            "id": "fe09d68a-a09c-4d0e-8125-c115dfefe258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7840
        },
        {
            "id": "110c5afc-c81b-4d56-810e-fbbf0041834c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7841
        },
        {
            "id": "aa67ae97-f8d0-4ec6-b669-41fb339e8931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7842
        },
        {
            "id": "99e76b85-442f-4c00-83fa-9835199ccde6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7843
        },
        {
            "id": "451e8278-0763-4eb7-9a10-ba653b15f313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7844
        },
        {
            "id": "fa31e014-3731-452d-b1cd-8a581933510c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7845
        },
        {
            "id": "4ea3f280-9d8c-4250-b939-80294fab2462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7846
        },
        {
            "id": "4cd5a712-9cd0-4556-92c9-86a05920c486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7848
        },
        {
            "id": "2114d8db-eee5-4f3a-89e0-13c9e88638ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7849
        },
        {
            "id": "d6a06192-abd3-4a5c-9bea-62b1700f6675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7850
        },
        {
            "id": "da5a4390-d2a8-426b-8a57-c6410f153c5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7851
        },
        {
            "id": "95d3066e-a06c-4857-94dc-9f87352b9484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7852
        },
        {
            "id": "f224a36a-b92b-47fc-98a3-0961c411bbeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7853
        },
        {
            "id": "55fb41e9-ae8e-4fd6-80e6-3f048857604b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7854
        },
        {
            "id": "d0e63b71-cc1b-4696-a98a-7aba82f4227c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7855
        },
        {
            "id": "776c4ce4-f9bb-47ee-a108-e78c3b98f7ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7856
        },
        {
            "id": "31c13f7c-dd2a-463c-9ca0-0065c58e90ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7857
        },
        {
            "id": "6042d174-7f62-410c-a8a0-5d641fccebd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7858
        },
        {
            "id": "84c6845f-b8fd-4061-9a5b-1ddb0e419020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7859
        },
        {
            "id": "6f3ac256-b7e1-4cc9-bcd0-eaabe7a89ed7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7860
        },
        {
            "id": "f60efdc9-33f1-477b-aba1-cd6404c1891a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7861
        },
        {
            "id": "3ac4f232-9605-4087-889c-245e54d69087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 7862
        },
        {
            "id": "d76bf2be-ec0e-4f73-817d-4f443a503db2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7863
        },
        {
            "id": "2627d757-2c87-4ab6-b38b-af3ab63e8ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7865
        },
        {
            "id": "83bd51d8-acff-4c59-a3bd-619ea2e3ec29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7867
        },
        {
            "id": "0f1a9d64-3427-453f-8bed-e211e8c0124a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7869
        },
        {
            "id": "de0efe5a-0ced-4da7-8f14-9f14c0798e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7871
        },
        {
            "id": "0e3b78ab-8c3d-411e-a366-0d425158035d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7875
        },
        {
            "id": "05eb08ed-1156-4f38-8479-4953dde67f7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7877
        },
        {
            "id": "a0e6d142-265c-418b-a3f1-4571f21c8529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7879
        },
        {
            "id": "39f8263d-fc3e-4a8e-80a4-4661919c0bd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7885
        },
        {
            "id": "ba173a81-dbd8-48b4-b2d2-01e37a7547d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7887
        },
        {
            "id": "507396cc-fefb-4176-a6c3-f4ecfa61dda7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7889
        },
        {
            "id": "a0efec92-9398-47eb-9b7d-1ad7da80b1b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7893
        },
        {
            "id": "d640ddac-49c0-45b3-9fb1-3e5a58879685",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7895
        },
        {
            "id": "0288e025-b690-4c7e-8726-aaed9e34849b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7897
        },
        {
            "id": "cf329826-a09b-441f-8bcc-0edd4e0eba23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7899
        },
        {
            "id": "11a271bb-0637-46ca-955d-b772e22c86c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7901
        },
        {
            "id": "cdba2d45-09c1-4191-bff7-bf8abf294f8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7903
        },
        {
            "id": "e2a13542-5003-4524-81e6-a46f425fa35f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7905
        },
        {
            "id": "3862f4f8-b8f2-4822-be9a-9248ec49232c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7907
        },
        {
            "id": "52ae06a4-51ec-42e1-820a-bd379ea9e96a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8218
        },
        {
            "id": "a1cfd96c-9636-4436-8c9f-a014a4aa7f11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8222
        },
        {
            "id": "580f46c5-bf4f-4ffb-a484-17439301f2bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 67
        },
        {
            "id": "0553ab50-6718-48e4-bc0b-8a9c43c550d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 71
        },
        {
            "id": "8ee47e50-2b03-4a94-810e-d13bddfad78e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 79
        },
        {
            "id": "87527641-faec-4daf-8442-f26cc10dcfe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 81
        },
        {
            "id": "2e0f6080-b3e0-446e-a3b4-e80839e8da21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 199
        },
        {
            "id": "1c233ace-48c9-4ec3-b5fa-f05b977b4f35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 210
        },
        {
            "id": "0a84e3bb-5a7b-402d-ad5d-239010c69eb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 211
        },
        {
            "id": "fab3a237-6f0e-49b6-b127-7874e55e9bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 212
        },
        {
            "id": "ce1c5f3a-40f6-40fa-bd4c-a32cdc0f1fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 213
        },
        {
            "id": "4dcfdfcc-a3d9-4d48-8fc1-6ce3c1afd176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 214
        },
        {
            "id": "ad16f689-5474-485a-9a39-3721f0598028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 216
        },
        {
            "id": "7e5d14b1-e5ee-4e14-9a78-8528f9dbd16a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 262
        },
        {
            "id": "a9dbb64e-5fc5-42e1-840b-b07104bcc16b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 264
        },
        {
            "id": "535cb4e4-3cff-45a5-922e-79a8bb40a4d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 266
        },
        {
            "id": "8f0ab7c9-5cb8-48f1-a644-1bfa46121cfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 268
        },
        {
            "id": "b7cc4b34-327c-4c1c-badf-21da00311a8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 284
        },
        {
            "id": "4a036bf3-3790-400c-a375-f6cbc297e30d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 286
        },
        {
            "id": "fc0d4486-7f51-4f5d-8819-f8ad7d4b40bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 288
        },
        {
            "id": "e2146db5-97f8-4d22-ab5f-c1b2e7c13733",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 290
        },
        {
            "id": "6301f2b0-125d-4f11-bca8-0325bf305bf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 332
        },
        {
            "id": "a24e2a2a-1265-4f97-83fe-3855aa4516d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 334
        },
        {
            "id": "f9311168-bd8b-4baf-805a-e116115d3ce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 336
        },
        {
            "id": "c2fcf706-896d-40cc-a242-4502d27075fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 338
        },
        {
            "id": "06276b29-e42c-487f-a7eb-c5a2df956c0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 416
        },
        {
            "id": "d56358f2-b519-426a-aba6-2170cc181622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 510
        },
        {
            "id": "ccf7934e-d021-4037-9d4e-3737ef20eae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7884
        },
        {
            "id": "0a0e3cd2-7aaa-4356-bb18-f498bf167be3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7886
        },
        {
            "id": "0e32c734-19ab-4ee9-8c37-957baedffbfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7888
        },
        {
            "id": "4c1b4ad5-c667-44f3-9ca3-68cf62b5b00b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7890
        },
        {
            "id": "5662dc18-3235-41bd-93c1-3e827436b105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7892
        },
        {
            "id": "ba0a12f8-8731-4406-9fa9-78a193db3e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7894
        },
        {
            "id": "f1425898-3fcd-472b-b243-d243e19f3b9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7896
        },
        {
            "id": "513f003d-d8f4-4f95-8fb4-2c881cb5a4ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7898
        },
        {
            "id": "31b18156-02f7-4c2b-bd12-c8003500265c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7900
        },
        {
            "id": "3b153cf1-522a-46f4-aa5c-db91d4ac19c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7902
        },
        {
            "id": "75c7ee89-35cb-4cec-aa3a-a7e679267541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7904
        },
        {
            "id": "3927fc2f-fecb-496f-9ac9-c3c4f43b300c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 7906
        },
        {
            "id": "512fe4dd-f35c-4670-b6fc-46da73a661ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "8a52804c-8f2d-462d-9856-082588114b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "0e3f1a40-dc5e-4750-9d9c-c7fede42644b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 63
        },
        {
            "id": "71ef1f77-c154-4811-b6d0-1d2aa99fc37c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 65
        },
        {
            "id": "73063075-90c8-41ef-87a6-8d05c55a20f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "6bd9c6cb-cbe9-469b-aa78-8f56bbabaa9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "c015ad90-f968-40a2-91f8-4d80125852da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "9a4510e0-d8ca-4bc2-8a9a-303b71e5cdd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 81
        },
        {
            "id": "1c1b70d4-c19b-4c11-a074-ebd1e10071c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "8ad2bbfb-15a9-4172-b66b-adfb1dc92332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 99
        },
        {
            "id": "23392cb7-9bb0-4b71-b98f-3409213f15b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 100
        },
        {
            "id": "88d77fed-96dc-4547-83d4-df78f988b2f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "845f5821-2487-4ce2-b58c-f05fc70e83a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "5e8a3c6e-2335-4e95-ace5-d39bf97a8b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 109
        },
        {
            "id": "d7dda3d7-9bad-4e2b-acf0-f1bec4306d81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 110
        },
        {
            "id": "a8931a5f-6671-442e-b72a-aff52f837e84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "9e597884-4367-44da-be39-c4cb99c9561f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "05d4443a-1eb3-4267-9737-30960f65a6aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "f7fa396f-6c09-4565-839c-4907ce524498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 114
        },
        {
            "id": "d8c1a527-0244-4215-bc83-e0ab256010a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 115
        },
        {
            "id": "22423166-17a2-4bcb-bd39-fc8e7d0de997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "5576b844-d7b0-41f8-a746-a48be4ff10df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 122
        },
        {
            "id": "2d096e74-fc92-4fa6-9f1c-52a3800f7f09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 192
        },
        {
            "id": "bb36b49f-a8fc-4b08-bce3-2f3b95cb06ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 193
        },
        {
            "id": "5a55b348-6220-497c-bd91-a3b7c8183bd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 194
        },
        {
            "id": "fbc8f6d5-365f-4cd6-8f6d-dbfbe2a131d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 195
        },
        {
            "id": "c7740fb8-55a8-4153-b410-48287f314b58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 196
        },
        {
            "id": "bf84a0b4-36e5-42ee-96d7-4fd1bba273c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 197
        },
        {
            "id": "872a3a73-4450-4305-8b9a-0ee82bbae489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 199
        },
        {
            "id": "f1eba81f-4d08-4e33-a916-de13d6b30df4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 210
        },
        {
            "id": "42181de1-1159-4413-b300-3b5c30f34b0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 211
        },
        {
            "id": "96afb514-0025-4096-8f6b-18375bdfdc40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 212
        },
        {
            "id": "7bd3776e-7135-4609-9fd2-f40a79a8d983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 213
        },
        {
            "id": "d39e6fe7-f750-4e5e-8bfc-dd4ad591f61c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 214
        },
        {
            "id": "13a57b2f-3ae8-4c54-8f5c-55a37f46be07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 216
        },
        {
            "id": "a3f879a8-914f-4c75-a4ff-df13a96f6780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 224
        },
        {
            "id": "c049e68a-be8a-49e2-8f09-4a5448ff235c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 225
        },
        {
            "id": "48502b38-8f5b-4e8e-827b-6bd2236d82d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 226
        },
        {
            "id": "526dbab7-aa70-4f06-8748-9bf1e2fdf702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 227
        },
        {
            "id": "5e03a3e0-0bea-4f33-bb9f-ff04fe2d3d3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 228
        },
        {
            "id": "e4a1cddb-69cc-4bc6-8958-110293ce8f30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 229
        },
        {
            "id": "ffac906b-4036-496f-a917-65e7fee1b200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 230
        },
        {
            "id": "0ef85249-20e3-47c8-8224-5dc59921f387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 231
        },
        {
            "id": "0ff6cdc4-64a1-46d8-a5b5-e871ad6f7c1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 232
        },
        {
            "id": "d0d07bb6-3046-4d05-b26b-f9a93e19a933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 233
        },
        {
            "id": "acb9d603-c564-4341-bcf5-a2af6d57c2eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 234
        },
        {
            "id": "44edd051-f644-4b67-bb9d-f06f9c380b28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 235
        },
        {
            "id": "1afc0c6b-3b7b-4566-ac8a-12defd4124ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 242
        },
        {
            "id": "d75c95ad-148e-4e6c-a1f5-91d97b686169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 243
        },
        {
            "id": "88b3c2e0-bf03-4433-bbfd-9b7f2da7ebc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 244
        },
        {
            "id": "19a39638-3472-4596-8b3a-2e75118101ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 245
        },
        {
            "id": "404be4e1-c371-4257-b8dc-b98ab2ca7d39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 246
        },
        {
            "id": "bbbe4a43-4cc0-49c5-a74e-076ee7e083ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 248
        },
        {
            "id": "2a2dd191-effa-44fd-95b3-71ae431bb1c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 249
        },
        {
            "id": "3348ab38-8eea-4dfb-833a-f298a2759862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 250
        },
        {
            "id": "80e16692-1ddb-4716-b4c8-539f1aa29069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 251
        },
        {
            "id": "71b55ea3-d652-4239-a5dd-038eee6e622e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 252
        },
        {
            "id": "93dea33a-2af3-46ec-a004-468d61c04364",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 256
        },
        {
            "id": "a30df01c-de01-4dc2-8440-d1709534044c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 257
        },
        {
            "id": "2218371c-64c7-497e-b6af-f0edc38c7786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 258
        },
        {
            "id": "175ddf78-678c-4da6-9461-31ac8f21a649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 259
        },
        {
            "id": "f3179b67-8ea3-46f6-a25f-dfb81f0c466f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 260
        },
        {
            "id": "c3d53058-57ac-4b3b-95ff-e675b2d02a8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 261
        },
        {
            "id": "fe4c954d-ed48-46f2-a5d7-e5dd68c468e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 262
        },
        {
            "id": "07815d83-ff38-4ef1-813b-b0453ecdd349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 263
        },
        {
            "id": "d3cfd6fb-c80f-4624-8435-941993c29054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 264
        },
        {
            "id": "3efc6e5a-7d31-48ba-a283-9dbae403fe01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 265
        },
        {
            "id": "bd7ddb82-181a-427f-afde-263960d77dd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 266
        },
        {
            "id": "e5a76236-18cc-4454-a70a-894ef0b3f9c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 267
        },
        {
            "id": "20497e57-0d0c-4f51-831a-4f8339a18751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 268
        },
        {
            "id": "13a91aa3-44f8-484a-90c5-62ca905ddf0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 269
        },
        {
            "id": "8cc6fcd2-3429-4a40-8a35-d3bf0bbbfa49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 271
        },
        {
            "id": "ca7aa3b9-05d5-46b2-8434-de403f192f51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 273
        },
        {
            "id": "d1871dc7-55ff-4d38-a8f7-b2606c7e9050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 275
        },
        {
            "id": "4d56ccae-e34c-4254-ab13-c58415e98de2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 277
        },
        {
            "id": "7299cef3-edb4-4bff-b6d6-c033c2a0c5d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 279
        },
        {
            "id": "fcd343dd-0dd4-4e59-8c45-07cc7be54a0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 281
        },
        {
            "id": "80270c0c-d0e0-4bb5-8edf-db676170bdc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 283
        },
        {
            "id": "f3727c71-4fc2-46ed-a503-49e4b190482a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 284
        },
        {
            "id": "67fbf36d-a908-4aae-9dd1-682f0e12075b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 285
        },
        {
            "id": "d59a82b0-d689-4028-ad6b-9943ed624049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 286
        },
        {
            "id": "cbfa1dd9-64b7-4f4d-9537-ddacdb1fadad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 287
        },
        {
            "id": "6a229c9d-adc7-4955-b588-78f0e7a7b128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 288
        },
        {
            "id": "aa8918e3-f82d-4dd8-b1c1-a805030e8a96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 289
        },
        {
            "id": "49025e77-4465-4da6-b4a0-4604cdf73d0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 290
        },
        {
            "id": "06ff5e26-0330-40f1-8a80-9279e5305890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 291
        },
        {
            "id": "7dbdcdf7-1fb3-454c-9c1f-d911a73d4ebe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 312
        },
        {
            "id": "04521dab-5f3b-4b42-8959-c564e25fe239",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 324
        },
        {
            "id": "38d6bfe9-7d84-459b-ba90-ba19f0bb6afb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 326
        },
        {
            "id": "ea5260de-4564-4ca9-89c2-70fb47cd2d30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 331
        },
        {
            "id": "0dcaf523-6870-42b7-8ca4-4670af203cbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 332
        },
        {
            "id": "6a358019-ad76-4c38-92d8-b5d7b998d6d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 333
        },
        {
            "id": "cbdad6aa-1807-430f-b2e8-f7d04d1d1ff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 334
        },
        {
            "id": "6e6f876c-aa3d-4792-9d5f-1d1b464487a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 335
        },
        {
            "id": "f52feb9c-f3fd-43d1-9794-0f7f4ae5d56d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 336
        },
        {
            "id": "22b43dc6-4182-48aa-80fa-04440735cf56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 337
        },
        {
            "id": "a5727ac3-4f2b-41ef-91e9-75e7f9207d31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 338
        },
        {
            "id": "30fcb6e7-deb2-473a-b6c8-ce90c7f0ac3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 339
        },
        {
            "id": "0eef9253-c5ec-4f06-857d-a9a54f7f0f8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 341
        },
        {
            "id": "2de9abd1-03f4-4458-a969-31eb76c8f3a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 343
        },
        {
            "id": "e2c3969f-4822-4bd7-9d87-8bd133609d20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 347
        },
        {
            "id": "e948620c-e687-40b5-bfe1-8105e1214d1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 351
        },
        {
            "id": "a09fb5f7-e27f-4156-ad02-8cbc87a666ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 361
        },
        {
            "id": "870a2e75-723f-40ed-995b-076884358e05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 363
        },
        {
            "id": "089f05d4-80e3-47be-b461-156cef6f6b3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 365
        },
        {
            "id": "d1a8dc3d-5735-4505-8d48-129a2d4aca6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 367
        },
        {
            "id": "354f7919-23cd-4b3d-9a93-044536d0b386",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 369
        },
        {
            "id": "6d832a91-ef53-4fef-acf4-425f1bf592c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 371
        },
        {
            "id": "f34bbea4-5110-49e2-8e3e-258dd42b9a45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 378
        },
        {
            "id": "2f59c1b6-d794-4728-87ac-b8e931fead3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 380
        },
        {
            "id": "63ed2667-a1f4-42aa-ad7a-b9dfc47fa5cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 382
        },
        {
            "id": "3ba0fbe3-e160-4b99-bafb-da55bb7ac43d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 416
        },
        {
            "id": "6c9dfced-0dd1-4c34-9bb7-51416527d258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 417
        },
        {
            "id": "e7c8f155-747c-4a80-8fde-93d01710f7d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 432
        },
        {
            "id": "02865120-352d-4e7b-8919-2a289e7eafbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 506
        },
        {
            "id": "eb3426e6-4c98-44dc-943e-91ab86007d6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 507
        },
        {
            "id": "814570c2-94cb-452f-b90e-fe25cfddb343",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 509
        },
        {
            "id": "67597107-0635-49bd-b074-5317e9906cbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 510
        },
        {
            "id": "f2381045-1e36-41ff-9b2f-edb19302a62f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 511
        },
        {
            "id": "61f8ef27-d4b0-43be-ac99-59ec137c2769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 537
        },
        {
            "id": "e496d746-936c-4503-953a-47875796b1f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7680
        },
        {
            "id": "7d89f037-2fc1-4641-a99c-7b55a597620a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7681
        },
        {
            "id": "dc24812f-cf08-447a-80be-950a7ca5759e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7743
        },
        {
            "id": "d28ba572-15cd-4d80-98ae-cf0961bbe07d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7840
        },
        {
            "id": "773cd781-f219-44fd-b90c-8125c5f12802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7841
        },
        {
            "id": "84bdc5f5-84db-4cb0-a7d6-1b12a835a43d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7842
        },
        {
            "id": "107cd899-4fbb-438a-955a-aa54d2900370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7843
        },
        {
            "id": "f3929642-6932-4601-b6cb-ff4663db58cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7844
        },
        {
            "id": "06e2a870-1dcb-4f89-b99d-7d7bc29f6c3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7845
        },
        {
            "id": "0082ffca-37df-4330-9de7-8772f860c8b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7846
        },
        {
            "id": "98751038-402e-403b-8d30-f8512336f707",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7848
        },
        {
            "id": "4c1f3716-148a-4878-8171-4ea2a372fc69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7849
        },
        {
            "id": "60444b30-a414-4e88-a784-1dcf5f9c796f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7850
        },
        {
            "id": "36ccf1d8-87ad-44e7-87cd-386350bdb3c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7851
        },
        {
            "id": "4ba9abba-85a1-4fa0-927e-4880bb825fa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7852
        },
        {
            "id": "61b76195-f58f-41ca-8a41-2de780d2380e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7853
        },
        {
            "id": "6a49efee-a2d4-4d94-96a2-0c021bd588cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7854
        },
        {
            "id": "d6e11e13-511d-40cc-a945-7f64a1a8fe65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7855
        },
        {
            "id": "682b07cb-3274-4c81-aafc-3f9eb50cd3fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7856
        },
        {
            "id": "d117f6a6-6047-4ba9-8871-80d9e0a44e9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7857
        },
        {
            "id": "8e2ecb23-f496-425a-9b0c-f21ae3e8161f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7858
        },
        {
            "id": "e51a83ca-7a45-4991-bd54-5a5ec45f038c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7859
        },
        {
            "id": "cf19d63c-8219-495e-98ea-caa134cd4b63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7860
        },
        {
            "id": "201ee74b-d8eb-4abf-8a99-fba436256e1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7861
        },
        {
            "id": "355376a0-5cff-41eb-8e0a-5f9df2fa1c63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 7862
        },
        {
            "id": "089d14c1-e1a4-40b2-b6aa-ed7b4f54fc83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7863
        },
        {
            "id": "c666c9ce-1207-48ab-84af-6f082f65f645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7865
        },
        {
            "id": "f43f8f56-d14d-4876-a516-fd2f17b11e96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7867
        },
        {
            "id": "e77b60ad-ec4d-48db-ace8-99004f5b375f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7869
        },
        {
            "id": "9d1aa703-46a2-4819-88ba-84dc65a472b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7871
        },
        {
            "id": "f691ba1c-73b7-4e0c-a826-cebb9d7b3614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7875
        },
        {
            "id": "fdf11203-a70e-4608-9af3-2dcb283f6899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7877
        },
        {
            "id": "2a88a4a6-0e14-4020-b5b4-d1437247ec36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7879
        },
        {
            "id": "6b3aabe4-9132-42ef-9cd0-fd4935eca1d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7884
        },
        {
            "id": "c395fad6-c5d4-46de-8c4d-6f367de80fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7885
        },
        {
            "id": "036f36c0-e35c-4443-a410-8acb513868b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7886
        },
        {
            "id": "effa34d5-3601-4ad5-99f0-ec025424d2e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7887
        },
        {
            "id": "b53c59b2-a385-4cbf-8816-9d0d47ee9e3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7888
        },
        {
            "id": "947041d0-3249-458e-8a9d-1407d9bf49b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7889
        },
        {
            "id": "c0668d89-8321-4d8f-a0a2-f8b466273af3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7890
        },
        {
            "id": "bfff5472-a782-4f05-af87-0a1d307f8e7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7892
        },
        {
            "id": "0435ca36-89ee-4f43-9aad-adb4ecc4bbe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7893
        },
        {
            "id": "5e1321c2-4282-4635-9ef3-5709b6cc9b39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7894
        },
        {
            "id": "cac4d645-3a79-4ef2-90b5-01e8bcf252ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7895
        },
        {
            "id": "16f9b247-f20e-4775-be4d-fbf3f971822b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7896
        },
        {
            "id": "9379111e-a52a-46f4-809d-05955a7d4fb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7897
        },
        {
            "id": "27c088fe-63e6-493b-9def-b05b5c4150b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7898
        },
        {
            "id": "ddbfaa5e-41cb-4fad-a834-16afe2ee4d3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7899
        },
        {
            "id": "3f8d1280-6ad3-4af0-8dcb-781336c1b9f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7900
        },
        {
            "id": "9527929a-8ee5-4066-b445-0085d0aae5d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7901
        },
        {
            "id": "183437ba-24a6-4efd-b84b-918f9a70ee2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7902
        },
        {
            "id": "6b499f2d-2380-40fa-b38b-6c22c1e989dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7903
        },
        {
            "id": "bc551327-357f-49e9-bdcc-d257b0dd784c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7904
        },
        {
            "id": "abf5812c-09f2-4344-a7f5-e70fea0d3390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7905
        },
        {
            "id": "7d14364e-dd59-401e-87ce-5f1372f9fd2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7906
        },
        {
            "id": "f11698dd-a954-4597-9cd1-aa79afaf6d84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7907
        },
        {
            "id": "263e4b7a-4c47-4144-987c-582cf7c88e8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7909
        },
        {
            "id": "f5da94fc-d3c6-484f-add8-1ef55cf35b46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7911
        },
        {
            "id": "d16debfb-a416-49f2-8192-cc5ecf020705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7913
        },
        {
            "id": "6e2bd8e3-1455-4be8-8211-99eee3faa647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7915
        },
        {
            "id": "d352b17d-e113-4105-9b6f-d2dfce17a4d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7917
        },
        {
            "id": "f3d26f82-86fb-4964-aa61-e16ca548953b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7919
        },
        {
            "id": "deaa1fe5-f610-44b7-b896-e7bc45fccd85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7921
        },
        {
            "id": "9570de39-640c-42be-83b9-1ba79b52f77b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8218
        },
        {
            "id": "d74cc85a-a9d8-4e81-81b2-14d7d4844176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8222
        },
        {
            "id": "f0d4bdd7-4050-4743-9d10-b901ef177c6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 91,
            "second": 74
        },
        {
            "id": "6c21a74d-16c5-4d46-9fb0-6e7f60921b6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 118
        },
        {
            "id": "84329d39-80d1-4e32-8064-4386050cdf88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 119
        },
        {
            "id": "a551e40d-4150-44f7-86ac-36cf1e005b85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "f23a4f9b-bf1a-46c6-b85b-11ba0648a173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 121
        },
        {
            "id": "603e6f71-004a-4bd8-bd59-96af1bd418df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 253
        },
        {
            "id": "6b56af1c-4eca-44d7-b838-c7ba634590fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 373
        },
        {
            "id": "b08273f4-d468-4f2d-9c83-9dd1e333e24d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7809
        },
        {
            "id": "40710f97-1362-4ab4-ac55-698f1d973091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7811
        },
        {
            "id": "6280f20a-14ac-4405-bc65-8c37f5b39217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 7925
        },
        {
            "id": "16490b0d-14dc-40e8-8306-6db813122f2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 34
        },
        {
            "id": "ba73d43c-dceb-4184-86ac-6b5bd045f659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 39
        },
        {
            "id": "0f784d4e-c912-4411-933d-69ffbcb0d5eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 8217
        },
        {
            "id": "fc2d3664-e2f6-4d30-9e09-c45d20564a80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 8221
        },
        {
            "id": "768294c3-ccdc-43f2-9e60-4e96c5e2dbda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 118
        },
        {
            "id": "464a299f-db0d-4e6d-8476-bb4d59d40be6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 119
        },
        {
            "id": "2d271622-5560-4c92-88e1-4bc713b9e547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 120
        },
        {
            "id": "32e5cacf-f69b-4b76-9ba1-aba7e3ff6b02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 121
        },
        {
            "id": "effc15e0-9712-4873-ac08-01807df0a166",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 253
        },
        {
            "id": "431275eb-6b7f-4625-b044-fcbe817b1b12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 373
        },
        {
            "id": "29038fc1-18d5-459e-b05f-c5059e93f79b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7809
        },
        {
            "id": "15769732-49fc-4531-827f-38389a713107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7811
        },
        {
            "id": "dd23d3f2-207f-49ca-859f-c2761de19014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 7925
        },
        {
            "id": "309e04ec-1005-4601-bf6a-fcd3109a451e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 102,
            "second": 34
        },
        {
            "id": "3a8c54f4-b664-41fa-b98b-d3b4164e3a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 102,
            "second": 39
        },
        {
            "id": "f10f2151-5417-46e7-87c9-4e02f0f650da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 102,
            "second": 8217
        },
        {
            "id": "c1e4ad4d-9163-49d9-84f6-10f1f08244f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 102,
            "second": 8221
        },
        {
            "id": "c2fea765-c22d-40ea-8d71-fc3b610aafe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 99
        },
        {
            "id": "45b42630-a1d9-41ae-bd0f-d9d9d6754db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 100
        },
        {
            "id": "f1cef1af-61a8-441c-a158-1a5946067115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 101
        },
        {
            "id": "4b868ca1-074e-4489-84fa-7f3e585dbecd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 111
        },
        {
            "id": "f7574ef4-117e-4cac-a038-3eca77f8da7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 113
        },
        {
            "id": "52bd5ec6-e94c-477a-b034-d1b6d4366d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 224
        },
        {
            "id": "1d8c075b-ea45-440a-a00b-0f6a283e5d21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 231
        },
        {
            "id": "b08895c0-0520-4cfe-974b-ed597584bb0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 232
        },
        {
            "id": "0e171862-0d7e-45e3-ad50-6b13c545662f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 233
        },
        {
            "id": "75b50ce2-47e9-4ed4-8cc5-e56018d7e4c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 234
        },
        {
            "id": "c2fbcf75-02b1-4bd7-95a3-025718f1ab96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 235
        },
        {
            "id": "9fba0a3a-d65a-4805-b642-0e218953066b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 242
        },
        {
            "id": "0a97103c-6e6e-44f3-aa01-2b9eb94a48df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 243
        },
        {
            "id": "2f96c60b-924c-4599-9400-9cc1d2286087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 244
        },
        {
            "id": "0af30303-4e98-4f1e-90da-ec4c90698fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 245
        },
        {
            "id": "223c3b5e-ada1-42ac-b95f-b8df93222a01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 246
        },
        {
            "id": "b207aa57-a340-473d-b912-f4bba438e7f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 248
        },
        {
            "id": "3883beab-3a25-4927-9103-f1b5337722a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 263
        },
        {
            "id": "e9db0183-afbe-4834-986f-a42fea75f40c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 265
        },
        {
            "id": "ff531833-2bf1-458c-8195-e587d7f22e83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 267
        },
        {
            "id": "03b77e8f-200b-4ef5-862b-eb2cad85a171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 269
        },
        {
            "id": "57d0f93e-62ce-4d05-9fde-5c85acbd2983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 271
        },
        {
            "id": "db3c6c80-869a-4033-890d-0d4cf2886dba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 273
        },
        {
            "id": "4831db6a-984e-452f-aa87-b61322247353",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 275
        },
        {
            "id": "f0b7e171-a125-40aa-a1bc-0d0af9eac586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 277
        },
        {
            "id": "e157b35b-1b3b-4643-8014-d5964e670846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 279
        },
        {
            "id": "a82f1211-139f-4542-9b5b-bd834b801224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 281
        },
        {
            "id": "d5eba3cf-95c8-40c6-98f1-208c3512a1c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 283
        },
        {
            "id": "0685bf33-f5d5-4d3d-b856-203b97965df4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 333
        },
        {
            "id": "6e1a9796-84ce-4cf9-a662-e40c1163c560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 335
        },
        {
            "id": "6eb2d94b-93e1-4312-b949-def59029e659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 337
        },
        {
            "id": "a31391b1-8495-49e5-bf42-5805e53ce9c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 339
        },
        {
            "id": "4140c42b-accb-4928-9bb8-c181e10faf67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 417
        },
        {
            "id": "96e1addd-dbe6-4deb-9551-f3fa569d7545",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 511
        },
        {
            "id": "2bab15e9-1c79-451a-84fb-fe0efb9291a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7865
        },
        {
            "id": "f77a701a-8f6f-4976-93ab-6d01f6b2d8e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7867
        },
        {
            "id": "063e49ab-529f-4866-9125-d195aefff365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7869
        },
        {
            "id": "60e038fe-9df2-45b9-8afd-b2fb3fa8e6aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7871
        },
        {
            "id": "8faf7fa2-1415-4b62-92d2-b2e41c19fd23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7875
        },
        {
            "id": "ea31fed9-d6aa-4993-b428-924ad106c927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7877
        },
        {
            "id": "b46f9f64-a8bc-4d6a-a89f-626ead7d418c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7879
        },
        {
            "id": "ca932a03-48a6-43d1-91e5-bf880c0ecd34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7885
        },
        {
            "id": "f1b73aea-3caf-46f9-9fd4-3029e6765efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7887
        },
        {
            "id": "3c936b20-0218-44b8-b694-e2d6e11630b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7889
        },
        {
            "id": "64b3d837-b73d-442e-a8bf-02de5fa1a31e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7893
        },
        {
            "id": "32c1298d-74c7-4d9b-90e9-bf769033d980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7895
        },
        {
            "id": "95660e4b-9f84-4619-bcec-5b5b2c41398a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7897
        },
        {
            "id": "7d6b5c43-d60c-4e6c-a7ae-fb9ed0c2757d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7899
        },
        {
            "id": "50bcdb36-6f26-4416-9c38-e41597c79633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7901
        },
        {
            "id": "f1148342-3fdb-477d-9766-d15565dbf5a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7903
        },
        {
            "id": "41ed448f-4453-4707-bd9e-40d37c313fd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7905
        },
        {
            "id": "e8db7aa4-e0f5-432d-96ca-a075aeb3ce34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 7907
        },
        {
            "id": "31e15c30-7e13-4376-85f0-744eebc636cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 118
        },
        {
            "id": "edc117ce-e644-4f1a-95f7-38ccb8874e23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 119
        },
        {
            "id": "ef228b89-f1d0-4705-905d-37886737987a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 120
        },
        {
            "id": "f1c500e6-f3cf-4cb2-b049-b49b5a063873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 121
        },
        {
            "id": "a65ce3dd-6cd5-4351-b9d1-34015b6afbbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 253
        },
        {
            "id": "fe62d72e-5e40-4530-b3c8-1590b645439f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 373
        },
        {
            "id": "34a112bf-dd02-43fb-8ba2-5a670941483a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7809
        },
        {
            "id": "b52e58fe-f77a-4a7d-af1b-ca00e44d8e30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7811
        },
        {
            "id": "c105e574-323e-49e7-8b71-8d6dd9d582e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 7925
        },
        {
            "id": "e8d9b0a0-4081-4b4e-90f4-bca7e6d88def",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 118
        },
        {
            "id": "196bff48-6292-4b6f-b84e-a47c7e01248d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "bb297b9a-1c03-4386-9c0a-ab72ba779f8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 120
        },
        {
            "id": "c7a408b0-e295-4b1e-995f-fb5f5a8dd2e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 121
        },
        {
            "id": "46429663-e72b-4a4d-9899-784621106b4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 253
        },
        {
            "id": "e5b4ddac-4de8-4791-b0c9-c5106a0be291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 373
        },
        {
            "id": "a0095053-b936-4e98-b608-e4d12beffbc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7809
        },
        {
            "id": "a6ec6f7a-b441-424c-a30b-2be28b13449a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7811
        },
        {
            "id": "1403bfa2-df8b-4a1a-8425-2107ddc2bb87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 7925
        },
        {
            "id": "94b2eec1-0ae9-46eb-9611-7b80bb0415e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 34
        },
        {
            "id": "78cc6c5f-6381-4003-88e6-23f99321c201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 39
        },
        {
            "id": "9566bbe7-a376-4827-98a5-88b3108655be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 97
        },
        {
            "id": "c9520ef8-0d75-4d90-aff6-de781ccfffdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 99
        },
        {
            "id": "1885f390-d314-4279-9b9a-37a93cb28108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 100
        },
        {
            "id": "d39734f7-8001-4e36-a3d5-3f8b47596755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 101
        },
        {
            "id": "1f9c4b64-b6c3-4c39-802b-a6d4d01e0576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 111
        },
        {
            "id": "bf79da7e-3998-4ecc-91a2-af6c3c9fe716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 113
        },
        {
            "id": "e398561e-bb14-4f08-b820-ca714608036b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 224
        },
        {
            "id": "f1e9239e-0406-45a6-bf7a-ea20d34ee755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 225
        },
        {
            "id": "58abb20b-879e-490b-bf40-53d3d6d4e419",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 226
        },
        {
            "id": "1fbee0c6-2114-42ff-86be-c6eac86778f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 227
        },
        {
            "id": "7ff0b12e-d930-4f54-b2ea-502a78ded940",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 228
        },
        {
            "id": "6af9de2c-3487-4569-b4b0-a86a02fad41f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 229
        },
        {
            "id": "ffbac998-09dc-46f6-a276-2c5c3b9d50df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 230
        },
        {
            "id": "d518bf50-4578-46c6-9847-863cb5651875",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 231
        },
        {
            "id": "d67e33c5-888a-4ad4-a0e0-0db1c1e1545e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 232
        },
        {
            "id": "823c95b5-da8d-4b6f-8f0e-fc70bb9415e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 233
        },
        {
            "id": "e6d7bec6-6bc5-4a90-a32e-36895a80e1f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 234
        },
        {
            "id": "e1a4b20f-d069-42aa-930e-955f2aac388c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 235
        },
        {
            "id": "ee8d056e-0a50-4415-9029-082e96f4fd2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 242
        },
        {
            "id": "096aed98-695d-4bf2-a5c5-fd8b4603e874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 243
        },
        {
            "id": "309a8f50-f0a2-4ab0-ad02-22bb3c46ce2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 244
        },
        {
            "id": "e7314a8b-4dd7-4408-ab42-d723324728fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 245
        },
        {
            "id": "74894fd0-9498-43e1-b241-4e0a1eb4eedc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 246
        },
        {
            "id": "3a9cc2ef-c698-4f13-9f5f-d98fc5a45854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 248
        },
        {
            "id": "6792bc24-4e97-447b-a883-defb44d89d40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 257
        },
        {
            "id": "53b83d41-b1c5-4662-a678-82b2fc7eca63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 259
        },
        {
            "id": "d6a7b183-6127-478a-bce5-14526d17498e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 261
        },
        {
            "id": "8544c628-f790-4164-b66d-89917fc6ee1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 263
        },
        {
            "id": "a2825ace-9f0b-4c86-b97e-9bebc6a9bc4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 265
        },
        {
            "id": "f8715a6d-ce4a-4273-a9e2-582a5b2c2723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 267
        },
        {
            "id": "8c9b2564-9621-4a38-a520-f656e41881ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 269
        },
        {
            "id": "fb0a1e4d-0341-4b83-bcb4-13cb939411fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 271
        },
        {
            "id": "b1c117b7-8759-482c-8112-45942564452f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 273
        },
        {
            "id": "a27e7e31-e186-420d-b121-4ca99f16543b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 275
        },
        {
            "id": "00231fef-516c-40a3-bb14-1b044904a19a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 277
        },
        {
            "id": "7db3bb40-916b-4845-9868-f2b8dacd74d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 279
        },
        {
            "id": "b11e15b8-54b7-40c7-b1c5-d6d155b96c08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 281
        },
        {
            "id": "954f9ef3-1935-4878-aa25-16a31e189719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 283
        },
        {
            "id": "6f4d8627-938a-40be-a898-16cb6fbc1f07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 333
        },
        {
            "id": "7328a0c4-7d28-47a5-b0fe-88308fbcb7ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 335
        },
        {
            "id": "cf230c96-5344-45b3-8314-b41375f0f718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 337
        },
        {
            "id": "31c8c772-dfc5-4dc0-ae55-678f177c4d81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 339
        },
        {
            "id": "2dc28188-e7c5-4d18-812b-44e5586af4f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 417
        },
        {
            "id": "67bf2200-0d88-4b08-89f9-f80d5d66c6e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 507
        },
        {
            "id": "24c39ab3-0c6a-46ad-9253-00a6930ee092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 509
        },
        {
            "id": "51cef909-da10-458a-8e0e-c012be0e1963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 511
        },
        {
            "id": "2bfcf897-ecb4-4b84-b3d0-2dfa568e4d73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7681
        },
        {
            "id": "b803e10d-9a5a-4ba1-92a0-0f36790b9181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7841
        },
        {
            "id": "9870cf31-b9d1-4eba-b751-f3b5aa45b701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7843
        },
        {
            "id": "776da7ab-0ed7-4358-bafc-587451836b03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7845
        },
        {
            "id": "536f53e4-0d0d-4c6c-a4b8-9f9f92612b42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7849
        },
        {
            "id": "a4c798d6-22a3-4f63-ab49-6fbc545f57fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7851
        },
        {
            "id": "30c157a0-c0da-445d-9210-56b4bd28ff95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7853
        },
        {
            "id": "d96b79f6-b138-4049-8c81-b1711f44089a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7855
        },
        {
            "id": "601895a3-f9c4-4035-a195-2404f48fbcb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7857
        },
        {
            "id": "e5774d6c-5b20-4227-9a39-158099775cf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7859
        },
        {
            "id": "c748fd47-aead-487f-8507-73bb40858a91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7861
        },
        {
            "id": "f3924f0b-8038-4ab2-a25b-285f36b07823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7863
        },
        {
            "id": "dcd26c86-b675-43c0-8afd-0fe230f900b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7865
        },
        {
            "id": "e6767e73-df6a-4386-8865-0f45898facd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7867
        },
        {
            "id": "2434c703-b3f4-4cc9-8abb-ab56775a1a58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7869
        },
        {
            "id": "76329baf-b959-4348-91cd-7574621f1bff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7871
        },
        {
            "id": "dbfe6672-06da-4761-9c01-04446f39330d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7875
        },
        {
            "id": "f28abd2d-2d7c-4bb8-b196-52839ea2f6ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7877
        },
        {
            "id": "305a4f0f-1baf-4eb3-9593-e76b6aa5c8fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7879
        },
        {
            "id": "67d19bab-3ac3-4a8b-8056-185d8919c7ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7885
        },
        {
            "id": "d48d79d2-95ad-433d-865b-48346146fe7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7887
        },
        {
            "id": "05c88793-cff8-46e0-b6db-3ed92511e566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7889
        },
        {
            "id": "5b923408-d138-43ad-b52e-049e0a9955fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7893
        },
        {
            "id": "73c43098-e367-4801-8636-210658ca1e4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7895
        },
        {
            "id": "863123d8-685f-480d-8a88-412a5e7c9817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7897
        },
        {
            "id": "d3f28b97-18b9-4970-9485-06dd159df3e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7899
        },
        {
            "id": "79a6a615-a823-429c-9902-a472795faa0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7901
        },
        {
            "id": "4aabf459-8848-4805-8315-a4e7438c2e2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7903
        },
        {
            "id": "683cc23a-1296-4dd3-83f9-8463f2e0bb68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7905
        },
        {
            "id": "2fbfa687-dc24-420e-b8f5-48041e6a0782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 7907
        },
        {
            "id": "9377d99d-5be1-47fe-a128-ed5abea1b1ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "ef779c30-19f6-44c4-b0b3-bbdaf21c4074",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8221
        },
        {
            "id": "c9d370dd-7572-466c-b358-e324476a1bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 34
        },
        {
            "id": "9b22a5b6-933f-4dc5-b882-52be9d53c6af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 39
        },
        {
            "id": "67637b07-5079-42fd-8787-343449113221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 8217
        },
        {
            "id": "cf7c334d-cc8c-490b-bbd7-55c9d23d8eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 8221
        },
        {
            "id": "55411f33-d9de-420a-b001-51df00f440f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 34
        },
        {
            "id": "adfba558-c410-4fb4-bf8c-af2199cb91f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 39
        },
        {
            "id": "8ba7250b-c38c-4641-a8fc-c51be020662a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "863a17a3-e4ba-4fcd-bdd3-f5eca8ebb8c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "5d504d95-dcb5-45b8-8689-c6ca41b060b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 63
        },
        {
            "id": "2ca56f74-7923-4dbb-92d2-94f5262bcf57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 8217
        },
        {
            "id": "a33ac805-3f72-42be-9611-b82accab8811",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8218
        },
        {
            "id": "25ecca94-ab4d-4dc3-883d-5db0437f8078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 118,
            "second": 8221
        },
        {
            "id": "3ddc52e0-2428-43b9-a2f1-653ec8c863da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8222
        },
        {
            "id": "4ae47fb2-c6df-4665-9df0-957e077531de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 119,
            "second": 34
        },
        {
            "id": "b37bf2f9-f808-409e-885d-c0b67441c1af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 119,
            "second": 39
        },
        {
            "id": "a6065af7-5195-4826-87eb-041eeaa1a387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "cc81958c-520a-4ce8-bb86-b50471597d48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "9da9ab6a-f08f-4770-bfe2-0cffbbd6bb33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 63
        },
        {
            "id": "09c9c7de-435e-45ec-9bd2-2feb9ef5f0c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 119,
            "second": 8217
        },
        {
            "id": "ce3378f9-0b33-409c-a4d2-773463736d12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 8218
        },
        {
            "id": "0cb844c9-850d-4398-8a08-97efde732c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 119,
            "second": 8221
        },
        {
            "id": "040bb261-05ae-481b-9b84-3e707911fec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 8222
        },
        {
            "id": "c3ae0035-b6c9-4cd0-b28f-fae200b03bb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 99
        },
        {
            "id": "609602c7-d093-452e-9a68-f16ac673e939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "bc19db51-c0db-4d9c-af9b-4ea55a59291c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 101
        },
        {
            "id": "a3bc0450-02ac-4659-a72e-f28df83b0372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "c8e44ed7-8ffc-42c8-b4c1-66972998a9e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "311a2ac3-6ace-43b6-ac24-4acf58e40be8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 224
        },
        {
            "id": "e9774f9f-ddb9-49ac-8481-d5b317f34ca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 231
        },
        {
            "id": "4fbdccaf-43de-4415-9090-a308b3f9ceb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 232
        },
        {
            "id": "542fbbe1-8cd2-440f-8dfb-5bc7f4bae77a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 233
        },
        {
            "id": "1077770c-6622-40e0-a576-c491aa983b7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 234
        },
        {
            "id": "2a5c3178-c39d-4b82-92b1-cf35131adcf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 235
        },
        {
            "id": "ccf7e01b-8d23-4632-8e32-211df8f99ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 242
        },
        {
            "id": "88937581-b1c2-4fcf-a84d-06f7be5909e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 243
        },
        {
            "id": "4dcba237-586a-4364-a2dc-d4a17e3cc2a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 244
        },
        {
            "id": "1802c7a1-58f4-47b6-b945-ca518154f49e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 245
        },
        {
            "id": "00e50a4f-4e48-4a1f-9458-eb602f5ed282",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 246
        },
        {
            "id": "b2cc1399-1db8-41ca-9800-821e106bf141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 248
        },
        {
            "id": "83e3d4b9-1fc3-4f11-8917-14aba240ef21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 263
        },
        {
            "id": "72ef83a6-5e23-46cb-8f28-79365cb0334c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 265
        },
        {
            "id": "909c05a1-8b95-48f3-92f2-e1b5a736ea8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 267
        },
        {
            "id": "6604540a-ce82-4d70-b2aa-176102ec1520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 269
        },
        {
            "id": "97eec578-a025-4668-b6ad-2e854142b026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 271
        },
        {
            "id": "c1df1adc-50ee-4d46-96a8-aab951134585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 273
        },
        {
            "id": "a775856f-326a-4ed4-a4f8-c06c9b786202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 275
        },
        {
            "id": "902826cc-e5f5-444c-8e0c-194aee033fa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 277
        },
        {
            "id": "0c181ab7-9837-4b96-876b-7e772162c56f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 279
        },
        {
            "id": "051512db-7c58-4d5c-9a98-2053fa7d636b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 281
        },
        {
            "id": "63a39132-fcc2-4da3-b103-fe3c8d458306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 283
        },
        {
            "id": "5a3faf37-07b5-4d43-93eb-2044f4cc6569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 333
        },
        {
            "id": "389d11e0-e9fe-486a-8ebc-2b57321b9b4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 335
        },
        {
            "id": "9b045170-3339-48ff-b0ae-0a9e259b59e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 337
        },
        {
            "id": "cb830d86-59b8-4501-9e12-e9282e3ee61e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 339
        },
        {
            "id": "503a1575-1ff0-4214-b2d8-d5f163db4436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 417
        },
        {
            "id": "222a1ff5-8136-49cd-be27-46d4670b4fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 511
        },
        {
            "id": "5c7d90e1-c6de-47be-bdfe-97065c87f59c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7865
        },
        {
            "id": "ea939afa-3072-40c5-9bea-e27a47e4136d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7867
        },
        {
            "id": "517d4770-7575-4586-9eeb-683529f969ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7869
        },
        {
            "id": "07209fe7-abb3-4c61-9e22-18d1ec955a51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7871
        },
        {
            "id": "380a2c24-99af-4bc4-ac3c-55fff32874fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7875
        },
        {
            "id": "62d0e495-dd90-4086-a7ce-1b3ecdf39f5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7877
        },
        {
            "id": "df90a027-239b-402d-b6a8-2ff2a8707b11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7879
        },
        {
            "id": "a5f39f38-e5ab-4723-b16e-efa140a84802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7885
        },
        {
            "id": "10025ef7-3383-42fa-bf9a-d6863350d7a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7887
        },
        {
            "id": "a550f3ae-e8b7-48fb-b84f-b65eb9f70ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7889
        },
        {
            "id": "24199f84-a701-4b9e-ba67-54f2dae53c6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7893
        },
        {
            "id": "cbde45cc-1d13-4190-8d85-ef2fd3d987b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7895
        },
        {
            "id": "b3397d99-1cd4-44d4-bc10-96a89c2d6d77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7897
        },
        {
            "id": "6fcd2d6d-5d2d-4618-8fa5-0b46b68112b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7899
        },
        {
            "id": "6c20c549-cbbf-463c-add6-5e56a93b5e35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7901
        },
        {
            "id": "9f576a1c-f2b9-4e9a-8993-73f582801bc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7903
        },
        {
            "id": "bcbf0a93-a594-4d10-9dd3-51b5a0787d41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7905
        },
        {
            "id": "055291cc-8d74-4e45-a264-5ada731f279e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 7907
        },
        {
            "id": "a1a36f94-7c59-4ac6-b9eb-4067b425640e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 121,
            "second": 34
        },
        {
            "id": "0188a00d-722c-4191-8cf3-ff3fa575188d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 121,
            "second": 39
        },
        {
            "id": "a8e1ea50-bbee-441c-ae8b-d2066ee097c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "cc41b26b-d24d-49fb-a5d1-790547fa9bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        },
        {
            "id": "cec47e5b-cdae-4fa0-b1db-00ecebc401be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 63
        },
        {
            "id": "3f8e566e-9441-4a3e-8707-7d993a2452cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 121,
            "second": 8217
        },
        {
            "id": "0380612e-e69b-4609-9b67-90b7340eb784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 8218
        },
        {
            "id": "3e78ce1b-f94b-416f-9ada-6c193966dec0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 121,
            "second": 8221
        },
        {
            "id": "a5b523c1-0920-4d2c-b1ae-36d7265185af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 8222
        },
        {
            "id": "92e8c8fc-cfb3-4b15-80f4-c723aadc6b9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 123,
            "second": 74
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 36,
    "styleName": "",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}